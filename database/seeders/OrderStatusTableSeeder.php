<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\OrderStatus;

class OrderStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $orderStatusRecords = [
            ['id'=>1,'name'=>'New','Status'=>1],
            ['id'=>2,'name'=>'Pending','Status'=>1],
            ['id'=>3,'name'=>'Cancelled','Status'=>1],
            ['id'=>4,'name'=>'In Process','Status'=>1],
            ['id'=>5,'name'=>'Shipped','Status'=>1],
            ['id'=>6,'name'=>'Delivered','Status'=>1],
            ['id'=>7,'name'=>'Paid','Status'=>1],
        ];

        OrderStatus::insert($orderStatusRecords);
    }
}
