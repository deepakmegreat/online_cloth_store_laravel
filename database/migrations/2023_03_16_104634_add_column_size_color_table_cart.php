<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('cart', function (Blueprint $table) {
            $table->string('color')->after('Productid');
            $table->string('size')->after('color');
            $table->string('photo')->after('size');
            $table->string('product_name')->after('photo');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('cart', function (Blueprint $table) {
            $table->dropColumn('color');
            $table->dropColumn('size');
            $table->dropColumn('photo');
            $table->dropColumn('product_name');
        });
    }
};
