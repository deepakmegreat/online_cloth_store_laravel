<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
	Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('category_id');
            $table->bigInteger('price');
            $table->bigInteger('quantity');
            $table->enum('status',['A','I'])->comment('A for Active & I for Inactive');
            $table->string('image')->nullable();
            $table->string('description');
            $table->softDeletes();
            $table->timestamps();
        });
        
        Schema::create('sizes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('size');
            $table->timestamps();
        });
        
        Schema::create('product_sizes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('size');
            $table->unsignedBigInteger('product_id');
            $table->enum('status',['A','I'])->comment('A for Active & I for Inactive');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('size')->references('id')->on('sizes')->onDelete('cascade');
        });

	Schema::create('cart', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('Userid');
            $table->unsignedBigInteger('Productid');
            $table->integer('Price');
            $table->integer('quantity');
            $table->foreign('Productid')->references('id')->on('products');
            $table->foreign('Userid')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
	Schema::dropIfExists('product_sizes');
        Schema::dropIfExists('products');
        Schema::dropIfExists('sizes');
        Schema::dropIfExists('products');
        Schema::dropIfExists('cart');
    }
};
