@extends('front_theme.layouts.master')

@section('title')
    Shop Page | StyleFushion
@endsection

@section('content')
<!-- Breadcrumb Begin -->
    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__links">
                        <a href="{{ route('/') }}"><i class="fa fa-home"></i> Home</a>
                        <span>Shop</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Breadcrumb End -->
<!-- Shop Section Begin -->
    <section class="shop spad">
        <div class="container">
            <form id="myfilter">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <div class="shop__sidebar">
                            <div class="sidebar__categories">
                                <div class="section-title">
                                    <h4>Categories</h4>
                                </div>
                                <div class="categories__accordion">
                                    <div class="accordion" id="accordionExample">
                                        @foreach ($category as $category)
                                            <div class="card">
                                            <div class="card-heading">
                                                <a data-toggle="collapse" data-target="#collapseOne" data-categoryid="{{ $category->id }}">{{ $category->name }}</a>
                                            </div>
                                             <!-- <div id="collapseOne" class="collapse show" data-parent="#accordionExample">
                                                <div class="card-body">
                                                    <ul>
                                                        <li><a href="#">Coats</a></li>
                                                        <li><a href="#">Jackets</a></li>
                                                    </ul>
                                                </div>
                                            </div>  -->
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <div class="sidebar__filter">
                                <div class="section-title">
                                    <h4>Shop by price</h4>
                                </div>
                                <div class="filter-range-wrap">
                                    <div class="price-range ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"
                                    data-min="100" data-max="30000" id="price-range"></div>
                                    <div class="range-slider">
                                        <div class="price-input">
                                            <p>Price:</p>
                                            <input type="text" id="minamount" name="minamount">
                                            <input type="text" id="maxamount" name="maxamount">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sidebar__sizes">
                                <div class="section-title">
                                    <h4>Shop by size</h4>
                                </div>
                                <div class="size__list">
                                    @foreach ($sizes as $size)
                                        <label for="{{ $size->size }}">
                                            {{ $size->size }}
                                            <input type="checkbox" id="{{ $size->size }}" name="size" value="{{ $size->id }}">
                                            <span class="checkmark" name="size"></span>
                                        </label>
                                    @endforeach
                                </div>
                            </div>
                            <div class="sidebar__color">
                                <div class="section-title">
                                    <h4>Shop by Color</h4>
                                </div>
                                <div class="size__list color__list" >

                                    @foreach ($productsColor as $colorvalue)
                                        <label for="color{{ $colorvalue->colour_code }}">
                                            <input type="checkbox" name="color" id="color{{ $colorvalue->colour_code }}" value="{{ $colorvalue->id }}">
                                            <span class="checkmark" style="background: {{ $colorvalue->colour_code }}"></span>
                                        </label>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9">
                        <div class="row productdisplay">
                                @foreach ($products as $product)
                                    <div class="col-lg-4 col-md-6">
                                        {{-- <div class="col-lg-3 col-md-4 col-sm-6 mix women"> --}}
                                        <div class="product__item">
                                                <?php
                                                if (file_exists(public_path('backend/images/uploads/products/'. $product->image)) && $product->image != null && $product->image != '') {
                                                $image = asset('backend/images/uploads/products/' . $product->image);
                                                } else {
                                                $image = asset('backend/images/uploads/defaultproduct.jpg');
                                                }
                                            ?>
                                            <div class="product__item__pic" >
                                                <div class="product__item__pic set-bg" data-setbg="{{ $image }}">
                                                    @if($product->quantity == 0)
                                                        <div class="label stockout">out of stock</div>
                                                    @else
                                                        <div class="label new">New</div>
                                                    @endif
                                                    <ul class="product__hover">
                                                        <li><a href="{{ $image }}" class="image-popup" ><span class="arrow_expand"></span></a></li>
                                                        <!-- <li><a href="#"><span class="icon_heart_alt"></span></a></li>
                                                        <li><a href="#"><span class="icon_cart_alt"></span></a></li> -->
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="product__item__text">
                                                <h6><a href="{{ route('product-detail', [$product->id]) }}">{{ $product->name }}</a></h6>
                                                {{-- <div class="rating">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </div> --}}
                                                <div class="product__price">&#8377; {{ $product->price }}</div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                @if(count($products) > 0)
                                    {{ $products->links('front_theme/custom-pagination',compact('products')) }}
                                @else
                                    <div class="error">
                                        Product Not Found
                                    </div>
                                @endif
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
<!-- Shop Section End -->

@endsection

@section('js')
<script>
    var priceRange = $('#price-range');
    var form = $('#myfilter');
    var url = window.location.pathname;
    var lastindex = url.substring(url.lastIndexOf('/'));
    var id = url.substring(url.lastIndexOf('/') + 1);

    if(lastindex != '/shop'){
        searchcategory();
    }
    priceRange.slider({
        change: function() {
            var categoryId = $('#active2').data('categoryid');
            var data = form.serializeArray();
            var selectedSizes = $('input[name="size"]:checked').serializeArray();
            var checkedColor = $('input[name="color"]:checked').serializeArray();
            var sizeIds = [];
            $.each(selectedSizes, function(index, value){
                sizeIds.push(value.value);
            });

            var colorIds = [];
            $.each(checkedColor, function(index, value){
                colorIds.push(value.value);
            });
            data.push({name: 'sizeId', value: sizeIds});
            data.push({name: 'colorId', value: colorIds});
            data.push({name: 'categoryid', value: categoryId});

            $.ajax({
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ route('shop_filter') }}",
            type: 'get',
            dataType: "json",
            data: data,
            success:function(data){
                $(".productdisplay").html('');
                $(".productdisplay").append(data.html);
            }

            });
        }
     });

    $('input[type="checkbox"]').click(function() {
        var data = form.serializeArray();
            var categoryId = $('#active2').data('categoryid');

            var selectedSizes = $('input[name="size"]:checked').serializeArray();
            var checkedColor = $('input[name="color"]:checked').serializeArray();
            var sizeIds = [];
            $.each(selectedSizes, function(index, value){
                sizeIds.push(value.value);
            });

            var colorIds = [];
            $.each(checkedColor, function(index, value){
                colorIds.push(value.value);
            });
            data.push({name: 'sizeId', value: sizeIds});
            data.push({name: 'colorId', value: colorIds});
            data.push({name: 'categoryid', value: categoryId});
            $.ajax({
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ route('shop_filter') }}",
            type: 'get',
            dataType: "json",
            data: data,
            success:function(data){
                $(".productdisplay").html('');
                $(".productdisplay").append(data.html);
            }

            });
    });

    var selectedCategories = []; // Initialize an empty array to store the selected category IDs
    //Catgegory Filter
    $('.categories__accordion a').click(function() {
            var categoryId = $(this).data('categoryid');
            $('[id^="active2"]').removeAttr('id');
            $(this).attr('id', 'active2');

            var data = form.serializeArray();
            var selectedSizes = $('input[name="size"]:checked').serializeArray();
            var checkedColor = $('input[name="color"]:checked').serializeArray();
            var sizeIds = [];
            $.each(selectedSizes, function(index, value){
                sizeIds.push(value.value);
            });

            var colorIds = [];
            $.each(checkedColor, function(index, value){
                colorIds.push(value.value);
            });
            data.push({name: 'sizeId', value: sizeIds});
            data.push({name: 'colorId', value: colorIds});
            data.push({name: 'categoryid', value: categoryId});
            $.ajax({
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ route('shop_filter') }}",
            type: 'get',
            dataType: "json",
            data: data,
            success:function(data){
                $(".productdisplay").html('');
                $(".productdisplay").append(data.html);
            }

            });
    });

    //  Pagination Ajax
    $(document).on('click', '.pagination__option a', function(event){
        event.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        fetch_data(page);
    });

    function fetch_data(page)
    {
        var categoryId = $('#active2').data('categoryid');
        var data = form.serializeArray();
        var selectedSizes = $('input[name="size"]:checked').serializeArray();
        var checkedColor = $('input[name="color"]:checked').serializeArray();
        var sizeIds = [];
        $.each(selectedSizes, function(index, value){
            sizeIds.push(value.value);
        });

        var colorIds = [];
        $.each(checkedColor, function(index, value){
            colorIds.push(value.value);
        });
        data.push({name: 'sizeId', value: sizeIds});
        data.push({name: 'colorId', value: colorIds});
        data.push({name: 'categoryid', value: categoryId});

        // var url = '{{route("shop_filter", ["page" => ":page"])}}';
        // url = url.replace(":page", page);
        //console.log(url);
        var url = "{{route('shop_filter')}}"+"?page="+page;
        $.ajax({
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            // url:"shop_filter?page="+page,
            url:url,
            data: data,
            success:function(data)
            {
                $(".productdisplay").html('');
                $(".productdisplay").append(data.html);

            }
        });
    }


    function searchcategory(){
        var categoryId = id;

        $('[id^="active2"]').removeAttr('id');
        //$(this).attr('id', 'active2');
       // alert(categoryId);
        $(".categories__accordion").find(`[data-categoryid='${categoryId}']`).attr('id', 'active2');
        var data = form.serializeArray();
        var selectedSizes = $('input[name="size"]:checked').serializeArray();
        var checkedColor = $('input[name="color"]:checked').serializeArray();
        var sizeIds = [];
        $.each(selectedSizes, function(index, value){
            sizeIds.push(value.value);
        });

        var colorIds = [];
        $.each(checkedColor, function(index, value){
            colorIds.push(value.value);
        });
        data.push({name: 'sizeId', value: sizeIds});
        data.push({name: 'colorId', value: colorIds});
        data.push({name: 'categoryid', value: categoryId});
        $.ajax({
        headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "{{ route('shop_filter') }}",
        type: 'get',
        dataType: "json",
        data: data,
        success:function(data){
            $(".productdisplay").html('');
            $(".productdisplay").append(data.html);
        }

        });
    }


</script>
@endsection