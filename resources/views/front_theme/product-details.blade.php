@extends('front_theme.layouts.master')

@section('title')
    Product Detail Page | StyleFushion
@endsection

@section('content')

@if(isset($product) and !empty($product))

<!-- Breadcrumb Begin -->
    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__links">
                        <a href="{{ route('index') }}"><i class="fa fa-home"></i> Home</a>
                        <span>{{ $product->name }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->
   
<!-- Product Details Section Begin -->
    <section class="product-details spad">
        <div class="container">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }} <a href="{{ route('cart') }}" class="alert-link">View Cart</a>
                </div>
            @endif
            @if(session('success_wishlist'))
                <div class="alert alert-success">
                    {{ session('success_wishlist') }} <a href="{{ route('wishlist') }}" class="alert-link">View Wishlist</a>
                </div>
            @endif
            
            <div class="row">
                <div class="col-lg-6">
                    <div class="product__details__pic">
                        <div class="product__details__pic__left product__thumb nice-scroll">
                            <a class="pt active" href="">
                            <?php
                                if (file_exists(public_path('backend/images/uploads/products/'. $product->image)) && $product->image != null && $product->image != '') {
                                        $image = asset('backend/images/uploads/products/' . $product->image);
                                    } else {
                                        $image = asset('backend/images/uploads/defaultproduct.jpg');
                                    }
                            ?>
                                <img src="{{ $image }}" alt="Product Image">
                            </a>                            
                        </div>
                        <div class="product__details__slider__content">
                            <div class="product__details__pic__slider owl-carousel">
                                <img data-hash="product-1" class="product__big__img" src="{{ $image }}" alt="Product Image">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="product__details__text">
                        <h3 class="prod_name">{{ $product->name }}</h3>
                        {{-- <div class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <span>( 138 reviews )</span>
                        </div> --}}
                        <div class="product__details__price">&#8377; <span class="price">{{ $product->price }}</span></div>
                        <div class="product__details__button">
                            <div class="quantity">
                                <span>Quantity:</span>
                                <div class="pro-qty">
                                    <input type="text" value="1" disabled class="quantity1">
                                </div>
                            </div>
                            @if($product->quantity > 0)
                            {{-- route('add_to_cart', $value->id) --}}
                                <a class="cart-btn add_to_cart" data-id={{ $product->id }} data-img= {{ $product->image }}><span class="icon_bag_alt"></span> Add to cart</a>
                                @if(Auth::user())
                                    <a class="cart-btn add_to_wishlist" data-id={{ $product->id }} data-img= {{ $product->image }}><span class="icon_heart_alt"></span> </a>
                                @else
                                    <a href="{{ route('wishlist') }}"><span class="cart-btn icon_heart_alt"></span></a>
                                @endif
                            @endif
                            <ul>
                                <!-- <li><a href="#"><span class="icon_heart_alt"></span></a></li> -->
                                <!-- <li><a href="#"><span class="icon_adjust-horiz"></span></a></li> -->
                            </ul>
                        </div>
                        <div class="product__details__widget">
                            <ul>
                                <li>
                                    <span>Availability:</span>
                                    @if($product->quantity > 0)
                                        <label for="stockin">
                                            In Stock
                                        </label>
                                    @else
                                        <label for="stockout">
                                            Stock Out
                                        </label>
                                    @endif
                                </li>
                                <li>
                                    <span>Available color:</span>
                                    <div class="color__checkbox">
                                            @foreach ($product->prodColor as $colorvalue)
                                            <label for="color{{ $colorvalue->id }}">
                                                <input type="radio" name="color_radio" id="color{{ $colorvalue->id }}" value="{{ $colorvalue->colour }}">
                                                <span class="checkmark" style="background: {{ $colorvalue->colour_code }}"></span>
                                            </label>
                                            @endforeach
                                        </div>
                                </li>
                                <li>
                                    <span>Available size:</span>
                                    <div class="size__list">
                                        
                                        @foreach ($product->prodSize as $sizevalue)
                                        <label for="{{ $sizevalue->size }}">
                                            <input type="radio" id="{{ $sizevalue->size }}" name="size_radio" value="{{ $sizevalue->size }}">
                                            {{ $sizevalue->size }}
                                            <span class="checkmark" name="size"></span>
                                        </label>
                                        @endforeach
                                    </div>
                                </li>
                                <li>
                                    <span>Promotions:</span>
                                    <p>Free shipping</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="product__details__tab">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab">Description</a>
                            </li>
                            {{-- <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab">Reviews ( 2 )</a>
                            </li> --}}
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tabs-1" role="tabpanel">
                                <h6>Description</h6>
                                <p>{{ $product->description }}</p>
                            </div>
                            {{-- <div class="tab-pane" id="tabs-3" role="tabpanel">
                                <h6>Reviews ( 2 )</h6>
                                <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut loret fugit, sed
                                    quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt loret.
                                    Neque porro lorem quisquam est, qui dolorem ipsum quia dolor si. Nemo enim ipsam
                                    voluptatem quia voluptas sit aspernatur aut odit aut loret fugit, sed quia ipsu
                                    consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Nulla
                                consequat massa quis enim.</p>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                                    dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                                    nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium
                                quis, sem.</p>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="related__title">
                        <h5>RELATED PRODUCTS</h5>
                    </div>
                </div>
                @foreach ($relatedProduct as $relatedproduct)
                    <div class="col-lg-3 col-md-4 col-sm-6 mix women">
                        <div class="product__item">
                            <?php
                                if (file_exists(public_path('backend/images/uploads/products/' . $relatedproduct->image)) && $relatedproduct->image != null && $relatedproduct->image != '') {
                                    $image = asset('backend/images/uploads/products/' . $relatedproduct->image);
                                } else {
                                    $image = asset('backend/images/uploads/defaultproduct.jpg');
                                }
                            ?>
                            <div class="product__item__pic set-bg" data-setbg="{{ $image }}">
                                @if ($relatedproduct->quantity == 0)
                                    <div class="label stockout">out of stock</div>
                                @else
                                    <div class="label new">New</div>
                                @endif
                                <ul class="product__hover">
                                    <li><a href="{{ asset('front_theme/img/product/product-1.jpg') }}" class="image-popup"><span class="arrow_expand"></span></a></li>
                                    {{-- <li><a href="#"><span class="icon_heart_alt"></span></a></li>
                                    <li><a href="#"><span class="icon_bag_alt"></span></a></li> --}}
                                </ul>
                            </div>
                            <div class="product__item__text">
                                <h6><a href="{{ route('product-detail',[$relatedproduct->id]) }}">{{ $relatedproduct->name }}</a></h6>
                                {{-- <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div> --}}
                                <div class="product__price">{{ $relatedproduct->price }}</div>
                            </div>
                        </div>
                     </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- Product Details Section End -->

@endif {{-- End if condition --}}
@endsection

@section('js')
<script>
    $('[name="color_radio"]:first').prop('checked', true);
    $('[name="size_radio"]:first').prop('checked', true);

    $('.add_to_cart').on('click', function() {
        
        $.ajax({
            url:'{{ route("add_to_cart") }}',
            method:"post",
            data:{
                _token: '{{ csrf_token() }}',
                id: $(this).attr("data-id"),
                price: $('.price').text(),
                quantity:$('.quantity1').val(),
                color:$('[name="color_radio"]:checked').val(),
                size:$('[name="size_radio"]:checked').val(),
                product_name:$('.prod_name').text(),
                photo:$(this).attr("data-img")
            },
            success: function(response){
                    window.location.reload();                
            }
        });
    });

    $('.add_to_wishlist').on('click', function() {
            $.ajax({
                url:'{{ route("add_to_wishlist") }}',
                method:"post",
                data:{
                    _token: '{{ csrf_token() }}',
                    id: $(this).attr("data-id"),
                    price: $('.price').text(),
                    quantity:$('.quantity1').val(),
                    color:$('[name="color_radio"]:checked').val(),
                    size:$('[name="size_radio"]:checked').val(),
                    product_name:$('.prod_name').text(),
                    photo:$(this).attr("data-img")
                },
                success: function(response){
                    // console.log(response);
                    window.location.reload();                
                }
            });
       
    });
</script>
@endsection