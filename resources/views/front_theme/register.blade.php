<!DOCTYPE html>
<html lang="en">
@include('front_theme.layouts.common_header')
<body>
@section('title')
    Register Page | StyleFushion
@endsection 
<section class="my-5">
<div class="container">
    <form id="signupForm" action="{{ route('register') }}"class="checkout__form" method="POST">
        @csrf
        <div class="row">
            <div class="col-lg-3"></div>

            <div class="col-lg-6 px-5 py-4 shadow-lg rounded" style="background-color: #eee; position: absolute; top: 50%;left: 50%;transform: translate(-50%, -50%);">
                <h5 class="text-center">Register Yourself</h5>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="checkout__form__input">
                            <p>Name <span>*</span></p>
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" autocomplete="name" autofocus placeholder="Enter Your Name">
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="checkout__form__input">
                            <p>Email <span>*</span></p>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" placeholder="Enter Your Email">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="checkout__form__input">
                            <p>Password <span>*</span></p>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password" placeholder="Enter Your Password">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="checkout__form__input">
                            <p>Confirm Password <span>*</span></p>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="new-password" placeholder="Enter Your Confirm Password">
                        </div>
                        
                    </div> 
                    <div class="col-lg-12 mt-4 d-flex justify-content-end">
                        <input type="submit" value="Register" class="btn btn-dark">
                    </div>
                    <div class="col-lg-12 mt-4 d-flex justify-content-end">
                        <p class="checkout__form__input" >Already Have an Account ! <a href="{{ route('login')}}" class="checkout__form__input" >Login</a></p>
                    </div>
                </div>
            </div>

            <div class="col-lg-3"></div>
        </div>
    </form>
</div>
</section>
@section('js')
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="{{ asset('front_theme/js/custom_js/form_validation.js') }}"></script>
@endsection

@include('front_theme.layouts.common_js')
</body>

</html>