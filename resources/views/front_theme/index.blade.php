@extends('front_theme.layouts.master')

@section('title')
    Home Page | StyleFushion
@endsection

@section('content')

    <!-- Categories Section Begin -->
    <section class="slider">
        {{-- <div class="container-fluid"> --}}
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" height="600px" src="{{ asset('front_theme/img/banner/img3.jpg') }}"
                            alt="First slide">
                        <div class="row float-right col-md-8">
                            <div class="carousel-caption categories__text">
                                <h1>New Clothes, New Passion.</h1>
                                <p class="text-dark">Confuse your mirror with our trendy outfits.
                                    <br>Stylish Fit You’ll Ever Need</p>
                                <a href="{{ route('shop') }}">Shop now</a>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" height="600px" src="{{ asset('front_theme/img/banner/men_img.jpg') }}"
                            alt="Second slide">
                            <div class="row col-md-8">
                                <div class="carousel-caption categories__text">
                                    <h1>Wear better, look better.</h1>
                                    <p class="text-dark">Fashion looks better when you feel good on the inside
                                        <br>The outfits you are looking for
                                        .</p>
                                    <a href="{{ route('shop') }}">Shop now</a>
                                </div>
                            </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" height="600px" src="{{ asset('front_theme/img/banner/banner-3.jpg') }}"
                            alt="Third slide">
                            <div class="row col-md-12 d-flex align-content-between">
                                <div class="carousel-caption categories__text">
                                     <h1>You pretty so wear pretty</h1>
                                    <p class="text-dark">FFF – Fashion, fun & friends
                                        <br>Keep calm & surround yourself with fashion</p>
                                    <a href="{{ route('shop') }}">Shop now</a>
                                </div>
                            </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        {{-- </div> --}}
        </div>
    </section>
    <!-- Categories Section End -->

    <section class="categories">
        <div class="container-fluid">
            <div class="row px-5 py-2">
                <div class="col-lg-4 col-md-4">
                    <div class="section-title">
                        <h4>Categories</h4>
                    </div>
                </div>
            </div>
            <div class="row d-flex bg-light px-5 py-2">
                @if (isset($categorys) && !empty($categorys))
                    @foreach ($categorys as $category)
                        <div class="col-lg-4 col-md-4">
                            <?php
                            if (file_exists(public_path('backend/images/uploads/category/' . $category->category_image)) && $category->category_image != null && $category->category_image != '') {
                                $image = asset('backend/images/uploads/category/' . $category->category_image);
                            } else {
                                $image = asset('backend/images/uploads/default.jpg');
                            }
                            ?>
                            <a href="{{ route('shop', [$category->id])  }}">
                                <div class="single-banner"
                                    style="background: url({{ $image }}); background-size: cover; height: 300px;">

                                </div>
                            </a>
                            <div class="category-name text-center m-2">
                            <a href="{{ route('shop', [$category->id])  }}"><h5>{{ $category->name }}</h5></a>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>

    <!-- Product Section Begin -->
    <section class="product spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <div class="section-title">
                        <h4>New product</h4>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8">
                    <ul class="filter__controls text-decoration-none">

                        @if (isset($categorys) && !empty($categorys))
                            <li><a class="category-filter" data-id="0">All</a></li>
                            @foreach ($categorys as $category)
                                <li><a class="category-filter" data-id="{{ $category->id }}">{{ $category->name }}</a></li>
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>

            <div class="productdisplay">

                @if (isset($products) && !empty($products))
                    <div class="row property__gallery">

                        @foreach ($products as $product)
                            <div class="col-lg-3 col-md-4 col-sm-6 mix women">
                                <?php
                                if (file_exists(public_path('backend/images/uploads/products/' . $product->image)) && $product->image != null && $product->image != '') {
                                    $image = asset('backend/images/uploads/products/' . $product->image);
                                } else {
                                    $image = asset('backend/images/uploads/defaultproduct.jpg');
                                }
                                ?>

                                <div class="product__item">
                                    <div class="product__item__pic set-bg" data-setbg="{{ $image }}">
                                        @if ($product->quantity == 0)
                                            <div class="label stockout">out of stock</div>
                                        @else
                                            <div class="label new">New</div>
                                        @endif
                                        <ul class="product__hover">
                                            <li><a href="{{ $image }}" class="image-popup"><span
                                                        class="arrow_expand"></span></a></li>
                                            <!-- <li><a href="#"><span class="icon_heart_alt"></span></a></li>
                                            <li><a href="#"><span class="icon_bag_alt"></span></a></li> -->
                                        </ul>
                                    </div>

                                    <div class="product__item__text">
                                        <h6><a
                                                href="{{ route('product-detail', [$product->id])  }}">{{ $product->name }}</a>
                                        </h6>
                                        {{-- <div class="rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div> --}}
                                        <div class="product__price">&#8377; {{$product->price }}</div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 d-flex justify-content-end">
                            <a href="{{ route('shop') }}" class="site-btn btn">View more</a>
                        </div>
                    </div>
                @endif
                <div>

    </section>



    <!-- Product Section End -->



    <!-- Banner Section Begin -->
    <!-- <section class="banner set-bg" data-setbg="{{ asset('front_theme/img/banner/banner-1.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-xl-7 col-lg-8 m-auto">
                    <div class="banner__slider owl-carousel">
                        <div class="banner__item">
                            <div class="banner__text">
                                <span>The Chloe Collection</span>
                                <h1>The Project Jacket</h1>
                                <a href="#">Shop now</a>
                            </div>
                        </div>
                        <div class="banner__item">
                            <div class="banner__text">
                                <span>The Chloe Collection</span>
                                <h1>The Project Jacket</h1>
                                <a href="#">Shop now</a>
                            </div>
                        </div>
                        <div class="banner__item">
                            <div class="banner__text">
                                <span>The Chloe Collection</span>
                                <h1>The Project Jacket</h1>
                                <a href="#">Shop now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!-- Banner Section End -->

    <!-- Trend Section Begin -->
    <!-- <section class="trend spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="trend__content">
                        <div class="section-title">
                            <h4>Hot Trend</h4>
                        </div>
                        <div class="trend__item">
                            <div class="trend__item__pic">
                                <img src="{{ asset('front_theme/img/trend/ht-1.jpg') }}" alt="">
                            </div>
                            <div class="trend__item__text">
                                <h6>Chain bucket bag</h6>
                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                                <div class="product__price">$ 59.0</div>
                            </div>
                        </div>
                        <div class="trend__item">
                            <div class="trend__item__pic">
                                <img src="{{ asset('front_theme/img/trend/ht-2.jpg') }}" alt="">
                            </div>
                            <div class="trend__item__text">
                                <h6>Pendant earrings</h6>
                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                                <div class="product__price">$ 59.0</div>
                            </div>
                        </div>
                        <div class="trend__item">
                            <div class="trend__item__pic">
                                <img src="{{ asset('front_theme/img/trend/ht-3.jpg') }}" alt="">
                            </div>
                            <div class="trend__item__text">
                                <h6>Cotton T-Shirt</h6>
                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                                <div class="product__price">$ 59.0</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="trend__content">
                        <div class="section-title">
                            <h4>Best seller</h4>
                        </div>
                        <div class="trend__item">
                            <div class="trend__item__pic">
                                <img src="{{ asset('front_theme/img/trend/bs-1.jpg') }}" alt="">
                            </div>
                            <div class="trend__item__text">
                                <h6>Cotton T-Shirt</h6>
                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                                <div class="product__price">$ 59.0</div>
                            </div>
                        </div>
                        <div class="trend__item">
                            <div class="trend__item__pic">
                                <img src="{{ asset('front_theme/img/trend/bs-2.jpg') }}" alt="">

                            </div>
                            <div class="trend__item__text">
                                <h6>Zip-pockets pebbled tote <br />briefcase</h6>
                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                                <div class="product__price">$ 59.0</div>
                            </div>
                        </div>
                        <div class="trend__item">
                            <div class="trend__item__pic">
                                <img src="{{ asset('front_theme/img/trend/bs-3.jpg') }}" alt="">
                            </div>
                            <div class="trend__item__text">
                                <h6>Round leather bag</h6>
                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                                <div class="product__price">$ 59.0</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="trend__content">
                        <div class="section-title">
                            <h4>Feature</h4>
                        </div>
                        <div class="trend__item">
                            <div class="trend__item__pic">
                                <img src="{{ asset('front_theme/img/trend/f-1.jpg') }}" alt="">
                            </div>
                            <div class="trend__item__text">
                                <h6>Bow wrap skirt</h6>
                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                                <div class="product__price">$ 59.0</div>
                            </div>
                        </div>
                        <div class="trend__item">
                            <div class="trend__item__pic">
                                <img src="{{ asset('front_theme/img/trend/f-2.jpg') }}" alt="">
                            </div>
                            <div class="trend__item__text">
                                <h6>Metallic earrings</h6>
                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                                <div class="product__price">$ 59.0</div>
                            </div>
                        </div>
                        <div class="trend__item">
                            <div class="trend__item__pic">
                                <img src="{{ asset('front_theme/img/trend/f-3.jpg') }}" alt="">
                            </div>
                            <div class="trend__item__text">
                                <h6>Flap cross-body bag</h6>
                                <div class="rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                                <div class="product__price">$ 59.0</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!-- Trend Section End -->

    <!-- Discount Section Begin -->
    <!-- <section class="discount">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 p-0">
                    <div class="discount__pic">
                        <img src="{{ asset('front_theme/img/discount.jpg') }}" alt="">
                    </div>
                </div>
                <div class="col-lg-6 p-0">
                    <div class="discount__text">
                        <div class="discount__text__title">
                            <span>Discount</span>
                            <h2>Summer 2019</h2>
                            <h5><span>Sale</span> 50%</h5>
                        </div>
                        <div class="discount__countdown" id="countdown-time">
                            <div class="countdown__item">
                                <span>22</span>
                                <p>Days</p>
                            </div>
                            <div class="countdown__item">
                                <span>18</span>
                                <p>Hour</p>
                            </div>
                            <div class="countdown__item">
                                <span>46</span>
                                <p>Min</p>
                            </div>
                            <div class="countdown__item">
                                <span>05</span>
                                <p>Sec</p>
                            </div>
                        </div>
                        <a href="#">Shop now</a>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!-- Discount Section End -->

    <!-- Services Section Begin -->
    <!-- <section class="services spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="services__item">
                        <i class="fa fa-car"></i>
                        <h6>Free Shipping</h6>
                        <p>For all oder over $99</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="services__item">
                        <i class="fa fa-money"></i>
                        <h6>Money Back Guarantee</h6>
                        <p>If good have Problems</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="services__item">
                        <i class="fa fa-support"></i>
                        <h6>Online Support 24/7</h6>
                        <p>Dedicated support</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="services__item">
                        <i class="fa fa-headphones"></i>
                        <h6>Payment Secure</h6>
                        <p>100% secure payment</p>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!-- Services Section End -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
    <script>
        $('.category-filter').click(function() {
            var val;
            if ($(this).attr('data-id')) {
                val = $(this).attr('data-id');
            } else {
                val = '';
            }

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'GET',
                url: '{{ route("product-filter", ":val") }}'.replace(':val', val),
                success: function(response) {

                    $(".productdisplay").html('');
                    $(".productdisplay").append(response.html);
                }


            })
        });
        
    </script>
@endsection
