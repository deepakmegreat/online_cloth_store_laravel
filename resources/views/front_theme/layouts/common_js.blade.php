<!-- Js Plugins -->
<script src="{{ asset('front_theme/js/jquery-3.3.1.min.js') }} "></script>
<script src="{{ asset('front_theme/js/bootstrap.min.js') }} "></script>
<script src="{{ asset('front_theme/js/jquery.magnific-popup.min.js') }} "></script>
<script src="{{ asset('front_theme/js/jquery-ui.min.js') }} "></script>
<script src="{{ asset('front_theme/js/mixitup.min.js') }} "></script>
<script src="{{ asset('front_theme/js/jquery.countdown.min.js') }} "></script>
<script src="{{ asset('front_theme/js/jquery.slicknav.js') }} "></script>
<script src="{{ asset('front_theme/js/owl.carousel.min.js') }} "></script>
<script src="{{ asset('front_theme/js/jquery.nicescroll.min.js') }} "></script>
<script src="{{ asset('front_theme/js/main.js') }} "></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $('.live-search').hide();
        $('#search').on('keyup',function(){
            $('.live-search').show();
            var search=$(this).val();
            $.ajax({
                type:'get',
                url:"{{ route('livesearch') }}",
                data:{'search':search},
                success:function(data){
                    $('.live-search').html(data);
                }
            });
        });
        $(".live-search").change(function() {
            var Searchvalue = $('.live-search').val();
            $('#search').val(Searchvalue);
            $('.live-search').hide();
        });
</script>
@yield('js')