<!DOCTYPE html>
<html lang="en">
@include('front_theme.layouts.common_header')

<body>
@include('front_theme.layouts.navbar')

@yield('content')

@include('front_theme.layouts.common_footer')
@include('front_theme.layouts.common_js')
</body>

</html>