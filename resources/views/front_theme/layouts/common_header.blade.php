
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Ashion Template">
    <meta name="keywords" content="Ashion, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Cookie&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{ asset('front_theme/css/bootstrap.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('front_theme/css/font-awesome.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('front_theme/css/elegant-icons.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('front_theme/css/jquery-ui.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('front_theme/css/magnific-popup.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('front_theme/css/owl.carousel.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('front_theme/css/slicknav.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('front_theme/css/style.css') }}" type="text/css">
    <link rel="icon" href="{{asset('backend/images/logo_new.png')}}" />
    @yield('css')
</head>