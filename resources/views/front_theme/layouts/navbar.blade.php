<!-- Page Preloder -->
<div id="preloder">
    <div class="loader"></div>
</div>

<!-- Offcanvas Menu Begin -->
<div class="offcanvas-menu-overlay"></div>
<div class="offcanvas-menu-wrapper">
    <div class="offcanvas__close"></div>
    <ul class="offcanvas__widget">
        <li><span class="icon_search search-switch"></span></li>
        <li><a href="#"><span class="icon_heart_alt"></span>
                <div class="tip">2</div>
            </a></li>
        <li><a href="#"><span class="icon_bag_alt"></span>
                <div class="tip">2</div>
            </a></li>
    </ul>
    <div class="offcanvas__logo">
        <a href="./index.html"><img src="{{ asset('front_theme/img/logo9.png') }} " alt="" height="40px" width="150px"></a>
    </div>
    <div id="mobile-menu-wrap"></div>
    <div class="offcanvas__auth">
        <a href="#">Login</a>
        <a href="#">Register</a>
    </div>
</div>
<!-- Offcanvas Menu End -->

<!-- Header Section Begin -->
<header class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-3 col-lg-2">
                <div class="header__logo">
                    <a href="{{ route('/') }}"><img src="{{ asset('front_theme/img/logo9.png') }}" alt="" height="50px" width="150px" ></a>
                </div>
            </div>
            <div class="col-xl-6 col-lg-7">
                <nav class="header__menu">
                    <?php
                    $currentPath = Route::currentRouteName();
                    ?>
                    <ul class="d-flex justify-content-center pt-2">

                        <li class="{{ $currentPath == 'index' ? 'active' : '' }}"><a href="{{ route('/') }}">Home</a></li>
                        <li class="{{ $currentPath == 'shop' ? 'active' : '' }}"><a href="{{ route('shop') }}">Shop</a>
                        </li>
                        
                        <li class="{{ $currentPath == 'contact' ? 'active' : '' }}"><a
                                href="{{ route('contact') }}">Contact</a></li>
                        @if (Auth::check())
                            <li id='drop1' class="{{ $currentPath == Auth::user()->name ? 'active' : '' }}"><a
                                    href="#">{{ Auth::user()->name }}<svg xmlns="http://www.w3.org/2000/svg"
                                        width="16" height="16" fill="currentColor" class="bi bi-chevron-down"
                                        viewBox="0 0 16 16">
                                        <path fill-rule="evenodd"
                                            d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z" />
                                    </svg></a>
                                <ul class="dropdown">

                                    <li class="{{ $currentPath == 'my-order' ? 'active' : '' }}"><a
                                            href="{{ route('my-order') }}">Orders</a></li>
                                    <li class="{{ $currentPath == 'wishlist' ? 'active' : '' }}"><a
                                            href="{{ route('wishlist') }}">Wishlist</a></li>

                                    <li class="{{ $currentPath == 'checkout' ? 'active' : '' }}"><a
                                                href="{{ route('checkout') }}">Checkout</a></li>        
                                    <li><a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        @else
                       
                        <li> 
                            <a href="{{ route('login') }}">Login</a>
                            {{-- <a href="{{ url('/register') }}">Register</a> --}}
                        </li>
                        <li> 
                            <a href="{{ route('register') }}">Register</a>
                        </li>
                    @endif
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>



                        {{-- <li><a href="#">Pages</a>
                                <ul class="dropdown">
                                    <li><a href="./product-details.html">Product Details</a></li>
                                    <li><a href="./shop-cart.html">Shop Cart</a></li>
                                    <li><a href="./checkout.html">Checkout</a></li>
                                    <li><a href="./blog-details.html">Blog Details</a></li>
                                </ul>
                            </li> --}}
                    </ul>
                </nav>
            </div>
            <div class="col-lg-3 ">
                <div class="header__right">
                    {{-- @if (!Auth::check())
                        <div class="header__right__auth d-flex">
                            <a href="{{ url('/login') }}">Login</a>
                            <a href="{{ url('/register') }}">Register</a>
                        </div>
                    @endif --}}
                    
                    <ul class="header__right__widget d-flex">
                    
                    <li  class="d-flex">
                        <form action="{{route('search')}}" method="POST" class="col-12">
                            @csrf
                            <div class="form-group d-flex">
                            <input type="search" name="search" id="search" class="form-control me-2 pb-0 pt-0" placeholder="search" value="{{$search ?? ''}}">
                            <button  type="submit" class="btn  btn-outline-success icon_search "></button>
                            </div>
                        </form>  
                    </li>
                          
                    <li class="pt-1"><a href="{{ route('wishlist') }}"><span class="icon_heart_alt"></span>
                            @if (Auth::user())
                                <div class="tip">{{ total_item_in_wishlist(Auth::user()->id) }}</div>
                            @else
                                <div class="tip">0</div>
                            @endif
                        </a></li>
                    <li class="pt-1"><a href="{{ route('cart') }}"><span class="icon_cart_alt"></span>
                            @if (Auth::user())
                                <div class="tip">{{ total_item_in_cart(Auth::user()->id) }}</div>
                            @else
                                <div class="tip">{{ count((array) session('cart')) }}</div>
                            @endif
                        </a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="canvas__open">
            <i class="fa fa-bars"></i>
        </div>
        <select type="text" class="form-control mb-4 live-search"  value="{{$search ?? ''}}">
        </select>
    </div>
</header>

<!-- Header Section End -->


