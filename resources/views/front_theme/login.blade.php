<!DOCTYPE html>
<html lang="en">
@include('front_theme.layouts.common_header')
<body>
@section('title')
    Login Page | StyleFushion
@endsection

<section>
        <div class="container">
            <form id="loginForm" action="{{ route('login') }}" class="checkout__form" method="post">
                @csrf
                @if (Session::has('message'))
                    <div class="alert alert-success" role="alert">
                        {{ Session::get('message') }}
                    </div>
                @endif
                <div class="row">
                    <div class="col-lg-4">

                    </div>
                    <div class="col-lg-4  p-5 shadow-lg" style="position: absolute; top: 50%;left: 50%;transform: translate(-50%, -50%); background-color: #eee">
                        <h5 class="text-center">Login</h5>
                        <div class="row">

                            <div class="col-lg-12">
                                <div class="checkout__form__input">
                                    <p>Email <span>*</span></p>
                                    <input class="rounded @error('email') is-invalid @enderror" type="email" name="email" id="email" placeholder="Enter Your email Address" value="{{ old('email') }}" autocomplete="email" autofocus>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                     @enderror
                                </div>

                                <div class="checkout__form__input">
                                    <p>Password <span>*</span></p>
                                    <input class="rounded @error('password') is-invalid @enderror" type="password" name="password" id="password" placeholder="Enter Your Password" autocomplete="current-password">
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                </div>

                            </div>
                            <div class="col-lg-12 mt-4 d-flex justify-content-end">
                                <input type="submit" value="Login" class="btn btn-dark">
                            </div>
                            @if (Route::has('forget.password.get'))
                                <a class="btn btn-link" href="{{ route('forget.password.get') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            @endif
                            <div class="col-lg-12 mt-4 d-flex justify-content-end">
                                <p class="checkout__form__input" >Does Not Have an Account ! <a href="{{ route('register')}}" class="checkout__form__input" >Register Now</a></p>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-4">

                    </div>
                </div>
            </form>
        </div>
    </section>
    @section('js')
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
        <script src="{{ asset('front_theme/js/custom_js/form_validation.js') }}"></script>
    @endsection

@include('front_theme.layouts.common_js')
</body>

</html>
