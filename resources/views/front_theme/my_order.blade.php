@extends('front_theme.layouts.master')

@section('title')
    MyOrders Page | StyleFushion
@endsection

@section('content')
<br><br><br><br>
<div class="page-style-a">
  <div class="container">
    <div class="page-intro">
        <h2 class="mx-auto">Your Orders</h2>
        {{-- <ul class="bread-crumb">
            <li class="has-separtor">
             <i class="ion ion-md-home"></i>
             <a href="index.html">Home</a>
            </li>
            <li class="is-marked">
                <a href="#">Orders</a>
            </li>
        </ul>     --}}
    </div>
  </div>
</div>
@if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
<div class="page-cart u-s-p-t-80">
    <div class="container mb-5">
        <div class="row">
            <table class="table table-striped table-orderless">
                <tr>
                    <th>Order Id</th>
                    <th>Ordered Products</th>
                    <th>Payment Method</th>
                    <th>Grand Total</th>
                    <th>Created On</th>
                    <th>Order Status</th>
                    <th>Action</th>
                </tr>

                @if (!empty($orders))
                    @foreach($orders as $order)
                    <tr>

                        <td><a href=" {{route('my-order',[$order['id']])}} "> {{ '#ORD-'. $order['id'] }}</a></td>
                        <td>
                        @foreach ($order['get_order_detail'] as $ordered_product)
                        {{$ordered_product['product_name'].'-'.$ordered_product['product_color'].'-'.$ordered_product['product_size']}}<br>
                        @endforeach
                        </td>
                        <td>{{strtoupper($order['payment_method'])}}</td>
                        <td>&#8377;{{$order['grand_total']}}</td>
                        <td>{{ date('Y-m-d', strtotime($order['created_at'])) }}</td>
                        <td>{{$order['order_status']}}</td>
                        @if($order['order_status'] != "Cancelled")
                            <td>
                                <form  method="post" action="{{ route('update-order-status') }}">
                                    @csrf
                                    <input type="hidden" name="order_id" value="{{ $order['id'] }}">
                                    <input type="submit" class="btn btn-primary" value="X"/>
                                </form>
                            </td
                        @else
                            <td><span class="btn btn-sm mb-1 btn-danger">Order Cancelled</span></td>
                        @endif
                    <tr>

                    @endforeach
                @else
                    <tr>
                        <td>You haven't placed any order yet</td>
                    </tr>
                @endif

            </table>
        </div>
    </div>
 </div><br><br><br><br><br><br><br><br>
 @endsection
