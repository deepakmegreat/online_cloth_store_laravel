@include('front_theme.layouts.common_header')
@include('front_theme.layouts.common_js')
@section('title')
    Home Page | StyleFushion
@endsection

<section class="h-100 gradient-custom">
  <div class="container py-5 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-lg-10 col-xl-8">
        <div class="card" style="border-radius: 10px;">
          <div class="card-header px-4 py-5">
            <h5 class="text-muted mb-0">Thanks for your Order, <span style="color: #a8729a;">{{ $order['first_name'] . " " . $order['last_name'] }}</span>!</h5>
          </div>
          <div class="card-body p-4">
            <div class="d-flex justify-content-between align-items-center mb-4">
              <p class="lead fw-normal mb-0" style="color: #a8729a;">Receipt</p>
              <p class="small text-muted mb-0">Order ID : {{ "#ORD-".$order['id'] }}</p>
            </div>
            <div class="card shadow-0 border mb-4">
              <div class="card-body">
                @if(isset($order_details) && !empty($order_details))
                      @foreach ($order_details as $details)
                    <div class="row">
                        <div class="col-md-2">
                          @foreach ($details['getproduct_detail'] as $detailproduct)
                              @if($detailproduct['image'] == 'defaultproduct.jpg')
                                  <img src="{{ asset('backend/images/uploads') . '/' . $detailproduct['image'] }}" alt="Product Image" width="50px" height="50px" class="img-fluid">
                              @else
                                  <img src="{{ asset('backend/images/uploads/products') . '/' . $detailproduct['image'] }}" alt="Product Image" width="50px" height="50px" class="img-fluid">
                              @endif
                          @endforeach
                        </div>
                        <div class="col-md-2 text-center d-flex justify-content-center align-items-center">
                          <p class="text-muted mb-0">{{ $details['product_name'] }}</p>
                        </div>
                        <div class="col-md-2 text-center d-flex justify-content-center align-items-center">
                          <p class="text-muted mb-0 small">{{ $details['product_color'] }}</p>
                        </div>
                        <div class="col-md-2 text-center d-flex justify-content-center align-items-center">
                          <p class="text-muted mb-0 small">{{ $details['product_size'] }}</p>
                        </div>
                        <div class="col-md-2 text-center d-flex justify-content-center align-items-center">
                          <p class="text-muted mb-0 small">{{ $details['product_qty'] }}</p>
                        </div>
                        <div class="col-md-2 text-center d-flex justify-content-center align-items-center">
                          <p class="text-muted mb-0 small">&#8377;{{$details['product_price'] }}</p>
                        </div>
                      </div>
                      <hr class="mb-4" style="background-color: #e0e0e0; opacity: 1;">
                      <br>
                      @endforeach
                @endif
              </div>
            </div>

          </div>
          <div class="card-footer border-0 px-4 py-5"
            style="background-color: #a8729a; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;">
            <h5 class="d-flex align-items-center justify-content-end text-white text-uppercase mb-0">Total
              paid: <span class="h2 mb-0 ms-2 ml-2">{{'Rs.'. $order['grand_total'] }}</span></h5>
            <a href="{{route('my-order')}}"><button class="btn btn-dark d-flex align-items-center justify-content-start text-white text-uppercase mb-0"><span class="h2 mb-0 ms-2 ml-2">Back</span></button></a>
            
          </div>
        </div>
      </div>
    </div>
    
  </div>
</section>
