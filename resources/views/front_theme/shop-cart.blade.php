@extends('front_theme.layouts.master')

@section('title')
    Cart Page | StyleFushion
@endsection

@section('content')
<!-- Breadcrumb Begin -->
    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__links">
                        <a href="{{ route('/') }}"><i class="fa fa-home"></i> Home</a>
                        <span>Shopping cart</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->
 <!-- Shop Cart Section Begin -->
    <section class="shop-cart spad">
        <div class="container">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if(session('error_message'))
                <div class="alert alert-danger">
                <a href="#" class="alert-link">Error: </a>{{ session('error_message') }}
                </div>
            @endif
            <div class="row">
                <div class="col-lg-12">
                    <div class="shop__cart__table">
                        <table>
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Colour</th>
                                    <th>Size</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $total = 0;  @endphp
                                @if(session('cart'))
                                    @if(count(session('cart')) > 0)
                                        @foreach (session('cart') as $id=>$details)
                                            @foreach ($details['color']  as $color => $size)
                                                @foreach ($size['size'] as $sizelabel => $sizename)
                                                @php   $total += $sizename['price'] * $sizename['quantity']; @endphp
                                                <tr data-id={{ $id }}>
                                                    <td class="cart__product__item">
                                                        @if($details['photo'] == 'defaultproduct.jpg')
                                                            <img src="{{ asset('backend/images/uploads') . '/' . $details['photo'] }}" alt="Product Image" width="100px" height="100px">
                                                        @else
                                                            <img src="{{ asset('backend/images/uploads/products') . '/' . $details['photo'] }}" alt="Product Image" width="100px" height="100px">
                                                        @endif
                                                        <div class="cart__product__item__title">
                                                            <h6>{{ $details['product_name'] }}</h6>
                                                            <div class="rating">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="cart__color">{{ $color }}</td>
                                                    <td class="cart__size">{{ $sizelabel }}</td>
                                                    <td class="cart__price">&#8377; {{ $sizename['price'] }}</td>
                                                    <td class="cart__quantity">
                                                        <div class="pro-qty">
                                                            <input type="text" value="{{ $sizename['quantity'] }}" class="quantity1" disabled/>
                                                        </div>
                                                    </td>
                                                    <td class="cart__total">&#8377; {{ $sizename['price'] * $sizename['quantity'] }}</td>
                                                    <td class="cart__close"><span class="icon_close cart_remove" data-id={{ $id }}></span></td>
                                                </tr>
                                                @endforeach
                                            @endforeach
                                        @endforeach
                                    @else
                                        <tr colspan="5" style="color:red">
                                            <td>Cart is Empty</td>
                                        </tr>
                                    @endif
                            @else
                                      @if(Auth::user())
                                          @php
                                              $cart_data = find_cart_login_user(Auth::user()->id);
                                              if(count($cart_data) > 0){
                                              foreach($cart_data as $details){
                                                  $total += $details['Price'] * $details['quantity'];
                                          @endphp
                                                <tr data-id={{ $details['id'] }}>
                                                    <td class="cart__product__item">
                                                        @if($details['photo'] == 'defaultproduct.jpg')
                                                            <img src="{{ asset('backend/images/uploads') . '/' . $details['photo'] }}" alt="Product Image" width="100px" height="100px">
                                                        @else
                                                            <img src="{{ asset('backend/images/uploads/products') . '/' . $details['photo'] }}" alt="Product Image" width="100px" height="100px">
                                                        @endif
                                                        <div class="cart__product__item__title">
                                                            <h6>{{ $details['product_name'] }} </h6>
                                                            <div class="rating">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="cart__color">{{ $details['color'] }}</td>
                                                    <td class="cart__size">{{ $details['size'] }}</td>
                                                    <td class="cart__price">&#8377; {{ $details['Price'] }}</td>
                                                    <td class="cart__quantity">
                                                        <div class="pro-qty">
                                                            <input type="text" value="{{ $details['quantity'] }}" class="quantity1" disabled/>
                                                        </div>
                                                    </td>
                                                    <td class="cart__total">&#8377; {{ $details['Price'] * $details['quantity'] }}</td>
                                                    <td class="cart__close"><span class="icon_close cart_remove" data-id={{ $details['id'] }}></span></td>
                                                </tr>
                                            @php
                                                }//end foreach
                                                }//end if
                                                else{
                                            @endphp
                                                    <tr colspan="5" style="color:red">
                                                            <td>Cart is Empty</td>
                                                    </tr>
                                            @php
                                                }//end else
                                            @endphp
                                      @endif

                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="cart__btn">
                        <a href="{{ route("shop") }}">Continue Shopping</a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    {{-- <div class="cart__btn update__btn">
                        <a href="#"><span class="icon_loading"></span> Update cart</a>
                    </div> --}}
                    <div class="cart__total__procced">
                        <h6>Cart total</h6>
                        <ul>
                            {{-- <li>Subtotal <span>$ 750.0</span></li> --}}
                            <li>Total <span>&#8377; {{ $total }}</span></li>
                        </ul>
                        <a href="{{ route("checkout") }}" class="primary-btn">Proceed to checkout</a>
                    </div>
                </div>
            </div>
            {{-- <div class="row">
                <div class="col-lg-6">
                    <div class="discount__content">
                        <h6>Discount codes</h6>
                        <form action="#">
                            <input type="text" placeholder="Enter your coupon code">
                            <button type="submit" class="site-btn">Apply</button>
                        </form>
                    </div>
                </div>
                <div class="col-lg-4 offset-lg-2">
                    <div class="cart__total__procced">
                        <h6>Cart total</h6>
                        <ul>
                            <li>Total <span></span></li>
                        </ul>
                        <a href="#" class="primary-btn">Proceed to checkout</a>
                    </div>
                </div>
            </div> --}}
        </div>
    </section>
    <!-- Shop Cart Section End -->
@endsection

@section('js')
<script>
    $('.cart__quantity').on('click', '.qtybtn', function() {
        $.ajax({
            url:'{{ route("update_cart") }}',
            method:"post",
            data:{
                _token: '{{ csrf_token() }}',
                id: $(this).parents('tr').attr("data-id"),
                quantity:$(this).closest('.pro-qty').find('.quantity1').val(),
                color: $(this).parents('tr').find('.cart__color').text(),
                size: $(this).parents('tr').find('.cart__size').text()
            },
            success: function(response){
                    window.location.reload();
            }
        });
    });

    $('.cart_remove').click(function(e){
        e.preventDefault();
        var ele = $(this);
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {

                    $.ajax({
                        url:'{{ route("remove_from_cart") }}',
                        method:"post",
                        data:{
                            _token: '{{ csrf_token() }}',
                            id: ele.attr("data-id"),
                            color: ele.parents('tr').find('.cart__color').text(),
                            size: $(this).parents('tr').find('.cart__size').text()

                        },
                        success: function(response){
                            swal("Poof! Your imaginary file has been deleted!", {
                            icon: "success",
                            });
                            window.location.reload();

                            }
                    });
                } else {
                    swal("Your imaginary file is safe!");
                }
            });
    });
</script>
@endsection

