@extends('front_theme.layouts.master')

@section('title')
    Wishlist Page | StyleFushion
@endsection

@section('content')
<!-- Breadcrumb Begin -->
    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__links">
                        <a href="{{ route('/') }}"><i class="fa fa-home"></i> Home</a>
                        <span>Wishlist</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->
 <!-- Shop Cart Section Begin -->
    <section class="shop-cart spad">
        <div class="container">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif

            @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
            <div class="row">
                <div class="col-lg-12">
                    <div class="shop__cart__table">
                        <table>
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Price</th>
                                    <th>Color</th>
                                    <th>Size</th>
                                    <th>Quantity</th>
                                    <th>Stock</th>

                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $total = 0;  @endphp

                                @if(Auth::user())
                                    @php
                                      $wishlist_data = find_wishlist_login_user(Auth::user()->id);
                                      if(count($wishlist_data) > 0){
                                      foreach($wishlist_data as $details){
                                          $total += $details['price'] * $details['quantity'];
                                          $product_id=$details['product_id'];
                                    @endphp
                                        <tr data-id={{ $details['id'] }}>
                                            <td class="cart__product__item">
                                               
                                                <a href="{{ route('product-detail',[$product_id]) }}" style="cursor: pointer;">
                                                @if($details['photo'] == 'defaultproduct.jpg')
                                                    <img src="{{ asset('backend/images/uploads') . '/' . $details['photo'] }}" alt="Product Image" width="100px" height="100px">
                                                @else
                                                    <img src="{{ asset('backend/images/uploads/products') . '/' . $details['photo'] }}" alt="Product Image" width="100px" height="100px">
                                                @endif
                                                </a>
                                                <div class="cart__product__item__title">
                                                    <a href="{{ route('product-detail',[$product_id]) }}" style="cursor: pointer;">
                                                    <h6>{{ $details['product_name'] }} </h6>
                                                </a>
                                                    <div class="rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                </div>
                                            </td>

                                            <td class="cart__price">&#8377; {{ $details['price'] }}</td>
                                            <td class="cart__color"> {{ $details['color'] }}</td>
                                            <td class="cart__size"> {{ $details['size'] }}</td>
                                            <td class="cart__quantity">
                                                {{ $details['quantity'] }}
                                            </td>
                                            <td class="stock_update">
                                                @php
                                                $quantity = checkForStockAvailability($details['product_id']);
                                                @endphp

                                                @if($quantity > 0)
                                                    <span class="text-success">Stock Available</span>
                                                @else
                                                    <span class="text-danger">Stock Unvailable</span>
                                                @endif


                                            </td>


                                            <td class="cart__close"><span class="icon_close wishlist_remove" data-id={{ $details['id'] }}></span></td>
                                        </tr>
                                    @php
                                        }//end foreach
                                        }//end if
                                        else{
                                    @endphp
                                        <tr colspan="5" style="color:red">
                                                <td>Wishlist is Empty</td>
                                        </tr>
                                    @php
                                        }//end else
                                    @endphp
                                @endif


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="cart__btn">
                        <a href="{{ route('shop') }}">Continue Shopping</a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    {{-- <div class="cart__btn update__btn">
                        <a href="#"><span class="icon_loading"></span> Update cart</a>
                    </div> --}}
                    <div class="cart__total__procced">
                        <h6>Your Wishes Value</h6>
                        <ul>
                            {{-- <li>Subtotal <span>$ 750.0</span></li> --}}
                            <li>Total <span>&#8377; {{ $total }}</span></li>
                        </ul>
                        @if($total!=0)
                            <a href="{{ route('add-cart') }}" class="primary-btn">Add to Cart</a>
                        @endif
                    </div>
                </div>
            </div>
            {{-- <div class="row">
                <div class="col-lg-6">
                    <div class="discount__content">
                        <h6>Discount codes</h6>
                        <form action="#">
                            <input type="text" placeholder="Enter your coupon code">
                            <button type="submit" class="site-btn">Apply</button>
                        </form>
                    </div>
                </div>
                <div class="col-lg-4 offset-lg-2">
                    <div class="cart__total__procced">
                        <h6>Cart total</h6>
                        <ul>
                            <li>Total <span></span></li>
                        </ul>
                        <a href="#" class="primary-btn">Proceed to checkout</a>
                    </div>
                </div>
            </div> --}}
        </div>
    </section>
    <!-- Shop Cart Section End -->
@endsection

@section('js')
<script>

    $('.wishlist_remove').click(function(e){
        e.preventDefault();
        var ele = $(this);
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {

                    $.ajax({
                        url:'{{ route("remove_from_wishlist") }}',
                        method:"post",
                        data:{
                            _token: '{{ csrf_token() }}',
                            id: ele.attr("data-id"),
                            color: ele.parents('tr').find('.cart__color').text(),
                            size: $(this).parents('tr').find('.cart__size').text()

                        },
                        success: function(response){
                            console.log(response);
                            swal("Poof! Your imaginary file has been deleted!", {
                                icon: "success",
                            });
                            window.location.reload();

                        }
                    });
                }
                else {
                    swal("Your imaginary file is safe!");
                }
            });
    });
</script>
@endsection

