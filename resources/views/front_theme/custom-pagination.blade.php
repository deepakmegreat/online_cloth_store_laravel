<div class="col-lg-12 text-center">
    <div class="pagination__option">
        
        @if ($products->currentPage() != 1)
            <a href="{{ $products->url($products->currentPage() - 1) }}"><i class="fa fa-angle-left"></i></a>
        @endif
        @for ($i = 1; $i <= $products->lastPage(); $i++)
            <a href="{{ $products->url($i) }}" @if ($i == $products->currentPage()) class="active" @endif>{{ $i }}</a>
        @endfor
        @if ($products->currentPage() != $products->lastPage())
            <a href="{{ $products->url($products->currentPage() + 1) }}"><i class="fa fa-angle-right"></i></a>
        @endif
    </div>
</div>
