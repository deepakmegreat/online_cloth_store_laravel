@extends('front_theme.layouts.master')

@section('title')
    Checkout Page | StyleFushion
@endsection

@section('content')
<!-- Breadcrumb Begin -->
    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__links">
                        <a href="{{ route('/') }}"><i class="fa fa-home"></i> Home</a>
                        <span>checkout</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Breadcrumb End -->

<!-- Checkout Section Begin -->
    <section class="checkout spad">
        <div class="container">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if(session('error_message'))
                <div class="alert alert-danger">
                <a href="#" class="alert-link">Error: </a>
                <ul style="list-style-type: none">
                    @foreach(explode('<br>', session()->get('error_message')) as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
                </div>
            @endif
            @if (!auth()->check())
             <a class="primary-btn" href="{{ route('login') }}?page=checkout">Login</a>
             <div class="m-3">
                OR
             </div>
            @endif
            <form id="checkOut" action="{{ route('place-order') }}" class="checkout__form" method="post">
                @csrf
                <div class="row">
                    <div class="col-lg-8">
                        <h5>Billing detail</h5>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="checkout__form__input">
                                    <p>First Name <span>*</span></p>
                                    <input type="text" name="firstname">
                                    <span class="text-danger">@error('firstname'){{$message}}@enderror</span>

                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="checkout__form__input">
                                    <p>Last Name <span>*</span></p>
                                    <input type="text" name="lastname">
                                    <span class="text-danger">@error('lastname'){{$message}}@enderror</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="checkout__form__input">
                                    <p>City <span>*</span></p>
                                    <input type="text" name="city">
                                    <span class="text-danger">@error('city'){{$message}}@enderror</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                 <div class="checkout__form__input">
                                    <p>State <span>*</span></p>
                                    <input type="text" name="state">
                                    <span class="text-danger">@error('state'){{$message}}@enderror</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                 <div class="checkout__form__input">
                                    <p>Country <span>*</span></p>
                                    <input type="text" name="country">
                                    <span class="text-danger">@error('country'){{$message}}@enderror</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="checkout__form__input">
                                    <p>Phone <span>*</span></p>
                                    <input type="phone" name="phone">
                                    <span class="text-danger">@error('phone'){{$message}}@enderror</span>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="checkout__form__input">
                                    <p>Address <span>*</span></p>
                                    <textarea rows="3" class="form-control" name="address"></textarea>
                                    <span class="text-danger">@error('address'){{$message}}@enderror</span>
                                </div>

                                <div class="checkout__form__input">
                                    <p>Postcode/Zip <span>*</span></p>
                                    <input type="text" name="postcode">
                                    <span class="text-danger">@error('postcode'){{$message}}@enderror</span>
                                </div>
                            </div>

                        </div>
                    </div>
                        <div class="col-lg-4">
                            <div class="checkout__order">
                                <h5>Your order</h5>
                                <div class="checkout__order__product">
                                    <table>
                                        <tr>
                                            <th style="width:300px">Products</th>
                                            <th style="width:20px">Total</th>
                                        </tr>
                                        @php $total = 0;  @endphp
                                        @if (session('cart'))
                                        @foreach (session('cart') as $id=>$details)
                                            @foreach ($details['color']  as $color => $size)
                                                @foreach ($size['size'] as $sizelabel => $sizename)
                                                 @php   $total += $sizename['price'] * $sizename['quantity']; @endphp
                                                <tr>
                                                    <td>{{ $details['product_name'] }} - {{ $sizename['price'] }} * {{ $sizename['quantity'] }} - {{ $color }} - {{ $sizelabel }}</td>
                                                    <td>{{ $sizename['price'] * $sizename['quantity'] }}</td>
                                                </tr>
                                                 @endforeach
                                                @endforeach
                                        @endforeach
                                        @else
                                            @if(Auth::user())
                                            @php
                                              $cart_data = find_cart_login_user(Auth::user()->id);
                                              foreach($cart_data as $details){
                                                  $total += $details['Price'] * $details['quantity'];
                                            @endphp
                                                    <tr>
                                                        <td>{{ $details['product_name'] }} - {{ $details['Price'] }}  * {{$details['quantity']}} - {{ $details['color'] }} - {{ $details['size'] }} </td>
                                                        <td>{{ $details['Price'] * $details['quantity'] }}</td>
                                                    </tr>
                                            @php
                                                }
                                            @endphp
                                            @endif
                                        @endif
                                    </table>

                                </div>
                                <div class="checkout__order__total">
                                    <ul>
                                        <li>Total <span>{{ $total }}</span></li>
                                    </ul>
                                </div>
                                <div class="checkout__order__widget">
                                    <label for="payment_gateway">
                                        Cash On Delivery
                                        <input type="radio" id="payment_gateway" name="payment_gateway" value="cod" checked>
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                @if(Auth::user() && !session('error_message'))
                                    <button type="submit" class="site-btn" name="btnsubmit" value="submit">Place oder</button>
                                @endif
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        <!-- Checkout Section End -->

@endsection

@section('js')
<script src="{{ asset('front_theme/js/jquery.validate.min.js') }}"></script>

<script src="{{ asset('front_theme/js/custom_js/form_validation.js') }}"></script>
@endsection
