@extends('front_theme.layouts.master')

@section('title')
    Order Details Page | StyleFushion
@endsection

@section('content')
<br><br><br><br>

  <div class="container">
    <div class="page-intro">
        <h2 class="mx-auto">Order Details</h2>
    </div>    
  </div>
</div> 

<div class="page-cart u-s-p-t-80">
    <div class="container">
        <div class="row">
            <table class="table table-striped table-orderless">
                <tr>
                    <th>Order Detals</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                     
                </tr>   
               
                <tr>
                    <td>order date</td>
                    <td> {{date('Y-m-d', strtotime($orders['created_at']))}}</td>
                <tr>
                    
                    <td>Order Total</td>
                    <td>{{'Rs.'.$orders['grand_total']}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                <tr>  
               
               </table>   
               
               <table class="table table-striped table-orderless">
                <tr>
                    
                    <th>Product Name</th>
                    <th>Product Size</th>
                    <th>Product Color</th>
                    <th>Product Qty</th>
                    <th>Product Price</th> 
                </tr>   
                @foreach($orders['get_order_detail'] as $order)
               
                <tr>
                    <td>{{$order['product_name']}}</td>
                    <td>{{$order['product_size']}}</td>
                    <td>{{$order['product_color']}}</td>
                    <td>{{$order['product_qty']}}</td>
                    <td>&#8377;{{$order['product_price']}}</td>

                <tr>
            
             @endforeach   
            </table>
            
            <table class="table table-striped table-orderless">
                <tr>
                    <th>Delivery Address</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                     
                </tr>   
               
                <tr>
                    <td>Name</td>
                    <td>{{$orders['first_name'].' '.$orders['last_name']}}</td>
                    
                <tr>
                <tr>
                    <td>Address</td>
                    <td>{{$orders['address']}}</td>
                    
                <tr>  
                <tr>
                    <td>City</td>
                    <td>{{$orders['city']}}</td>

                <tr>  
                <tr>
                    <td>State</td>
                    <td>{{$orders['state']}}</td>

                <tr> 
                <tr>
                    <td>Country</td>
                    <td>{{$orders['country']}}</td>

                <tr> 
                <tr>
                    <td>Pincode</td>
                    <td>{{$orders['postcode']}}</td>

                <tr> 
                <tr>
                    <td>Mobile No:</td>
                    <td>{{$orders['phone']}}</td>

                <tr>

               </table>   
               




        </div>   
    </div>    
 </div><br><br><br><br><br><br><br><br>
 @endsection