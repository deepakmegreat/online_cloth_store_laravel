@extends('front_theme.layouts.master')

@section('title')
    Search Page | StyleFushion
@endsection

@section('content')

<!-- Product Section Begin -->
<section class="product spad">
    <div class="container">
        <div class="row">
           
           @if( count($products)!=0)
           <div class="col-lg-4 col-md-4">
                <div class="section-title">
                    <h4>Product You Searched</h4>
                </div>
            </div>
            @else
            <div class="col-lg-4 col-md-4">
                <h4 class="text-danger">!!Product Not Found!!</h4>
            </div>
            @endif
        </div>

        <div class="row property__gallery">
            @foreach ($products as $product)
            <div class="col-lg-3 col-md-4 col-sm-6 mix women">
                    <?php
                        if (file_exists(public_path('backend/images/uploads/products/'. $product->image)) && $product->image != null && $product->image != '') {
                            $image = asset('backend/images/uploads/products/' . $product->image);
                        } else {
                            $image = asset('backend/images/uploads/defaultproduct.jpg');
                        }
                    ?>
                <div class="product__item">
                    <div class="product__item__pic set-bg" data-setbg="{{ $image }}">
                        @if($product->quantity == 0)
                            <div class="label stockout">out of stock</div>
                        @else
                            <div class="label new">New</div>
                        @endif
                        <ul class="product__hover">
                            <li><a href="{{ $image }}" class="image-popup" ><span class="arrow_expand"></span></a></li>
                            {{-- <li><a href="#"><span class="icon_heart_alt"></span></a></li>
                            <li><a href="#"><span class="icon_bag_alt"></span></a></li> --}}
                        </ul>
                    </div>

                    <div class="product__item__text">
                       <h6><a href="{{ route('product-detail',[$product->id]) }}" >{{ $product->name }}</a></h6>
                        <div class="rating">
                            {{-- <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i> --}}
                        </div>
                        <div class="product__price">&#8377; {{ $product->price }}</div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        {{-- <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 d-flex justify-content-end">
                <a href="{{route('shop')}}" class="site-btn btn">View more</a>
            </div>
        </div> --}}
        
    <div>
</section>
@endsection

<!-- Product Section End -->
