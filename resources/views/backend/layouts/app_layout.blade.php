<!DOCTYPE html>
<html lang="en">

<!-- Start Header  -->

@include('backend.includes.header')

<!-- End Header -->
<body>
	<div class="main-wrapper">

		<!-- Start Sidebar -->

        @include('backend.includes.sidebar')

		<!-- End Sidebar -->

		<div class="page-wrapper">

			<!-- Start Body Header -->

            @include('backend.includes.body_header')

			<!-- End Body Header -->

			@yield('section')

			<!-- Start Body Footer -->
            @include('backend.includes.body_footer')
			<!-- End Body Footer -->

		</div>
	</div>

    <!-- Start Footer -->
    @include('backend.includes.footer')
    <!-- End Footer -->

</body>
</html>
