@extends('backend.layouts.app_layout')


@section('section')
    <div class="page-content">
        @if (session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}

            </div>
        @endif

        <nav class="page-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="text-dark">Home  &nbsp;</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.color') }}" class="text-dark">Color  &nbsp;</a></li>
                <li class="breadcrumb-item">Add Color</li>
        </nav>
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Add Color</h4>

                        <form id="color" action="{{ route('admin.color-add') }}" method="POST">
                            @csrf
                            <div class="mb-3">
                                <label for="name" class="form-label">Color Name</label>
                                <input id="name" class="form-control" name="colour" type="text" value="{{old('colour')}}">
                                <span class="text-danger">@error('colour'){{$message}}@enderror</span>
                            </div>

                            <div class="mb-3">
                                <label for="code" class="form-label">Color Code</label>
                                <input id="code" class="form-control" name="colour_code" type="text"  
                                value="{{old('colour')}}">
                                <span class="text-danger">@error('colour_code'){{$message}}@enderror</span>
                            </div>

                            <input class="btn btn-primary" type="submit" value="Submit">
                            <a href="{{route('admin.color')}}"><input class="btn btn-secondary"  type="button" value="Cancel"></a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
        @endsection
