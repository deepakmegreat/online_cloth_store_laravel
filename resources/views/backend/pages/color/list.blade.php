@extends('backend.layouts.app_layout')

@section("css")
<style>
    .select2-selection{
        border: 1px solid #e9ecef !important;
        height: 37px !important;
    }
    .select2-selection__rendered{
        padding: 5px !important;
    }
    .select2-selection__arrow{
        margin: 5px !important;
    }
</style>
@endsection

@section('section')
    <div class="page-content">

        <nav class="page-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="text-dark">Home &nbsp;</a></li>
                <li class="breadcrumb-item">Color</li>
        </nav>

        <!-- Accordio for search filter -->
        <div class="row mb-4">
            <div class="col-md-12">
                <div class="w-100 bg-primary text-light py-2 px-4 d-flex justify-content-between align-items-center rounded-2 search-box"
                    type="button" data-toggle="collapse" data-target="#search-card" aria-expanded="false"
                    aria-controls="search-card">
                    <h5>Search Colors</h5>
                    <i class="link-icon" data-feather="arrow-down"></i>
                </div>
                <div class="card w-100 collapse" id="search-card">
                    <form action="{{ route('admin.color') }}" type="get" id="searchColorForm">
                        <!-- @csrf -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label for="colorName" class="form-label">Color Name</label>
                                    <select class="form-select" name="color_name" id="colorName" style="width: 100%;">
                                        <option value="" selected>All</option>
                                        @if(isset($color) && !empty($color)){
                                            @foreach($color as $key => $item)
                                            <option value="{{$item->colour}}" <?php if($request->color_name==$item->colour){echo "selected";} ?> >{{$item->colour}}</option>
                                            @endforeach 
                                        }
                                        @endif      
                                    </select>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="colorCode" class="form-label">Color Code </label>
                                    <select class="form-select" name="color_code" id="colorCode" style="width: 100%;">
                                        <option value="" selected>All</option>
                                        @if(isset($color) && !empty($color)){
                                            @foreach($color as $key => $item)
                                            <option value="{{$item->colour_code}}"  <?php if($request->color_code==$item->colour_code){echo "selected";} ?> >{{$item->colour_code}}</option>
                                            @endforeach 
                                        }   
                                        @endif   

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <input type="submit" name="search_submit"
                                class="bg-primary text-light px-4 py-1 rounded-2 border-0" value="search">
                        </div>
                    </form>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="row d-flex justify-content-center align-items-center mb-3">
                            <div class="col-md-6">
                                <h6 class="card-title mt-3">Color Table</h6>
                            </div>
                            <div class="col-md-6">
                                <a href="color-add">
                                    <button type="button" class="btn btn-primary mb-1 mb-md-0 float-end">
                                        Add Color
                                    </button>
                                </a>
                            </div>
                        </div>
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                    aria-label="btn-close"></button>
                            </div>
                        @endif
                        <div class="table-responsive">
                            @if (count($colors) > 0)
                            <table id="dataTableExample" class="table">@csrf
                                <thead>
                                    <tr>
                                        <th>@sortablelink('id', 'ID')</th>
                                        <th>@sortablelink('colour', 'Colours')</th>
                                        <th>@sortablelink('colour_code', 'colour_code')</th>
                                        <th class="text-primary">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($colors as $key => $color)
                                        <tr>
                                            <td>{{ $colors->firstItem() + $key }}</td>
                                            <td>{{ $color['colour'] }}</td>
                                            <td>{{ $color['colour_code'] }}</td>

                                            <td>

                                                <a href="color-edit/{{ $color['id'] }}"><span class="m-1 text-warning"> <i
                                                            data-feather="edit"></i></span></a>

                                                <a class="color_remove" data-id="{{ $color['id'] }}">
                                                    <span class="m-1 text-danger"><i data-feather="trash-2"></i>
                                                    </span></a>

                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @else
                            <div class="border-top">
                                <h4 align="center" style="padding : 20px;">No Record Found.</h4>
                            </div>
                             @endif
                            <br><br>
                            {!! $colors->appends(Request::except('page'))->render() !!}
                            {{-- {{ $colors->links() }} --}}
                        </div>


                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $(".search-box").click(function() {
                $('#search-card').toggle('slow');
            });

            $("#colorName, #colorCode").select2();

            $('.color_remove').click(function(e) {
                e.preventDefault();
                var ele = $(this);
                swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover this color!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {

                            $.ajax({
                                url: '{{ route('admin.delete-color') }}',
                                method: "post",
                                data: {
                                    _token: '{{ csrf_token() }}',
                                    id: ele.attr("data-id"),
                                },
                                success: function(response) {
                                    swal("Poof! Your color has been deleted!", {
                                        icon: "success",
                                    });
                                    window.location.reload();
                                }
                            });
                        } else {
                            swal("Your color is safe!");
                        }
                    });
            });
        })
    </script>
@endsection
