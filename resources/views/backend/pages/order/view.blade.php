<?php
  use App\Models\Orders;
?>
@extends('backend.layouts.app_layout')
@section('section')
<div class="page-content">
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="text-dark">Home  &nbsp;</a></li>
            <li class="breadcrumb-item"><a href="{{ route('admin.view-order') }}" class="text-dark">Order  &nbsp;</a></li>
            <li class="breadcrumb-item">View Order</li>
    </nav>
<div class="row profile-body">
    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    <div class="row">
      <div class="col-sm-6">
        <div class="card rounded">
          <div class="card-body">
            <div class="d-flex align-items-center justify-content-between mb-2">
              <h6 class="card-title mb-0">
                <b>Orders Details</b>
              </h6>
            </div>
            <hr>
            <div class="row col-md-12">
              <div class="col-md-12">
                <label class="fw-bolder mb-0 text-uppercase">Order ID:</label> #ORD-{{ $orders_detail['id']}}
              </div>
              <div class="col-md-12">
                <label class="fw-bolder mb-0 text-uppercase">Order Date:</label>
                {{ date('d-m-Y',strtotime($orders_detail['created_at'])) }}
              </div>
              <div class="col-md-12">
                <label class="fw-bolder mb-0 text-uppercase">Order Status:</label>
                {{ $orders_detail['order_status'] }}
              </div>
              <div class="col-md-12">
                <label class="fw-bolder mb-0 text-uppercase">Order Total:</label>
                Rs. {{ $orders_detail['grand_total'] }}
              </div>
              <div class="col-md-12">
                <label class="fw-bolder mb-0 text-uppercase">Payment Method:</label>
                {{ strtoupper($orders_detail['payment_method']) }}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card rounded">
          <div class="card-body">
            <div class="d-flex align-items-center justify-content-between mb-2">
              <h6 class="card-title mb-0">
                <b>Customer Details</b>
              </h6>
            </div>
            <hr>
            <div class="row col-md-12">
              <div class="col-md-12">
                <label class="fw-bolder mb-0 text-uppercase">Name:</label>
                {{ $user_detail['name']}}
              </div>
              <div class="col-md-12">
                <label class="fw-bolder mb-0 text-uppercase">Email</label>
                {{ $user_detail['email']}}
              </div>
            </div>
          </div>
        </div>
      </div>
</div>
    <div class="row mt-3">
      <div class="col-sm-6">
        <div class="card rounded">
          <div class="card-body">
            <div class="d-flex align-items-center justify-content-between mb-2">
              <h6 class="card-title mb-0">
                <b>Delivery Details</b>
              </h6>
            </div>
            <hr>
            <div class="row col-md-12">
              <div class="col-md-12">
                <label class="fw-bolder mb-0 text-uppercase">Name:</label>
                {{ $orders_detail['first_name'] . " ". $orders_detail['last_name']}}
              </div>
              <div class="col-md-12">
                <label class="fw-bolder mb-0 text-uppercase">Address</label>
                {{ $orders_detail['address']}}
              </div>
              <div class="col-md-12">
                <label class="fw-bolder mb-0 text-uppercase">City</label>
                {{ $orders_detail['city']}}
              </div>
              <div class="col-md-12">
                <label class="fw-bolder mb-0 text-uppercase">State:</label>
                {{ $orders_detail['state']}}
              </div>
              <div class="col-md-12">
                <label class="fw-bolder mb-0 text-uppercase">Country:</label>
                {{ $orders_detail['country']}}
              </div>
              <div class="col-md-12">
                <label class="fw-bolder mb-0 text-uppercase">Pincode:</label>
                {{ $orders_detail['postcode']}}
              </div>
              <div class="col-md-12">
                <label class="fw-bolder mb-0 text-uppercase">Mobile:</label>
                {{ $orders_detail['phone']}}
              </div>
              <div class="col-md-12">
                <label class="fw-bolder mb-0 text-uppercase">Email:</label>
                {{ $orders_detail['email']}}
              </div>
            </div>
          </div>
        </div>
    </div>
        <div class="col-sm-6">
          <div class="card rounded">
            <div class="card-body">
              <div class="d-flex align-items-center justify-content-between mb-2">
                <h6 class="card-title mb-0">
                  <b>Update Order Status</b>
                </h6>
              </div>
              <hr>
              <?php
                  $order = Orders::select('order_status')->where('id',$orders_detail['id'])->first()->toArray();
                  $order_status = $order['order_status'];
                  if($order_status != 'Cancelled'){
              ?>
              <div class="row col-md-12">
                    <form action="{{ route('admin.update-order-status') }}" method="post">
                        @csrf
                    <input type="hidden" name="order_id" value="{{ $orders_detail['id'] }}">
                        <select class="form-control" name="sltstatus">
                            @foreach($orderStatuses as $status)
                                <<option value="{{ $status['name'] }}" @if(!empty($orders_detail['order_status']) && $orders_detail['order_status']== $status['name']) selected="" @endif
                                >{{ $status['name'] }}</option>
                            @endforeach
                        </select>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
              </div>
              <?php

                }else{
                    echo "Order Cancelled";
                  }
              ?>
            </div>
          </div>
        </div>
    </div>

    <div class="row mt-3">
      <div class="col-sm-12">
        <div class="card rounded">
          <div class="card-body">
            <div class="d-flex align-items-center justify-content-between mb-2">
              <h6 class="card-title mb-0">
                <b>Ordered Product</b>
              </h6>
            </div>
            <hr>
            <div class="row col-md-12">
            <div class="table-responsive">
                <table id="dataTableExample" class="table">
                    <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Product Color</th>
                            <th>Product Size</th>
                            <th>Product Price</th>
                            <th>Product Qty</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($orders_detail['get_order_detail'] as $detail)
                            <tr>
                                <td>{{ $detail['product_name'] }}</td>
                                <td>{{ $detail['product_color'] }}</td>
                                <td>{{ $detail['product_size'] }}</td>
                                <td>Rs. {{ $detail['product_price'] }}</td>
                                <td>{{ $detail['product_qty'] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div><br>
            </div>
          </div>
        </div>
    </div>
    </div>
</div>
</div>
@endsection
