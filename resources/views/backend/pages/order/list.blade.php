@extends('backend.layouts.app_layout')

@section("css")
<style>
    .select2-selection__choice {
        background-color: #0d6efd !important;
    }
    .select2-selection{
        border: 1px solid #e9ecef !important;
        height: 37px !important;
    }
    .select2-selection__rendered{
        padding: 5px !important;
    }
    .select2-selection__arrow{
        margin: 5px !important;
    }
</style>
@endsection

@section('section')
    <div class="page-content">

        <nav class="page-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="text-dark">Home  &nbsp;</a></li>
                <li class="breadcrumb-item">Order</li>
        </nav>

        <!-- Accordio for search filter -->
        <div class="row mb-4">
            <div class="col-md-12">
                <div class="w-100 bg-primary text-light py-2 px-4 d-flex justify-content-between align-items-center rounded-2 search-box" type="button" data-toggle="collapse" data-target="#search-card" aria-expanded="false" aria-controls="search-card">
                    <h5>Search Orders</h5>
                    <i class="link-icon" data-feather="arrow-down"></i>
                </div>
                <div class="card w-100 collapse" id="search-card">
                    <form action="{{route('admin.view-order')}}" type="get" id="searchProductForm">
                        <!-- @csrf -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4 form-group">
                                    <label for="customerName" class="form-label">Customer Name</label>
                                    <select type="text" class="form-control mb-4" id="customerName" name="name" style="width: 100%;">
                                        <option value="" selected>All</option>
                                        @if(isset($users_name) && !empty($users_name)){
                                            @foreach($users_name as $username)
                                            <option value="{{$username->first_name.' '.$username->last_name}}" <?php if($request->name == $username->first_name.' '.$username->last_name){echo "selected";} ?> >{{$username->first_name.' '.$username->last_name}}</option>
                                            @endforeach
                                        }
                                        @endif
                                    </select>
                                </div>

                                <div class="col-md-4 form-group">
                                    <label for="customerEmail" class="form-label">User Email</label>
                                    <select type="email" class="form-control mb-4" id="customerEmail" name="email" style="width: 100%;">
                                        <option value="" selected>All</option>
                                        @if(isset($users_email) && !empty($users_email)){
                                            @foreach($users_email as $user_email)
                                            <option value="{{$user_email->id}}" <?php if($request->email == $user_email->id){echo "selected";} ?> >{{$user_email->email}}</option>
                                            @endforeach
                                        }
                                        @endif
                                    </select>
                                </div>

                                <div class="col-md-4 form-group">
                                    <label for="orderFrom" class="form-label">Order From</label>
                                    <input type="date" class="form-control mb-4" id="orderFrom" name="start" value="{{$request->start}}">
                                </div>
                                <div class="col-md-4 form-group">
                                    <label for="orderTo" class="form-label">Order To</label>
                                    <input type="date" class="form-control mb-4" id="orderTo" name="end" value="{{$request->end}}">
                                </div>
                                <div class="col-md-4 form-group">
                                    <label for="orderStatus" class="form-label">Order Status</label>
                                    <!-- <input type="text" id="myselect" name="myselect"> -->
                                    <select class="form-control mb-4" id="orderStatus" multiple="multiple" name="orderstatus[]"  placeholder="Status" style="width: 100%;">
                                        <!-- <option value="" selected>All</option> -->
                                        @foreach($allStatus as $status){ 
                                        <option value="{{$status->order_status}}" <?php if(isset($request->orderstatus) && !empty($request->orderstatus) && in_array($status->order_status, $request->orderstatus)){echo "selected";} ?>  >{{$status->order_status}}</option>
                                        }
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <input type="submit" name="search_submit" class="bg-primary text-light px-4 py-1 rounded-2 border-0" value="search">
                        </div>
                    </form>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">Orders</h6>
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                    aria-label="btn-close"></button>
                            </div>
                        @endif
                        <div id="del_msg">

                        </div>

                        <div class="table-responsive">
                            <table id="dataTableExample" class="table">@csrf
                                <thead>
                                    <tr>
                                        <th>@sortablelink('id','Order ID')</th>
                                        <th>@sortablelink('created_at','Order Date')</th>
                                        <th>@sortablelink('fisrt_name','Customer Name') </th>
                                        <th>@sortablelink('email','Customer Email')</th>
                                        <th class="text-primary">Ordered Product </th>
                                        <th>@sortablelink('grand_total',' Ordered Amount') </th>
                                        <th>@sortablelink('order_status','Ordered Status')</th>
                                        <th class="text-primary">Payment Method</th>
                                        <th class="text-primary" >Action</th>

                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach ($orders as  $order)
                                        <tr>
                                            <td>#ORD- {{ $order['id'] }}</td>
                                            <td>{{ date('d-m-Y',strtotime($order['created_at'])) }}</td>
                                            <td>{{ $order['first_name'] . " " . $order['last_name'] }}</td>
                                            <td>{{ $order['email'] }}</td>
                                            <td>

                                                @foreach ($order->getOrderDetail as $ordered_product)
                                                {{$ordered_product['product_name'].'-'.$ordered_product['product_color'].'-'.$ordered_product['product_size']}}<br><br>
                                                 @endforeach
                                            </td>
                                            <td>&#8377; {{ $order['grand_total'] }}</td>
                                            <td>{{ $order['order_status'] }}</td>
                                            <td>{{ strtoupper($order['payment_method']) }}</td>
                                            <td> <a href="{{ route('admin.view-order').'/'. $order['id'] }}"><span
                                                        class="m-1 text-primary"> <i data-feather="eye"></i></span></a></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!! $orders->appends(Request::except('page'))->render() !!}

                        </div><br>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("js")
<script>
    $(document).ready(function(){
        $(".search-box").click(function() {
           $('#search-card').toggle('slow');
        });

        $('#orderStatus, #customerEmail, #customerName').select2();

    })
</script>
<!-- Latest compiled and minified JavaScript -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script> -->

<!-- (Optional) Latest compiled and minified JavaScript translation files -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/i18n/defaults-*.min.js"></script> -->
@endsection
