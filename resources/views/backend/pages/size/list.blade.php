@extends('backend.layouts.app_layout')

@section("css")
<style>
    .select2-selection{
        border: 1px solid #e9ecef !important;
        height: 37px !important;
    }
    .select2-selection__rendered{
        padding: 5px !important;
    }
    .select2-selection__arrow{
        margin: 5px !important;
    }
</style>
@endsection

@section('section')
    <div class="page-content">

        <nav class="page-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="text-dark">Home &nbsp;</a></li>
                <li class="breadcrumb-item">Size</li>
        </nav>

        <!-- Accordio for search filter -->
        <div class="row mb-4">
            <div class="col-md-12">
                <div class="w-100 bg-primary text-light py-2 px-4 d-flex justify-content-between align-items-center rounded-2 search-box"
                    type="button" data-toggle="collapse" data-target="#search-card" aria-expanded="false"
                    aria-controls="search-card">
                    <h5>Search Sizes</h5>
                    <i class="link-icon" data-feather="arrow-down"></i>
                </div>
                <div class="card w-100 collapse" id="search-card">
                    <form action="{{ route('admin.size') }}" type="get" id="searchSizeForm">
                        <!-- @csrf -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label for="sizeName" class="form-label">Size Name</label>

                                    <select class="form-select" name="size" id="sizeName" style="width: 100%;">
                                        <option value="">All</option>
                                        @if(isset($size) && !empty($size)){
                                            @foreach($size as $key => $item)
                                            <option value="{{$item->size}}"  <?php if($request->size==$item->size){echo "selected";} ?> >{{$item->size}}</option>
                                            @endforeach  
                                        } 
                                        @endif    
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <input type="submit" name="search_submit"
                                class="bg-primary text-light px-4 py-1 rounded-2 border-0" value="search">
                        </div>
                    </form>
                </div>

            </div>
        </div>


        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="row d-flex justify-content-center align-items-center mb-3">
                            <div class="col-md-6">
                                <h6 class="card-title mt-3">Size Table</h6>
                            </div>
                            <div class="col-md-6">
                                <a href="size-add">
                                    <button type="button" class="btn btn-primary mb-1 mb-md-0 float-end">
                                        Add Size
                                    </button>
                                </a>
                            </div>
                        </div>
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                    aria-label="btn-close"></button>
                            </div>
                        @endif
                        <div class="table-responsive">
                            @if (count($sizes) > 0)
                            <table id="dataTableExample" class="table">@csrf
                                <thead>
                                    <tr>
                                        <th>@sortablelink('id')</th>
                                        <th>@sortablelink('size')</th>
                                        <th class="text-primary">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($sizes as $key => $size)
                                        <tr>
                                            <td>{{ $sizes->firstItem() + $key }}</td>
                                            <td>{{ $size['size'] }}</td>
                                            <td>

                                                <a class="size_remove" data-id="{{ $size['id'] }}">
                                                    <span class="m-1 text-danger"><i data-feather="trash-2"></i>
                                                    </span></a>

                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @else
                                <div class="border-top">
                                    <h4 align="center" style="padding : 20px;">No Record Found.</h4>
                                </div>
                            @endif<br><br>
                            {!! $sizes->appends(Request::except('page'))->render() !!}
                            {{-- {{ $sizes->links()}} --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection


@section('js')
    <script>
        $(document).ready(function() {
            $(".search-box").click(function() {
                $('#search-card').toggle('slow');
            });

            $("#sizeName").select2();

            $('.size_remove').click(function(e) {
                e.preventDefault();
                var ele = $(this);
                swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover this size!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {

                            $.ajax({
                                url: '{{ route('admin.delete-size') }}',
                                method: "post",
                                data: {
                                    _token: '{{ csrf_token() }}',
                                    id: ele.attr("data-id"),
                                },
                                success: function(response) {
                                    swal("Poof! Your size has been deleted!", {
                                        icon: "success",
                                    });
                                    window.location.reload();
                                }
                            });
                        } else {
                            swal("Your size is safe!");
                        }
                    });
            });
        })
    </script>
@endsection
