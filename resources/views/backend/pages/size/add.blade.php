@extends('backend.layouts.app_layout')


@section('section')
    <div class="page-content">
        @if (session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}

            </div>
        @endif

        <nav class="page-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="text-dark">Home  &nbsp;</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.size') }}" class="text-dark">Size  &nbsp;</a></li>
                <li class="breadcrumb-item">Add Size</li>
        </nav>
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Add Size</h4>
                        <form id="size" action="{{ route('admin.size-add') }}" method="POST">
                            @csrf
                            <div class="mb-3">
                                <label for="name" class="form-label">Size</label>
                                <input id="name" class="form-control size_validate" name="size" type="text" required 
                                value="{{old('size')}}">
                                <span class="text-danger validate_err_msg">@error('size'){{$message}}@enderror</span>
                            </div>

                            <input class="btn btn-primary" type="submit" value="Submit">
                            <a href="{{route('admin.size')}}"><input class="btn btn-secondary"  type="button" value="Cancel"></a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
        @endsection
