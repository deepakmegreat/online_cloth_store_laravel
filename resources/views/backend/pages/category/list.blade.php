@extends('backend.layouts.app_layout')

@section("css")
<style>
    .select2-selection{
        border: 1px solid #e9ecef !important;
        height: 37px !important;
    }
    .select2-selection__rendered{
        padding: 5px !important;
    }
    .select2-selection__arrow{
        margin: 5px !important;
    }
</style>
@endsection

@section('section')
    <div class="page-content">

        <nav class="page-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="text-dark">Home &nbsp;</a></li>
                <li class="breadcrumb-item">Category &nbsp;</li>
        </nav>

        <!-- Accordio for search filter -->
        <div class="row mb-4">
            <div class="col-md-12">
                <div class="w-100 bg-primary text-light py-2 px-4 d-flex justify-content-between align-items-center rounded-2 search-box"
                    type="button" data-toggle="collapse" data-target="#search-card" aria-expanded="false"
                    aria-controls="search-card">
                    <h5>Search Categories</h5>
                    <i class="link-icon" data-feather="arrow-down"></i>
                </div>
                <div class="card w-100 collapse" id="search-card">
                    <form action="{{ route('admin.category') }}" type="get" id="searchProductForm">
                        <!-- @csrf -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label for="categoryName" class="form-label">Category Name</label>
                                    <select class="form-select" name="category" id="categoryName" style="width: 100%;">
                                        <option value="" selected>All</option>
                                        @if(isset($allCategory) && !empty($allCategory)){
                                            @foreach ($allCategory as $key => $item)
                                                <option value="{{ $item['name'] }}" <?php if($request->category == $item['name']){echo "selected";} ?> >{{ $item['name'] }}</option>
                                            @endforeach
                                        }
                                        @endif
                                    </select>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="categoryStatus" class="form-label">Category Status</label>
                                    <select class="form-select" name="status" id="categoryStatus" style="width: 100%;">
                                        <option value="" selected>All</option>
                                        {{-- @if(isset($status) && !empty($status)){
                                            @foreach ($status as $key => $item)
                                            $status = ""; if($request->status == $item->status) { $status = "selected"; }
                                                @if ($item->status == 'A')
                                                    <option value="{{ $item->status }}" <?php echo $status; ?> >Active</option>
                                                @else
                                                    <option value="{{ $item->status }}" <?php echo $status; ?> >Inactive</option>
                                                @endif
                                            @endforeach
                                        }
                                        @endif --}}
                                            @foreach (config('constant.status') as $key => $value)
                                            <?php $status = ""; if($request->status == $key) { $status = "selected"; }?>
                                                <option value="{{ $key }}" {{ $status }} >{{ $value }}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <input type="submit" name="search_submit"
                                class="bg-primary text-light px-4 py-1 rounded-2 border-0" value="search">
                        </div>
                    </form>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="row d-flex justify-content-center align-items-center mb-3">
                            <div class="col-md-6 d-flex align-items-center">
                                <h6 class="card-title mt-3">Category Table</h6>
                            </div>
                            <div class="col-md-6">
                                <a href="category-add">
                                    <button type="button" class="btn btn-primary mb-1 mb-md-0 float-end add_cat">Add
                                        Category
                                    </button>
                                </a>
                            </div>
                        </div>
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                    aria-label="btn-close"></button>
                            </div>
                        @endif
                        <div class="table-responsive">
                            @if (count($categories) > 0)
                            <table id="dataTableExample" class="table">@csrf
                                <thead>
                                    <tr>
                                        <th>@sortablelink('id')</th>
                                        <th>@sortablelink('id', 'Category Name')</th>
                                        <th class="text-primary">Category Image</th>
                                        <th>@sortablelink('status')</th>
                                        <th class="text-primary">Action</th>
                                    </tr>
                                </thead>

                                @foreach ($categories as $key => $category)
                                    <tr>
                                        <td>{{ $categories->firstItem() + $key }}</td>
                                        <td>{{ $category['name'] }}</td>
                                        <?php
                                        if (file_exists(public_path('backend/images/uploads/category/' . $category['category_image'])) && $category['category_image'] != null && $category['category_image'] != '') {
                                            $image = asset('backend/images/uploads/category/' . $category['category_image']);
                                        } else {
                                            $image = asset('backend/images/uploads/default.jpg');
                                        }
                                        ?>
                                        <td> <img src="<?= $image ?>" alt="<?= $image ?>"> </td>
                                        @if ($category['status'] == 'A')
                                            <td>
                                                <h5><span class="badge bg-primary">Active</span></h5>
                                            </td>
                                        @else
                                            <td>
                                                <h5><span class="badge bg-danger">Inactive</span></h5>
                                            </td>
                                        @endif

                                        <td>

                                            <a href="category-edit/{{ $category['id'] }}"><span class="m-1 text-warning"> <i
                                                        data-feather="edit"></i></span></a>

                                            <a class="category_remove" data-id="{{ $category['id'] }}">
                                                <span class="m-1 text-danger"><i data-feather="trash-2"></i>
                                                </span></a>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @else
                            <div class="border-top">
                                <h4 align="center" style="padding : 20px;">No Record Found.</h4>
                            </div>
                        @endif
                            <br><br>
                            {!! $categories->appends(Request::except('page'))->render() !!}
                            {{-- {{ $categories->links() }} --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $(".search-box").click(function() {
                $('#search-card').toggle('slow');
            });

            $('#categoryName, #categoryStatus').select2();

            // $('#categoryStatus, ').select2();

            $('.category_remove').click(function(e) {
                e.preventDefault();
                var ele = $(this);
                swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover this category!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {

                            $.ajax({
                                url: '{{ route('admin.delete-category') }}',
                                method: "post",
                                data: {
                                    _token: '{{ csrf_token() }}',
                                    id: ele.attr("data-id"),
                                },
                                success: function(response) {
                                    swal("Poof! Your category has been deleted!", {
                                        icon: "success",
                                    });
                                    window.location.reload();
                                }
                            });
                        } else {
                            swal("Your category is safe!");
                        }
                    });
            });
        })
    </script>
@endsection
