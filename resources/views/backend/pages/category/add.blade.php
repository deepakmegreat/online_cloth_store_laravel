@extends('backend.layouts.app_layout')


@section('section')
    <div class="page-content">
        @if (session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}

            </div>
        @endif

        <nav class="page-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="text-dark">Home  &nbsp;</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.category') }}" class="text-dark">Category  &nbsp;</a></li>
                <li class="breadcrumb-item">Add Category</li>
        </nav>

        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Add Category</h4>

                        <form id="category" action="{{ route('admin.category-add') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="mb-3">
                                <label for="name" class="form-label">Category Name</label>
                                <input id="name" class="form-control" name="name" type="text" value="{{ old('name') }}">
                                <span class="text-danger">@error('name'){{$message}}@enderror</span>
                            </div>

                            <div class="mb-3">
                                <label for="profile_img" class="form-label">Category Image</label>
                                <input id="profile_img" class="form-control" name="category_image" type="file" accept="image/png, image/jpeg" value="{{ old('profile_img') }}" >
                                <span class="text-danger">@error('category_image'){{$message}}@enderror</span><br>
                                Selected Image: <span id="preview" class="mt-2">none</span>

                            </div>

                            <div class="mb-3">
                                <label class="form-label" name="status">Status</label>
                                <div>
                                    <div class="form-check form-check-inline">
                                        <input type="radio" class="form-check-input" name="status" value="A"
                                            id="active" checked>
                                        <label class="form-check-label" for="active">
                                            Active
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input type="radio" class="form-check-input" name="status" value="I"
                                            id="inactive">
                                        <label class="form-check-label" for="inactive">
                                            Inactive
                                        </label>
                                    </div>

                                </div>
                            </div>

                            <input class="btn btn-primary" type="submit" value="Submit">
                            <a href="{{route('admin.category')}}"><input class="btn btn-secondary"  type="button" value="Cancel"></a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
        @endsection


