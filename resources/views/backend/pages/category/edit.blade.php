@extends('backend.layouts.app_layout')


@section('section')
    <div class="page-content">
        @if (session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}

            </div>
        @endif

        <nav class="page-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="text-dark">Home  &nbsp;</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.category') }}" class="text-dark">Category  &nbsp;</a></li>
                <li class="breadcrumb-item">Edit Category</li>
        </nav>

        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Update Category</h4>

                        <form id="category" action="{{ route('admin.category-update') }}" method="POST"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="mb-3">
                                <label for="name" class="form-label">Category Name</label>
                                <input id="name" class="form-control" name="name" type="text"
                                    value="{{ $category['name'] }}">
                                <span class="text-danger">
                                    @error('name')
                                        {{ $message }}
                                    @enderror
                                </span>
                                <input name="cid" type="hidden" value="{{ $category['id'] }}">
                            </div>

                            <div class="mb-3">
                                <label class="form-label" for="update_profile_img">Category Image</label>
                                <input id="update_profile_img" class="form-control" name="category_image" type="file"
                                    accept="image/png, image/jpeg">
                                <span class="text-danger">
                                    @error('category_image')
                                        {{ $message }}
                                    @enderror
                                </span><br>
                                <?php
                                if (file_exists('backend/images/uploads/category/' . $category['category_image']) && $category['category_image'] != null && $category['category_image'] != '') {
                                    $image = 'backend/images/uploads/category/' . $category['category_image'];
                                } else {
                                    $image = 'backend/images/uploads/default.jpg';
                                }
                                ?>
                                Image: <img class="profile-img  mt-2" id="update_preview" src="{{ asset($image) }}"
                                    alt="<?= $image ?>" height="40px" width="40px">
                            </div>


                            <div class="mb-3">
                                <label class="form-label" name="status">Status</label>
                                <div>
                                    <div class="form-check form-check-inline">
                                        <input type="radio" class="form-check-input" name="status" value="A"
                                            id="active" {{ $category['status'] == 'A' ? 'checked' : '' }}>
                                        <label class="form-check-label" for="active">
                                            Active
                                        </label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <input type="radio" class="form-check-input" name="status" value="I"
                                            id="inactive" {{ $category['status'] == 'I' ? 'checked' : '' }}>
                                        <label class="form-check-label" for="inactive">
                                            Inactive
                                        </label>
                                    </div>

                                </div>
                            </div>
                            <input class="btn btn-primary" type="submit" value="Update">
                            <a href="{{ route('admin.category') }}"><input class="btn btn-secondary" type="button"
                                    value="Cancel"></a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
        @endsection
