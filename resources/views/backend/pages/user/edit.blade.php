@extends('backend.layouts.app_layout')


@section('section')
    <div class="page-content">
        @if (session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}

            </div>
        @endif

        <nav class="page-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="text-dark">Home  &nbsp;</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.user') }}" class="text-dark">User  &nbsp;</a></li>
                <li class="breadcrumb-item">Edit User</li>
        </nav>

        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Update User</h4>

                        <form id="user" action="{{ route('admin.save-edit-user') }}" method="POST">
                            @csrf
                            <div class="mb-3">
                                <label for="name" class="form-label">User Name</label>
                                <input id="name" class="form-control" name="name" type="text"
                                    value="{{ $user['name'] }}">
                                <span class="text-danger">
                                    @error('name')
                                        {{ $message }}
                                    @enderror
                                </span>
                                <input name="uid" type="hidden" value="{{ $user['id'] }}">
                            </div>

                            <div class="mb-3">
                                <label for="email" class="form-label">User Email</label>
                                <input id="email" class="form-control" name="email" type="email"
                                    value="{{ $user['email'] }}">
                                <span class="text-danger">
                                    @error('email')
                                        {{ $message }}
                                    @enderror
                                </span>
                            </div>

                            <input class="btn btn-primary" type="submit" value="Update">
                            <a href="{{ route('admin.user') }}"><input class="btn btn-secondary" type="button"
                                    value="Cancel"></a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
        @endsection
