@extends('backend.layouts.app_layout')

@section('section')
<div class="page-content">

    @include('backend.includes.breadcrumb')
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">User Details</h4>

                    <form id="signupForm">
                        <div class="mb-3">
                            <label for="name" class="form-label">Name</label>
                            <input id="name" class="form-control" name="name" type="text" value="Deepak Tripathi">
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label">Email</label>
                            <input id="email" class="form-control" name="email" type="email" value="deepakmegreat@gmail.com">
                        </div>
                        <div class="mb-3">
                            <label for="age" class="form-label">Age</label>
                            <input id="age" class="form-control" name="age" type="age" value="21">
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Gender</label>
                            <div>
                                <div class="form-check form-check-inline">
                                    <input type="radio" class="form-check-input" name="gender_radio" id="gender1" checked>
                                    <label class="form-check-label" for="gender1">
                                        Male
                                    </label>
                                </div>

                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Items In Cart</label>
                            <div>
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" name="skill_check" class="form-check-input" id="checkInline1" checked>
                                    <label class="form-check-label" for="checkInline1">
                                      T-shirt
                                    </label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" name="skill_check" class="form-check-input" id="checkInline2" checked>
                                    <label class="form-check-label" for="checkInline2">
                                      T-shirt
                                    </label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" name="skill_check" class="form-check-input" id="checkInline3" checked>
                                    <label class="form-check-label" for="checkInline3">
                                       T-shirt
                                    </label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @endsection