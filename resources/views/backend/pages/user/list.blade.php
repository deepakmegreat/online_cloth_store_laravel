@extends('backend.layouts.app_layout')

@section("css")
<style>
    .select2-selection{
        border: 1px solid #e9ecef !important;
        height: 37px !important;
    }
    .select2-selection__rendered{
        padding: 5px !important;
    }
    .select2-selection__arrow{
        margin: 5px !important;
    }
</style>
@endsection

@section('section')
    <div class="page-content">

        <nav class="page-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="text-dark">Home &nbsp;</a></li>
                <li class="breadcrumb-item">User</li>
        </nav>

        <!-- Accordio for search filter -->
        <div class="row mb-4">
            <div class="col-md-12">
                <div class="w-100 bg-primary text-light py-2 px-4 d-flex justify-content-between align-items-center rounded-2 search-box"
                    type="button" data-toggle="collapse" data-target="#search-card" aria-expanded="false"
                    aria-controls="search-card">
                    <h5>Search Users</h5>
                    <i class="link-icon" data-feather="arrow-down"></i>
                </div>
                <div class="card w-100 collapse" id="search-card">
                    <form action="{{ route('admin.user') }}" type="get" id="searchProductForm">
                        <!-- @csrf -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4 form-group">
                                    <label for="name" class="form-label">User Name</label>
                                    <select type="text" class="form-control mb-4" id="customerName" name="name" value="{{old('name')}}" style="width: 100%;">
                                        <option value="" selected >All</option>
                                        @if(isset($allUsers) && !empty($allUsers)){
                                            @foreach($allUsers as $user)
                                            <option value="{{$user->name}}" <?php if($request->name == $user->name){echo "selected";} ?> >{{$user->name}}</option>
                                            @endforeach
                                        }
                                        @endif
                                    </select>
                                </div>

                                <div class="col-md-4 form-group">
                                    <label for="email" class="form-label">User Email</label>
                                    <select type="email" class="form-control mb-4" id="customerEmail" name="email" value="{{old('email')}}" style="width: 100%;">
                                        <option value="" selected>All</option>
                                        @if(isset($allUsers) && !empty($allUsers)){
                                            @foreach($allUsers as $user)
                                            <option value="{{$user->email}}" <?php if($request->email ==$user->email){echo "selected";} ?>>{{$user->email}}</option>
                                            @endforeach
                                        }
                                        @endif

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <input type="submit" name="search_submit"
                                class="bg-primary text-light px-4 py-1 rounded-2 border-0" value="search">
                        </div>
                    </form>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">User Table</h6>
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                    aria-label="btn-close"></button>
                            </div>
                        @endif

                        <div class="table-responsive">
                            @if (count($users) > 0)
                            <table id="dataTableExample" class="table">@csrf
                                <thead>
                                    <tr>
                                        <th>@sortablelink('id')</th>
                                        <th> @sortablelink('name')</th>
                                        <th>@sortablelink('email')</th>
                                        <th class="text-primary">Action</th>
                                        {{-- <th>Action</th> --}}
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach ($users as $key => $user)
                                        <tr>
                                            <td>{{ $users->firstItem() + $key }}</td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>
                                                <a href="edit-user/{{ $user['id'] }}"><span class="text-warning"> <i
                                                            data-feather="edit"></i></span></a>
                                                <a class="btn btn-btn-light btn-xs user_remove" data-id="{{$user['id']}}"><i class="text-danger"
                                                        data-feather="trash-2"></i>
                                            </td>

                                        </tr>

                                    @endforeach
                                </tbody>
                            </table>
                            @else
                            <div class="border-top">
                                <h4 align="center" style="padding : 20px;">No Record Found.</h4>
                            </div>
                            @endif
                        </div><br>
                        {!! $users->appends(Request::except('page'))->render() !!}

                        {{-- {{ $users->links() }} --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $(".search-box").click(function() {
                $('#search-card').toggle('slow');
            });

            $('#customerEmail,#customerName').select2();

            $('.user_remove').click(function(e) {
                e.preventDefault();
                var ele = $(this);
                swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover this user!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {

                            $.ajax({
                                url: '{{ route('admin.delete-user') }}',
                                method: "post",
                                data: {
                                    _token: '{{ csrf_token() }}',
                                    id: ele.attr("data-id"),
                                },
                                success: function(response) {
                                    swal("Poof! Your user has been deleted!", {
                                        icon: "success",
                                    });
                                    window.location.reload();
                                }
                            });
                        } else {
                            swal("Your user is safe!");
                        }
                    });
            });
        })
    </script>
@endsection
