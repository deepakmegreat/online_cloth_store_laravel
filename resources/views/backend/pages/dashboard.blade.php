@extends('backend.layouts.app_layout')

@section('section')
<div class="page-content">
    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Home</li>
    </nav>
    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
    <div>
        <h4 class="mb-3 mb-md-0">Welcome  {{Auth::user()->name;}}</h4>
    </div>
    </div>

    <div class="row">
    <div class="col-12 col-xl-12 stretch-card">
        <div class="row flex-grow-1">
        <div class="col-md-4 grid-margin stretch-card">
            <div class="card">
            <div class="card-body">
                <div class="d-flex mb-3 justify-content-between align-items-baseline">
                    <h6 class="card-title mb-0">New Customers</h6>
                </div>
                <div class="row">
                    <div class="col-1">
                        <div class="mb-2"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user-plus"><path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="8.5" cy="7" r="4"></circle><line x1="20" y1="8" x2="20" y2="14"></line><line x1="23" y1="11" x2="17" y2="11"></line></svg></div>
                    </div>
                    <div class="col-11">
                        <h3 class="mb-2">&nbsp;{{$total_users}}</h3>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <div class="col-md-4 grid-margin stretch-card">
            <div class="card">
            <div class="card-body">
                <div class="d-flex mb-3 justify-content-between align-items-baseline">
                 <h6 class="card-title mb-0">New Orders</h6>
                </div>
                <div class="row">
                    <div class="col-1">
                        <div class="col-sm-6 col-md-4 col-lg-3"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-feather"><path d="M20.24 12.24a6 6 0 0 0-8.49-8.49L5 10.5V19h8.5z"></path><line x1="16" y1="8" x2="2" y2="22"></line><line x1="17.5" y1="15" x2="9" y2="15"></line></svg></div>
                    </div>
                    <div class="col-11">
                        <h3 class="mb-2">{{ $total_orders }}</h3>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <div class="col-md-4 grid-margin stretch-card">
            <div class="card">
            <div class="card-body">
                <div class="d-flex mb-3 justify-content-between align-items-baseline">
                  <h6 class="card-title mb-0">Total Products</h6>
                </div>
                <div class="row">
                    <div class="col-1">
                        <div class="col-sm-6 col-md-4 col-lg-3"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shopping-bag"><path d="M6 2L3 6v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2V6l-3-4z"></path><line x1="3" y1="6" x2="21" y2="6"></line><path d="M16 10a4 4 0 0 1-8 0"></path></svg></div>
                    </div>
                    <div class="col-11">
                        <h3 class="mb-2">&nbsp;{{$total_products}}
                        </h3>
                    </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
    </div> <!-- row -->
</div>
@endsection
