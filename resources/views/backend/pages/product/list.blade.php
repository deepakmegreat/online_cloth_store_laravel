@extends('backend.layouts.app_layout')

@section("css")
<style>
    .select2-selection__choice {
        background-color: #0d6efd !important;
    }
    .select2-selection{
        border: 1px solid #e9ecef !important;
        height: 37px !important;
    }
    .select2-selection__rendered{
        padding: 5px !important;
    }
    .select2-selection__arrow{
        margin: 5px !important;
    }
</style>
@endsection

@section('section')
    <div class="page-content">

        <nav class="page-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}" class="text-dark">Home &nbsp;</a></li>
                <li class="breadcrumb-item">Product</li>
        </nav>

        <!-- Accordio for search filter -->
        <div class="row mb-4">
            <div class="col-md-12">
                <div class="w-100 bg-primary text-light py-2 px-4 d-flex justify-content-between align-items-center rounded-2 search-box"
                    type="button" data-toggle="collapse" data-target="#search-card" aria-expanded="false"
                    aria-controls="search-card">
                    <h5>Search Products</h5>
                    <i class="link-icon" data-feather="arrow-down"></i>
                </div>
                <div class="card w-100 collapse" id="search-card">
                    <form action="{{ route('admin.product') }}" type="get" id="searchProductForm">
                        <!-- @csrf -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4 form-group">
                                    <label for="productName" class="form-label">Product Name</label>
                                    <input type="text" class="form-control mb-4" id="productName" name="name"
                                        value="{{$request->name}}">
                                </div>
                                <div class="col-md-4 form-group">
                                    <label for="productStatus" class="form-label">Product Status</label>
                                    <select class="form-select" name="status" id="productStatus">
                                        <option selected value="">All</option>
                                        @if(isset($status) && !empty($status)){
                                            @foreach ($status as $key => $item)
                                            <?php $status = ""; if($request->status == $item->status) { $status = "selected"; }?>
                                                @if ($item->status == 'A')
                                                    <option value="{{ $item->status }}" <?php echo $status; ?> >Active</option>
                                                @else
                                                    <option value="{{ $item->status }}" <?php echo $status; ?> >Inactive</option>
                                                @endif
                                            @endforeach
                                        }
                                        @endif

                                    </select>
                                </div>
                                <div class="col-md-4 form-group">
                                    <label for="productCategory" class="form-label">Product Category</label>
                                    <select class="form-select" name="category" id="productCategory" style="width: 100%">
                                        <option value="" selected>All </option>
                                        @if(isset($category) && !empty($category)){
                                            @foreach ($category as $key => $item)
                                                <option value="{{ $item->id }}" <?php if($request->category == $item->id){echo "selected";} ?> >{{ $item->name }}</option>
                                            @endforeach
                                        }
                                        @endif

                                    </select>
                                </div>

                                <div class="col-md-4 form-group">
                                    <label for="productSize" class="form-label">Product Size</label><br>

                                    <select class="form-select" name="size[]" id="productSize" multiple="multiple" style="width: 100%;">
                                        <!-- <option value="">Sizes</option> -->
                                        @if(isset($size) && !empty($size)){
                                            @foreach($size as $key => $item)
                                            <option value="{{$item->id}}" <?php if(isset($request->size) && !empty($request->size) && in_array($item->id, $request->size)){echo "selected";} ?> >{{$item->size}}</option>
                                            @endforeach       
                                        }
                                        @endif

                                    </select>

                                </div>

                                <div class="col-md-4 form-group">
                                    <label for="productColor" class="form-label">Product Color</label><br>
                                    <select class="form-select" name="color[]" id="productColor" multiple="multiple" style="width: 100%;">
                                        <!-- <option value="">Colors</option> -->
                                        @if(isset($color) && !empty($color)){
                                            @foreach($color as $key => $item)
                                            <option value="{{$item->id}}" <?php if(isset($request->color) && !empty($request->color) && in_array($item->id, $request->color)){echo "selected";} ?>  >{{$item->colour}}</option>
                                            @endforeach  
                                        } 
                                        @endif    
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <input type="submit" name="search_submit"
                                class="bg-primary text-light px-4 py-1 rounded-2 border-0" value="search">
                        </div>
                    </form>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="row d-flex justify-content-center align-items-center mb-3">
                            <div class="col-md-6">
                                <h6 class="card-title mt-3">Product Table</h6>
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('admin.add-product') }}"><button type="button"
                                        class="btn btn-primary float-end">ADD PRODUCT</button></a>
                            </div>
                        </div>
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                    aria-label="btn-close"></button>
                            </div>
                        @endif
                        <div class="table-responsive">
                            @if (count($data) > 0)
                            <table id="dataTableExample" class="table">@csrf
                                <thead>
                                    <tr>
                                        <th>@sortablelink('id')</th>
                                        <th>@sortablelink('name')</th>
                                        <th>@sortablelink('status')</th>
                                        <th>@sortablelink('price')</th>
                                        <th>@sortablelink('quantity')</th>
                                        <th class="text-primary">Image</th>
                                        <th class="text-primary">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach ($data as $key => $item)
                                        <tr>
                                            <td>{{ $data->firstItem() + $key }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>
                                                @if ($item->status == 'A')
                                                    <h5><span class="badge bg-primary">Active</span></h5>
                                                @else
                                                    <h5><span class="badge bg-danger">Inactive</span></h5>
                                                @endif
                                            </td>
                                            <td>&#8377;{{ $item->price }}</td>
                                            <td>{{ $item->quantity }}</td>
                                            <?php
                                            if (file_exists(public_path('backend/images/uploads/products/' . $item->image)) && $item->image != null && $item->image != '') {
                                                $image = asset('backend/images/uploads/products/' . $item->image);
                                            } else {
                                                $image = asset('backend/images/uploads/defaultproduct.jpg');
                                            }
                                            ?>
                                            <td> <img src="<?= $image ?>" alt="<?= $image ?>"> </td>
                                            <td>
                                                <a href="{{ route('admin.view-product', [$item->id]) }} "><span
                                                        class="m-1 text-primary"> <i data-feather="eye"></i></span></a>
                                                <a href="{{ route('admin.edit-product', [$item->id]) }}"><span
                                                        class="m-1 text-warning"><i data-feather="edit"></i></span></a>
                                                <a class="product_remove" data-id="{{ $item->id }}"><span
                                                        class="m-1 text-danger"> <i data-feather="trash-2"></i></span></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @else
                            <div class="border-top">
                                <h4 align="center" style="padding : 20px;">No Record Found.</h4>
                            </div>
                        @endif
                        </div><br>
                        {!! $data->appends(Request::except('page'))->render() !!}

                        {{-- {{ $data->links() }} --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $(".search-box").click(function() {
                $('#search-card').toggle('slow');
            });

            $('#productCategory, #productSize, #productColor').select2();

            $('.product_remove').click(function(e) {
                e.preventDefault();
                var ele = $(this);
                swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover this product!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {

                            $.ajax({
                                url: '{{ route('admin.delete-product') }}',
                                method: "post",
                                data: {
                                    _token: '{{ csrf_token() }}',
                                    id: ele.attr("data-id"),
                                },
                                success: function(response) {
                                    swal("Poof! Your product has been deleted!", {
                                        icon: "success",
                                    });
                                    window.location.reload();
                                }
                            });
                        } else {
                            swal("Your product is safe!");
                        }
                    });
            });
        })
    </script>
@endsection
