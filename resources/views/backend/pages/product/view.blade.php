@extends('backend.layouts.app_layout')

@section('section')

<div class="page-content">

    <nav class="page-breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="text-dark">Home  &nbsp;</a></li>
            <li class="breadcrumb-item"><a href="{{ route('admin.product') }}" class="text-dark">Product  &nbsp;</a></li>
            <li class="breadcrumb-item">View Product</li>
    </nav>

    <div class="row profile-body">

        <div class="d-none d-md-block col-md-12 col-xl-12 left-wrapper">
          <div class="card rounded">
            <div class="card-body">
              <div class="d-flex align-items-center justify-content-between mb-2">
                <h6 class="card-title mb-0">PRODUCT DETAILS</h6>
              </div>
              <div class="mt-3">
                <label class="tx-11 fw-bolder mb-0 text-uppercase">Product Name:</label>
                <p class="text-muted">{{$data['name']}}</p>
              </div>
              <div class="mt-3">
                <label class="tx-11 fw-bolder mb-0 text-uppercase">Category Name:</label>
                <p class="text-muted">
                @if($category_data != null && $category_data != '')
                  {{$category_data[0]['name']}}
                @endif
              </p>
              </div>
              <div class="mt-3">
                <label class="tx-11 fw-bolder mb-0 text-uppercase">Colours Name:</label>
                @foreach ($colour_data as $color)
                <p class="text-muted">{{$color['colour']}}</p>
                @endforeach
              </div>
              <div class="mt-3">
                <label class="tx-11 fw-bolder mb-0 text-uppercase">Sizes Name:</label>
                @foreach ($size_data as $size)
                <p class="text-muted">{{$size['size']}}</p>
                @endforeach
              </div>
              <div class="mt-3">
                <label class="tx-11 fw-bolder mb-0 text-uppercase">Price:</label>
                <p class="text-muted">{{$data['price']}}</p>
              </div>
              <div class="mt-3">
                <label class="tx-11 fw-bolder mb-0 text-uppercase">Quantity:</label>
                <p class="text-muted">{{$data['quantity']}}</p>
              </div>
              <div class="mt-3">
                <label class="tx-11 fw-bolder mb-0 text-uppercase">Status:</label>
                <p class="text-muted">{{$data['status']=='A' ? 'Active':'Inactive'}}</p>
              </div>
              <div class="mt-3">
                <?php
                    if (file_exists(public_path('backend/images/uploads/products/'. $data['image'])) && $data['image'] != null && $data['image'] != '') {
                        $image = asset('backend/images/uploads/products/' . $data['image']);
                    } else {
                        $image = asset('backend/images/uploads/defaultproduct.jpg');
                    }
                ?>
                <label class="tx-11 fw-bolder mb-0 text-uppercase">Image:</label><br>
                <img src="<?= $image ?>" alt="<?= $image ?>">
              </div>
              <a href="{{route('admin.product')}}"><input class="btn btn-lg btn-secondary mt-3"  type="button" value="Back"></a>
            </div>
          </div>
        </div>

      </div>


</div>
@endsection
