<div class="row addproductvariantrow_{{  $id }}">
    <div class="col-md-3">
        <div class="mb-3">
            <label for="color" class="form-label">Color</label>
            <select class="form-select" name="color_id[]" id="color_id_{{  $id }}">
                <option value="">Select Color</option>
                @if(!empty($colorList))
                    @foreach ($colorList as $color)
                        <option value="{{ $color['id'] }}" @if(old('color_id.*') != '') @if(old('color_id.*') == $color['id']) selected @endif @elseif(isset($value['colour']) && $value['colour'] == $color['id']) selected @endif>{{ $color['colour'] }}</option>    
                    @endforeach
                @endif
            </select>
            <span class="text-danger customerror error">@error('color_id.*'){{$message}}@enderror</span>
        </div>
    </div>
    <div class="col-md-3">
        <div class="mb-3">
            <label for="color" class="form-label">Size</label>
            <select class="form-select" name="size_id[]" id="size_id_{{  $id }}">
                <option value="">Select size </option>
                @if(!empty($sizeList))
                    @foreach ($sizeList as $size)
                        <option value="{{ $size['id'] }}" @if(old('size_id.*') != '') @if(old('size_id.*') == $size['id']) selected @endif @elseif(isset($value['colour']) && $value['size'] == $size['id']) selected @endif>{{ $size['size'] }}</option>    
                    @endforeach
                @endif
            </select>
            <span class="text-danger customerror error">@error('size_id.*'){{ $message }}@enderror</span>
        </div>
    </div>
    <div class="col-md-2">
        <div class="mb-3">
            <label for="price" class="form-label">Price</label>
            <input id="price_{{  $id }}" class="form-control" name="price[]" type="text"  placeholder="Enter Price" value="@if(old('price.*') != '') {{ old('price') }} @elseif (isset($value['price']) && $value['price'] != ''){{$value['price']}}@endif">
            <span class="text-danger customerror error">@error('price.*'){{$message}}@enderror</span>
        </div>
    </div>
    <div class="col-md-2">
        <div class="mb-3">
            <label for="quantity" class="form-label">Quantity</label>
            <input id="quantity_{{  $id }}" class="form-control" name="quantity[]" type="text" 
            placeholder="Enter Quantity" value="@if(old('quantity.*') != '') {{ old('quantity.*') }} @elseif (isset($value['quantity']) && $value['quantity'] != ''){{$value['quantity']}}@endif">
            <span class="text-danger customerror error">@error('quantity.*'){{$message}}@enderror</span>
        </div>
    </div>
        <div class="col-md-2">
        <div class="mt-4">
            <button class="btn btn-danger deletevariant" data-id="{{ $id }}" type="button">
                <i class="fa fa-trash" aria-hidden="true"></i>
            </button>
        </div>
    </div>

</div>