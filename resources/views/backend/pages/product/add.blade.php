@extends('backend.layouts.app_layout')
<style>
    .error {color:red !important}
</style>
@section('section')

<div class="page-content">
<nav class="page-breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="text-dark">Home  &nbsp;</a></li>
        <li class="breadcrumb-item"><a href="{{ route('admin.product') }}" class="text-dark">Product  &nbsp;</a></li>
        <li class="breadcrumb-item">Add Product</li>
</nav>
{{-- {{ dd(session('error')); }} --}}
    @if(session('error'))
    <div class="alert alert-success">
        <?php echo $msg2 = implode("<br>",@session('error')) ?>
        </div>
    @endif
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Add Product</h4>
                    <form id="product" method="post" action="{{route('admin.save-product')}}" enctype="multipart/form-data" name="product">
                       @csrf
                        <div class="mb-3">
                            <label for="name" class="form-label"> Product Name</label>
                            <input id="name" class="form-control" name="name" type="text" value="{{old('name')}}" >
                            <span class="text-danger">@error('name'){{$message}}@enderror</span>
                                
                        </div>

                        <div class="mb-3">
                            <label for="category" class="form-label">Category</label>
                            <select class="form-select" name="category_id" id="category" value="{{old('category_id')}}">
                                <option selected disabled>Select your category</option>
                                @foreach ($data['category_list'] as $key => $value)
                                @if($value['status']!='I')
                                <option value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                                @endif
                                @endforeach
                            </select>
                            <span class="text-danger">@error('category_id'){{$message}}@enderror</span>
                        </div>

                        <div class="mb-3">
                            <label for="price" class="form-label">Price</label>
                            <input id="price_main" class="form-control" name="price_main" type="text"  placeholder="Enter Price">
                            <span class="text-danger">@error('price_main'){{$message}}@enderror</span>
                        </div>

                        <div class="mb-3">
                            <label for="quantity" class="form-label">Quantity</label>
                            <input id="quantity_main" class="form-control" name="quantity_main" type="text" 
                            placeholder="Enter Quantity">
                            <span class="text-danger">@error('quantity_main'){{$message}}@enderror</span>
                        </div>
                        
                        <div class="mb-3">
                            <label class="form-label">Status</label>
                            <div>
                                <div class="form-check form-check-inline">
                                    <input type="radio" class="form-check-input" value="A" name="status" id="active" checked >
                                    <label class="form-check-label" for="active">
                                    Active
                                    </label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input type="radio" class="form-check-input form-check-input-danger" value="I" name="status" id="inactive">
                                    <label class="form-check-label" for="inactive">
                                    Inactive
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="exampleFormControlTextarea1" class="form-label">Description</label>
                            <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3" value="{{old('description')}}"></textarea>
                            <span class="text-danger">@error('description'){{$message}}@enderror</span>
                        </div>

                        <div class="mb-3">
                            <label class="form-label" for="formFile">Product Image</label>
                            <input class="form-control profile_img" type="file" id="profile_img" accept="image/png, image/jpeg" name="image">
                            Selected Image: <span id="preview" class="mt-2">none</span>
                        </div>

                        <div class="col-12 addproductvariant">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="mb-3">
                                        <label for="color" class="form-label">Color</label>
                                        <select class="form-select" name="color_id[]" id="color_id">
                                            <option value="">Select Color</option>
                                            @if(!empty($colorList))
                                                @foreach ($colorList as $color)
                                                    <option value="{{ $color['id'] }}" {{ old('color_id[]') == $color['id'] ? 'selected' : ''}}>{{ $color['colour'] }}</option>    
                                                @endforeach
                                            @endif
                                        </select>
                                        <span class="text-danger customerror error">@error('color_id.*'){{$message}}@enderror</span>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="mb-3">
                                        <label for="color" class="form-label">Size</label>
                                        <select class="form-select" name="size_id[]" id="size_id">
                                            <option value="">Select size </option>
                                            @if(!empty($sizeList))
                                                @foreach ($sizeList as $size)
                                                    <option value="{{ $size['id'] }}" {{ old('size_id[]') == $size['id'] ? 'selected' : ''}}>{{ $size['size'] }}</option>    
                                                @endforeach
                                            @endif
                                        </select>
                                        <span class="text-danger customerror error">@error('size_id.*'){{ $message }}@enderror</span>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="mb-3">
                                        <label for="price" class="form-label">Price</label>
                                        <input id="price" class="form-control" name="price[]" type="text"  placeholder="Enter Price">
                                        <span class="text-danger customerror error">@error('price.*'){{$message}}@enderror</span>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="mb-3">
                                        <label for="quantity" class="form-label">Quantity</label>
                                        <input id="quantity" class="form-control" name="quantity[]" type="text" 
                                        placeholder="Enter Quantity">
                                        <span class="text-danger customerror error">@error('quantity.*'){{$message}}@enderror</span>
                                    </div>
                                </div>
                                 <div class="col-md-2">
                                    <div class="mt-4">
                                        <button class="btn btn-danger addvariant" type="button">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <input class="btn btn-primary productadd" type="submit" value="Submit">
                        <a href="{{route('admin.product')}}"><input class="btn btn-secondary"  type="button" value="Cancel"></a>
                    </form>
                </div>
            </div>
        </div>

    </div>

</div>


@endsection

@section('js')
    <script>

        $(document).ready(function(){
            var i=1; 
            $(".addvariant").on( "click", function() {
                $('.addvariant').prop('disabled', true);
                $.ajax({
                    type: "get",
                    headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{ route('admin.getproductvariant') }}",
                    data:{
                        "id":i
                    },
                    success: function (response) {
                        i++;
                        $(".addproductvariant").append(response.html);
                        $('.addvariant').prop('disabled', false);
                    }
                }); 
            });

            $(document).on("click",".deletevariant",function(){
                $("[data-id='" + $(this).attr("data-id") + "']").parents(".addproductvariantrow_"+ $(this).attr("data-id")).remove();
            });
            
        });

        $("#product").validate({
            rules: {
                name: {
                    required: true,
                    minlength: 3
                },
                category_id: {
                    required: true
                },
                'price_main':{
                    required: true,
                    number: true
                },
                'quantity_main':{
                    required: true,
                    number: true
                },
                'color_id[]': {
                    required: true,
                },
                'size_id[]': {
                    required: true,
                },
                'price[]': {
                    required: true,
                    number: true
                },
                'quantity[]': {
                    required: true,
                    number: true
                },
                description: {
                    required: true,
                    minlength: 10
                }

            },
            
            messages: {
                name: {
                    required: "Please enter a name",
                    minlength: "Name must consist of at least 3 characters"
                },
                category_id: "Please select your category",
                'price_main':{
                    required: "Please enter price",
                    number: "Enter numeric value only"
                },
                'quantity_main':{
                    required: "Please enter quantity",
                    number: "Enter numeric value only"
                },
                'color_id[]': {
                    required: "Please select a color",
                },
                'size_id[]': {
                    required: "Please select a size",
                },
                'price[]': {
                    required: "Please enter price",
                    number: "Enter numeric value only"
                },
                'quantity[]': {
                required: "Please enter quantity",
                number: "Enter numeric value only"
                },
                description: {
                required: "Please Fill the description of product",
                minlength: "Your description must be at least 10 characters",
                }
            },

            highlight: function(element, errorClass) {
                if ($(element).prop('type') != 'checkbox' && $(element).prop('type') != 'radio') {
                $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
                }
            },
            unhighlight: function(element, errorClass) {
                if ($(element).prop('type') != 'checkbox' && $(element).prop('type') != 'radio') {
                $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
                }
            }
            
        });

        $.validator.prototype.checkForm = function (){
            this.prepareForm();
            for ( var i = 0, elements = (this.currentElements = this.elements()); elements[i]; i++ ) {
                if (this.findByName( elements[i].name ).length != undefined && this.findByName( elements[i].name ).length > 1) {
                    for (var cnt = 0; cnt < this.findByName( elements[i].name ).length; cnt++) {
                        this.check( this.findByName( elements[i].name )[cnt] );
                    }
                } 
                else {
                    this.check( elements[i] );
                }
            }
            return this.valid();
        };


    </script>
@endsection