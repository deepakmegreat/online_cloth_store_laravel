<?php
    $routeName = Route::currentRouteName();
    //print_r($routeName); die();
?>
<nav class="sidebar">
  <div class="sidebar-header">
    <a href="{{route('admin.dashboard')}}" class="sidebar-brand">
        <img class="wd-130 ht-40 float-end" src="{{ asset('backend/images/logo_new.png') }}" alt="">
    </a>
    <div class="sidebar-toggler not-active">
      <span></span>
      <span></span>
      <span></span>
    </div>
  </div>
  <div class="sidebar-body">
    <ul class="nav">
      <li class="nav-item ">
        <a href="{{route('admin.dashboard')}}" class="nav-link">
          <i class="link-icon" data-feather="box"></i>
          <span class="link-title">Dashboard</span>
        </a>
      </li>
      <li class="nav-item {{ $routeName=='admin.edit-user' ? 'active' : ''}}">
          <a href="{{route('admin.user')}}" class="nav-link">
            <i class="link-icon" data-feather="user"></i>
            <span class="link-title">Users</span>
          </a>
        </li>
      <li class="nav-item {{ $routeName=='admin.category-add' || $routeName=='admin.category-edit' ? 'active' : ''}}">
        <a href="{{route('admin.category')}}" class="nav-link">
          <i class="link-icon" data-feather="grid"></i>
          <span class="link-title">Category</span>
        </a>
      </li>
      <li class="nav-item {{ $routeName=='admin.add-product' || $routeName=='admin.edit-product' || $routeName=='admin.view-product' ? 'active' : ''}}">
        <a href="{{route('admin.product')}}" class="nav-link">
          <i class="link-icon" data-feather="aperture"></i>
          <span class="link-title">Product</span>
        </a>
      </li>
      <li class="nav-item {{ $routeName=='admin.view-order' || $routeName=='admin.view-orderdetail' ? 'active' : ''}}">
          <a href="{{route('admin.view-order')}}" class="nav-link">
            <i class="link-icon" data-feather="shopping-cart"></i>
            <span class="link-title">Orders</span>
          </a>
        </li>
        <li class="nav-item {{ $routeName=='admin.color-add'  ? 'active' : ''}}">
          <a href="{{route('admin.color')}}" class="nav-link">
            <i class="link-icon" data-feather="archive"></i>
            <span class="link-title">Color</span>
          </a>
        </li>
        <li class="nav-item {{ $routeName=='admin.size-add'  ? 'active' : ''}}">
          <a href="{{route('admin.size')}}" class="nav-link">
            <i class="link-icon" data-feather="gitlab"></i>
            <span class="link-title">Size</span>
          </a>
        </li>
    </ul>
  </div>
</nav>

