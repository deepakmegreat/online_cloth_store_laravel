<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="Responsive  Admin Dashboard based on Bootstrap 5">
	<meta name="author" content="StyleFusion">
	<meta name="keywords" content="">

	<title>StyleFusion</title>

  <!-- Fonts -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="preconnect" href="https://fonts.googleapis.com/">
  <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
  <link href="{{asset('backend/fonts/fonts.css')}}" rel="stylesheet">
  <!-- End fonts -->

	<!-- inject:css -->

	<link rel="stylesheet" href="{{asset('backend/css/flag-icon.min.css')}}">
	<!-- endinject -->

  <!-- Layout styles -->
	<link rel="stylesheet" href="{{asset('backend/css/style.min.css')}}">
  <!-- End layout styles -->

  <link rel="shortcut icon" href="{{asset('backend/images/logo_new.png')}}" />
  <link rel="stylesheet"  href="{{ asset('backend/vendors/select2/select2.min.css') }}"/>
  @yield("css")

</head>
