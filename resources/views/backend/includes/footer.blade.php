<!-- core:js -->
<script src="{{asset('backend/js/core.js')}}"></script>
<!-- endinject -->

<!-- Plugin js for this page -->
<script src="{{asset('backend/js/Chart.min.js')}}"></script>
<script src="{{asset('backend/js/jquery.flot.js')}}"></script>
<script src="{{asset('backend/js/jquery.flot.resize.js')}}"></script>
<script src="{{asset('backend/js/apexcharts.min.js')}}"></script>
<!-- End plugin js for this page -->

<!-- inject:js -->
<script src="{{asset('backend/js/feather.min.js')}}"></script>
<script src="{{asset('backend/js/template.js')}}"></script>
<!-- endinject -->

<!-- Custom js for this page -->
<script src="{{asset('backend/js/dashboard-light.js')}}"></script>
<!-- End custom js for this page -->

<!-- Plugin js for this page -->
<script src="{{ asset('backend/vendors/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('backend/vendors/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>
<script src="{{ asset('backend/vendors/inputmask/jquery.inputmask.min.js') }}"></script>
<script src="{{ asset('backend/vendors/select2/select2.min.js') }}"></script>
<script src="{{ asset('backend/vendors/typeahead.js/typeahead.bundle.min.js') }}"></script>
<script src="{{ asset('backend/vendors/jquery-tags-input/jquery.tagsinput.min.js') }}"></script>
<script src="{{ asset('backend/vendors/dropzone/dropzone.min.js') }}"></script>
<script src="{{ asset('backend/vendors/dropify/dist/dropify.min.js') }}"></script>
<script src="{{ asset('backend/vendors/bootstrap-colorpicker/bootstrap-colorpicker.min.js') }}"></script>
<script src="{{ asset('backend/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('backend/vendors/moment/moment.min.js') }}"></script>
<script src="{{ asset('backend/vendors/tempusdominus-bootstrap-4/tempusdominus-bootstrap-4.js') }}"></script>
<!-- End plugin js for this page -->

<!-- inject:js -->
<script src="{{ asset('backend/vendors/feather-icons/feather.min.js') }}"></script>
<!-- endinject -->

<!-- Custom js for this page -->
{{-- <script src="{{ asset('backend/js/form-validation.js') }}"></script> --}}
<script src="{{ asset('backend/js/bootstrap-maxlength.js') }}"></script>
<script src="{{ asset('backend/js/inputmask.js') }}"></script>
<script src="{{ asset('backend/js/select2.js') }}"></script>
<script src="{{ asset('backend/js/typeahead.js') }}"></script>
<script src="{{ asset('backend/js/tags-input.js') }}"></script>
<script src="{{ asset('backend/js/dropzone.js') }}"></script>
<script src="{{ asset('backend/js/dropify.js') }}"></script>
<script src="{{ asset('backend/js/bootstrap-colorpicker.js') }}"></script>
<script src="{{ asset('backend/js/datepicker.js') }}"></script>
<script src="{{ asset('backend/js/timepicker.js') }}"></script>
<script src="{{ asset('backend/vendors/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{asset('backend/js/sweet-alert.js') }}"></script>
<script src="{{ asset('backend/js/customjs/delete_product.js') }}"></script>
<script src="{{ asset('backend/js/customjs/common_function.js') }}"></script>
<!-- End custom js for this page -->
<!-- <script src="https://kit.fontawesome.com/yourcode.js" crossorigin="anonymous"></script> -->

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@yield('js')






