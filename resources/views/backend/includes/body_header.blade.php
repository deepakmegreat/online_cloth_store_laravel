<nav class="navbar">
    <a href="#" class="sidebar-toggler">
        <i data-feather="menu"></i>
    </a>
    <div class="navbar-content">

        <ul class="navbar-nav">

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="profileDropdown" role="button"
                    data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{ ucfirst(Auth::user()->name)  }}
                    <svg xmlns="http://www.w3.org/2000/svg"
                    width="11" height="11" fill="currentColor" class="bi bi-chevron-down"
                    viewBox="0 0 16 16">
                    <path fill-rule="evenodd"
                        d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z" />
                    </svg>
                </a>
                <div class="dropdown-menu p-0" aria-labelledby="profileDropdown">
                    <ul class="list-unstyled p-1">
                        <li class="dropdown-item py-2">
                            <a href="edit-user/{{ Auth::user()->id }}" class="text-body ms-0">
                                <i class="me-2 icon-md" data-feather="edit"></i>
                                <span>Edit Profile</span>
                            </a>
                        </li>
                        @if (Auth::check())
                            <li class="dropdown-item py-2"><a href="{{ route('logout') }}" class="text-body ms-0"
                                    onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();"><i
                                        class="me-2 icon-md" data-feather="log-out"></i>
                                    <span>Logout</span>
                                </a>
                            </li>
                        @endif
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</nav>
