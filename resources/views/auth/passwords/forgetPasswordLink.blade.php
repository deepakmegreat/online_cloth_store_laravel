<!DOCTYPE html>
<html lang="en">
@include('front_theme.layouts.common_header')

<body>
    @section('title')
        Reset Password | StyleFushion
    @endsection

    <section>
        <div class="container">

            <form id="loginForm" action="{{ route('reset.password.post') }}" class="checkout__form" method="post">
                @csrf
                <input type="hidden" name="token" value="{{ $token }}">
                @if (Session::has('message'))
                    <div class="alert alert-success" role="alert">
                        {{ Session::get('message') }}
                    </div>
                @endif
                <div class="row">
                    <div class="col-lg-4">

                    </div>
                    <div class="col-lg-4  p-5 shadow-lg"
                        style="position: absolute; top: 50%;left: 50%;transform: translate(-50%, -50%); background-color: #eee">
                        <h5 class="text-center">Reset Password</h5>
                        <div class="row">

                            <div class="col-lg-12">
                                <div class="checkout__form__input">
                                    <p>Email <span>*</span></p>
                                    <input class="rounded @error('email') is-invalid @enderror" type="email"
                                        name="email" id="email" placeholder="Enter Your email Address"
                                        value="{{ old('email') }}" autocomplete="email" autofocus>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="checkout__form__input">
                                    <p>Password <span>*</span></p>
                                    <input class="rounded @error('password') is-invalid @enderror" type="password"
                                        name="password" id="password" placeholder="Enter Password" value="{{old('password')}}">
                                    @if ($errors->has('password'))
                                        <span class="text-danger">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>

                                <div class="checkout__form__input">
                                    <p>Confirm Password <span>*</span></p>
                                    <input class="rounded" type="password"
                                        name="password_confirmation" id="password-confirm" placeholder="Enter Confirm Password">
                                    @if ($errors->has('password_confirmation'))
                                        <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
                                    @endif

                                </div>
                            </div>
                            <div class="col-lg-12 mt-2 d-flex justify-content-end">
                                <input type="submit" value="Submit" class="btn btn-dark">
                            </div>

                            <div class="col-lg-12 mt-1 d-flex justify-content-start">
                                <a href="{{ route('login') }}" class="btn btn-link">SignIn</a>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-4">

                    </div>
                </div>
            </form>
        </div>
    </section>
    @section('js')
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
        <script src="{{ asset('front_theme/js/custom_js/form_validation.js') }}"></script>
    @endsection

    @include('front_theme.layouts.common_js')
</body>

</html>
