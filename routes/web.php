<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\front_theme\HomeController;
use App\Http\Controllers\front_theme\CartController;
use App\Http\Controllers\front_theme\ProductController;
use App\Http\Controllers\front_theme\CheckoutController;
use App\Http\Controllers\front_theme\ContactController;
use App\Http\Controllers\front_theme\OrderController;
use App\Http\Controllers\front_theme\wishlistController;
use App\Http\Controllers\backend\adminproductController;
use App\Http\Controllers\backend\dashboardController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\backend\CategoryController;
use App\Http\Controllers\backend\ColorController;
use App\Http\Controllers\backend\SizeController;
use App\Http\Controllers\backend\AdminUserController;
use App\Http\Controllers\backend\AdminOrderController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect('/index');
})->name('/');
Auth::routes();

Route::get('/index', [HomeController::class, 'index'])->name('index');
Route::get('/product-detail/{id}', [ProductController::class, 'view'])->name('product-detail');
Route::get('/shop/{id?}', [ProductController::class, 'shop'])->name('shop');
Route::get('/shop_filter', [ProductController::class, 'shop_filter'])->name('shop_filter');
Route::post('add_to_cart',[ProductController::class,'addToCart'])->name('add_to_cart');
Route::post('remove_from_cart',[ProductController::class,'remove'])->name('remove_from_cart');
Route::post('update_cart',[ProductController::class,'update_cart'])->name('update_cart');
Route::get('/cart', [ProductController::class, 'cart'])->name('cart');
Route::get('/checkout', [CheckoutController::class, 'index'])->name('checkout');
Route::get('/contact', [ContactController::class, 'index'])->name('contact');
Route::get('/register', [HomeController::class, 'register'])->name('register');
Route::get('/login', [HomeController::class, 'login'])->name('login');
Route::get('/product-filter/{id?}', [HomeController::class, 'product_filter'])->name('product-filter');
Route::post('/update-order-status',[OrderController::class,'cancelorder'])->name('update-order-status');

Route::middleware(['auth'])->group(function(){
    Route::post('place-order',[OrderController::class,'place_order'])->name('place-order');
    Route::get('order-success',[OrderController::class,'order_success'])->name('order-success');
    Route::get('my-order/{id?}',[OrderController::class,'my_order'])->name('my-order');
    Route::get('/wishlist', [wishlistController::class, 'wishlist'])->name('wishlist');
    Route::post('add_to_wishlist', [wishlistController::class, 'addToWishlist'])->name('add_to_wishlist');
    Route::post('remove_from_wishlist',[wishlistController::class,'remove'])->name('remove_from_wishlist');
    Route::get('add-cart',[wishlistController::class,'add_cart'])->name('add-cart');
});

//Search Route
Route::post('search',[HomeController::class,'search'])->name('search');
Route::get('livesearch',[HomeController::class,'livesearch'])->name('livesearch');
// Password reset routes...
Route::get('forget-password', [ForgotPasswordController::class, 'showForgetPasswordForm'])->name('forget.password.get');
Route::post('forget-password', [ForgotPasswordController::class, 'submitForgetPasswordForm'])->name('forget.password.post');
Route::get('reset-password/{token}', [ForgotPasswordController::class, 'showResetPasswordForm'])->name('reset.password.get');
Route::post('reset-password', [ForgotPasswordController::class, 'submitResetPasswordForm'])->name('reset.password.post');

//Admin Routes Here
Route::prefix('admin')->middleware(['auth','isAdmin'])->group(function(){

    Route::get('/', function () {
        return redirect()->route('admin.dashboard');
    });
    Route::get('/dashboard', [dashboardController::class, 'index'])->name('admin.dashboard');

    //Product Related Route
    Route::get('/product',[adminproductController::class, 'index'])->name('admin.product');
    Route::get('/add-product',[adminproductController::class, 'add'])->name('admin.add-product');
    Route::get('/edit-product/{id}',[adminproductController::class, 'edit'])->name('admin.edit-product');
    Route::get('/view-product/{id}',[adminproductController::class, 'view'])->name('admin.view-product');
    Route::post('/delete-product',[adminproductController::class, 'delete'])->name('admin.delete-product');
    Route::post('/save-product',[adminproductController::class, 'save_product'])->name('admin.save-product');
    Route::post('/save-edit-product/{id}',[adminproductController::class, 'save_edit_product'])->name('admin.save-edit-product');
    Route::get('/getproductvariant',[adminproductController::class, 'getproductvariant'])->name('admin.getproductvariant');

    //Category Related Route
    Route::get("/category",[CategoryController::class,'index'])->name('admin.category');
    Route::get('/category-add', function () {return view('backend.pages.category.add');})->name('admin.category-add');
    Route::post("/category-add",[CategoryController::class,'add'])->name('admin.category-add');
    Route::get("/category-edit/{id}",[CategoryController::class,'edit'])->name('admin.category-edit');
    Route::post('/delete-category',[CategoryController::class, 'delete'])->name('admin.delete-category');
    Route::post("/category-update",[CategoryController::class,'update'])->name('admin.category-update');

    //User Related Route
    Route::get('/user', [AdminUserController::class, 'index'])->name('admin.user');
    Route::get('/edit-user/{id}', [AdminUserController::class, 'edit'])->name('admin.edit-user');
    Route::post('/save-edit-user', [AdminUserController::class, 'update'])->name('admin.save-edit-user');
    // Route::delete('/user/{id}', [AdminUserController::class, 'destroy'])->name('user.destroy');
    Route::post('/delete-user', [AdminUserController::class, 'destroy'])->name('admin.delete-user');

    //Color Related Route
    Route::get("/color",[ColorController::class,'index'])->name('admin.color');

    Route::get('/color-add', function () {
        return view('backend.pages.color.add');
    })->name('admin.color-add');

    Route::post("/color-add",[ColorController::class,'add'])->name('admin.color-add');
    Route::get("/color-edit/{id}",[ColorController::class,'edit'])->name('admin.color-edit');
    Route::post("/color-update",[ColorController::class,'update'])->name('admin.color-update');
    Route::post('/delete-color',[ColorController::class, 'delete'])->name('admin.delete-color');

    //Size Related Route
    Route::get("/size",[SizeController::class,'index'])->name('admin.size');

    Route::get('/size-add', function () {
        return view('backend.pages.size.add');
    })->name('admin.size-add');

    Route::post("/size-add",[SizeController::class,'add'])->name('admin.size-add');
    Route::get("/size-edit/{id}",[SizeController::class,'edit'])->name('admin.size-edit');
    Route::post("/size-update",[SizeController::class,'update'])->name('admin.size-update');
    Route::post('/delete-size',[SizeController::class, 'delete'])->name('admin.delete-size');

    //Order Listing  Route
    Route::get('/view-order', [AdminOrderController::class, 'show'])->name('admin.view-order');
    Route::get('/view-order/{order_id?}', [AdminOrderController::class, 'order_detail'])->name('admin.view-orderdetail');
    Route::post('/update-order-status',[AdminOrderController::class,'updateOrderStatus'])->name('admin.update-order-status');

});

