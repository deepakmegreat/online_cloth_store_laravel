// alert();
$("body").on("click", ".delete-btn", function() {
    var id = $(this).attr('data-id');
    setTimeout(function() {
        $('.yes-delete:visible').attr('data-id', id);
        $('.yes-delete-category:visible').attr('data-id', id);
        $('.yes-delete-color:visible').attr('data-id', id);
        $('.yes-delete-size:visible').attr('data-id', id);
    }, 500);
});

$('body').on('click', '.yes-delete', function() {
    var id = $(this).attr('data-id');
    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('input[name="_token"]').val(),
        },
        url: "delete-product" ,
        data: { id },
        success: function(data) {
            $("#del_msg").html('<div class="alert alert-success alert-dismissible fade show" role="alert">'+
            '<strong>Product Successfully Deleted.</strong>'+
            '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="btn-close"></button>'+
            '</div>');
            location.href = "product";
        }
    });

});

$('body').on('click', '.yes-delete-category', function() {

    var id = $(this).attr('data-id');

        $.ajax({
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('input[name="_token"]').val(),
            },
            url: "delete-category" ,
            data: { id },
            success: function(data) {
                // console.log(JSON.parse(data));

                window.location.href = "category";
            }
        });

});

$('body').on('click', '.yes-delete-color', function() {

    var id = $(this).attr('data-id');
    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('input[name="_token"]').val(),
        },
        url: "delete-color" ,
        data: { id },
        success: function(data){
            if(data.trim() == "success"){
                window.location.href = "color";
            } 
        }
    });

});

$('body').on('click', '.yes-delete-size', function() {

    var id = $(this).attr('data-id');
    $.ajax({
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('input[name="_token"]').val(),
        },
        url: "delete-size" ,
        data: { id },
        success: function(data){
            if(data.trim() == "success"){
                window.location.href = "size";
            } 
        }
    });

});



