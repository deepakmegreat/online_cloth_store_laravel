  $(document).ready(function(){
     

    $("#category").validate({
      rules: {
        name: {
          required: true,
          maxlength: 20,
          minlength: 3
        },
      },
      
      messages: {
        name: {
          required: "Please enter a Category name",
          maxlength: "category name must consist of at Max 20 characters",
          minlength: "category name must consist of at Min 3 characters"
        },
        // category_image: {
        //   extension: "You're only allowed to upload jpg or png images"
        // },
      },

      errorPlacement: function(error, element) {
        error.addClass( "invalid-feedback" );

        if (element.parent('.input-group').length) {
          error.insertAfter(element.parent());
        }
        else {
          error.insertAfter(element);
        }
      },

    });

    $("#size").validate({
      rules: {
        size: {
          required: true,
          maxlength: 4,
          // regex: "^[A-z]+$",
        }
      },
      
      messages: {
        size: {
          required: "Please enter a size",
          maxlength: "Size must consist of at Max 4 characters",
          // regex: "Invalid Size"
        },
      },

      errorPlacement: function(error, element) {
        error.addClass( "invalid-feedback" );

        if (element.parent('.input-group').length) {
          error.insertAfter(element.parent());
        }
        else {
          error.insertAfter(element);
        }
      },


      // highlight: function(element, errorClass) {
      //   if ($(element).prop('type') != 'checkbox' && $(element).prop('type') != 'radio') {
      //     $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
      //   }
      // },
      // unhighlight: function(element, errorClass) {
      //   if ($(element).prop('type') != 'checkbox' && $(element).prop('type') != 'radio') {
      //     $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
      //   }
      // }
    });

    $("#color").validate({
      rules: {
        colour: {
          required: true,
          maxlength: 20,
          minlength: 3,
        },
        colour_code: {
          required: true,
          maxlength: 8,
        }
      },
      
      messages: {
        colour: {
          required: "Please enter a colour",
          maxlength: "color name must consist of at Max 20 characters",
          minlength: "Color name must consist of at Min 3 characters",
        },
        colour_code: {
          required: "Please enter a colour code",
          maxlength: "color code must consist of at Max 8 characters",
        },
      },

      errorPlacement: function(error, element) {
        error.addClass( "invalid-feedback" );

        if (element.parent('.input-group').length) {
          error.insertAfter(element.parent());
        }
        else {
          error.insertAfter(element);
        }
      },


      // highlight: function(element, errorClass) {
      //   if ($(element).prop('type') != 'checkbox' && $(element).prop('type') != 'radio') {
      //     $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
      //   }
      // },
      // unhighlight: function(element, errorClass) {
      //   if ($(element).prop('type') != 'checkbox' && $(element).prop('type') != 'radio') {
      //     $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
      //   }
      // }
    });
  });