


$(function(){

	$("#signupForm").validate({
          rules: {
            name: {
              required: true,
              minlength: 3
            },
            email: {
              required: true,
              email: true
            },
            password: {
              required: true,
              minlength: 5
            },
            password_confirmation: {
              required: true,
              minlength: 5,
              equalTo: "#password"
            },
            terms_agree: "required"
          },
          
          messages: {
            name: {
              required: "Please Enter Name",
              minlength: "Name must consist of at least 3 characters"
            },
            email: {
              required: "Please Enter Email Address",
              email: "Please Enter Valid Email Address"
            },
            password: {
              required: "Please Enter Password",
              minlength: "Your password must be at least 5 characters long"
            },
            password_confirmation: {
              required: "Please confirm your password",
              minlength: "Your password must be at least 5 characters long",
              equalTo: "Please enter the same password as above"
            },
            terms_agree: "Please agree to terms and conditions"
          },

          errorPlacement: function(error, element) {
            error.addClass( "invalid-feedback" );

            if (element.parent('.input-group').length) {
              error.insertAfter(element.parent());
            }
            else {
              error.insertAfter(element);
            }
          },


          // highlight: function(element, errorClass) {
          //   if ($(element).prop('type') != 'checkbox' && $(element).prop('type') != 'radio') {
          //     $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
          //   }
          // },
          // unhighlight: function(element, errorClass) {
          //   if ($(element).prop('type') != 'checkbox' && $(element).prop('type') != 'radio') {
          //     $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
          //   }
          // }
        })

  $("#loginForm").validate({
          rules: {
            email: {
              required: true,
              email: true
            },
            password: {
              required: true,
            },
          },
          
          messages: {
            email: {
              required: "Please Enter Email Address",
              email: "Please Enter Valid Email Address"
            },
            password: {
              required: "Please Enter Password",
            }
          },

          errorPlacement: function(error, element) {
            error.addClass( "invalid-feedback" );

            if (element.parent('.input-group').length) {
              error.insertAfter(element.parent());
            }
            else {
              error.insertAfter(element);
            }
          },


          // highlight: function(element, errorClass) {
          //   if ($(element).prop('type') != 'checkbox' && $(element).prop('type') != 'radio') {
          //     $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
          //   }
          // },
          // unhighlight: function(element, errorClass) {
          //   if ($(element).prop('type') != 'checkbox' && $(element).prop('type') != 'radio') {
          //     $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
          //   }
          // }
        })

  $("#checkOut").validate({
      rules: {
        firstname: {required: true},
        
        lastname: {required: true},
        
        city: {required: true},
        
        state: {required: true},
        
        country: {required: true},
        
        address:  {required: true},
       
        phone: {
          required: true,
          digits:true,
          minlength:10,
          maxlength:10
        },

        postcode: {
          required: true,
          digits:true,
          minlength:6,
          maxlength:6
        },

      },
      
      messages: {
        firstname:  "Please enter a firstname",
        lastname: "Please enter a lastname",
        city: "Please enter a city",
        state: "Please enter a state",
        country: "Please enter a country",
        address: "Please enter a address",
        
        phone: {
            required: "Please enter a mobile number",
            minlength: "Mobile number must be of 10 digits",
            maxlength: "Mobile number must be of 10 digits",
            digits: "Enter number only"
        },
        postcode: {
            required: "Please enter a postcode",
            minlength: "Mobile number must be of 6 digits",
            maxlength: "Mobile number must be of 6 digits",
            digits: "Enter number only"
        },

      },

      errorPlacement: function(error, element) {
        error.addClass( "invalid-feedback" );

        if (element.parent('.input-group').length) {
          error.insertAfter(element.parent());
        }
        else {
          error.insertAfter(element);
        }
      },

      // highlight: function(element, errorClass) {
      //   if ($(element).prop('type') != 'checkbox' && $(element).prop('type') != 'radio') {
      //     $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
      //   }
      // },

      // unhighlight: function(element, errorClass) {
      //   if ($(element).prop('type') != 'checkbox' && $(element).prop('type') != 'radio') {
      //     $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
      //   }
      // }

    });

});