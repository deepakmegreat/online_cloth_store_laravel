<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

use App\Models\Product;
use App\Models\Product_sizes;
use App\Models\Product_colours;

class Category extends Model
{
    use Sortable;
    use HasFactory, SoftDeletes;
    public $timestamps=false;
    protected $table = 'categories';

    public $sortable=['id',
    'name',
    'status'];

    public function getCategory(){
        return Category::where('status','A')->get()->toArray();
    }
}
