<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\ProductSize;
use App\Models\ProductColours;
use App\Models\Product_sizes;
use App\Models\Product_colours;
use App\Models\ProductVariant;
use App\Models\Sizes;
use App\Models\Colours;
use App\Models\Category;
use Kyslik\ColumnSortable\Sortable;

class Product extends Model
{
    use HasFactory, SoftDeletes,Sortable;

    protected $table = 'products';

    public $sortable=[
        'id',
        'name',
        'status',
        'price',
        'quantity'
    ];

    //Product Variant Get
    public function ProductVariant()
    {
        return $this->hasMany(ProductVariant::class, 'product_id', 'id');
    }

    // Product Sizes Relation
    public function productSizes()
    {
        return $this->hasMany(ProductSize::class, 'product_id', 'id');
    }

    // Product Colors Relation
    public function productColors()
    {
        return $this->hasMany(ProductColours::class, 'product_id', 'id');
    }

    //Get The Product Size
    public function prodSize()
    {
        return $this->hasMany(ProductSize::class, 'product_id', 'id')
                    ->join('sizes', 'sizes.id', '=', 'product_sizes.size')
                    ->orderBy('product_sizes.size');
    }

    //Get The Product Color
    public function prodColor()
    {
        return $this->hasMany(ProductColours::class, 'product_id', 'id')
                    ->join('colours', 'colours.id', '=', 'product_colours.colour_id')
                    ->orderBy('product_colours.colour_id');
    }

    public function getDetails($request){
        $product = Product::sortable()->where('deleted_at',null);
        if ($request->has('search_submit') && $request->search_submit != '') {
            if ($request->has('name') && $request->name != '') {
                $product->where('name',  'like', '%'.$request->name.'%');
            }
            if ($request->has('status') && $request->status != '') {
                $product->where('status',  $request->status);
            }
            if ($request->has('category') && $request->category != '') {
                $product->where('category_id',  $request->category);
            }
            if ($request->has('size') && $request->size != '') {
                $allProdId = Product_sizes::select('product_id')->whereIn('size',$request->size)->get()->toArray();
                $product->whereIn('id', $allProdId);
            }
            if ($request->has('color') && $request->color != '') {
                $allProdId = Product_colours::select('product_id')->whereIn('colour_id',$request->color)->get()->toArray();
                $product->whereIn('id', $allProdId);
            }
        }
        return $product->orderBy('id', 'DESC')->paginate(10);
    }



    // public function saveProduct($req){

    //     $product = new Product;
    //     $product->name = $req->name;
    //     $product->category_id = $req->category_id;
    //     $product->price = $req->price;
    //     $product->quantity = $req->quantity;
    //     $product->status = $req->status;
    //     $product->description = $req->description;
    //     if($req->file('image')==''){
    //         $img = 'defaultproduct.jpg';
    //         $product->image = $img;
    //     }else{
    //         $img = $req->file('image')->storeAs('',time().'.png');
    //         $product->image = $img;
    //         $req->file('image')->move(public_path('backend/images/uploads/products/'), $img);
    //     }

    //     if($product->save()){
    //         $insertedId = $product->id;

    //         //Add Product Size
    //         foreach ($req->size as  $val)
    //         {
    //             $sizes = new Product_sizes;
    //             $size_id = Sizes::where('size',$val)->first();
    //             $sizes->size = $size_id->id;
    //             $sizes->product_id = $insertedId;
    //             $sizes->status = $req->status;
    //             $sizes->save();
    //         }

    //         //Add Product Colour
    //         foreach ($req->colour as  $val)
    //         {
    //             $colours = new Product_colours();
    //             $color_id = Colours::where('colour_code',$val)->select()->get()->toArray();
    //             $colours->colour_id = $color_id[0]['id'];
    //             $colours->product_id = $insertedId;
    //             $colours->status = $req->status;
    //             $colours->save();
    //         }
    //         return true;

    //     }
    //     else{
    //         return false;
    //     }
    // }

    // public function saveEditProduct($req){
    //     // dd($req);
    //     $product = Product::find($req->pid);
    //     $product->name = $req->name;
    //     $product->category_id = $req->category_id;
    //     $product->price = $req->price;
    //     $product->quantity = $req->quantity;
    //     $product->status = $req->status;
    //     $product->description = $req->description;
    //     if($req->file('image')!=''){
    //         if(file_exists('backend/images/uploads/products/'.$product['image']) && $product->image != null && $product->image != "defaultproduct.jpg"){
    //             unlink('backend/images/uploads/products/'.$product->image);
    //         }

    //         $img = $req->file('image')->storeAs('',time().'.png');
    //         $product->image = $img;
    //         $req->file('image')->move(public_path('backend/images/uploads/products'), $img);
    //     }
    //     else{
    //         if(!file_exists('backend/images/uploads/products/'.$product['image'])){
    //             $img = 'defaultproduct.jpg';
    //             $product->image = $img;
    //         }
    //     }

    //     if($product->save()){
    //         $insertedId = $req->pid;

    //         $requested_size = array();
    //         foreach ($req->size as  $val){
    //             $size_id = Sizes::where('size',$val)->select()->get()->toArray();
    //             $requested_size_id = $size_id[0]['id'];
    //             array_push($requested_size,$requested_size_id);
    //         }

    //         foreach ($req->size as  $val)
    //         {
    //             $size_id = Sizes::where('size',$val)->select()->get()->toArray();
    //             $requested_size_id = $size_id[0]['id'];
    //             $stored_product = Product_sizes::where('product_id',$insertedId)->where('size',$requested_size_id)->select()->get()->toArray();
    //             if(count($stored_product)>0){
    //                 $unmatched_stored_product = Product_sizes::where('product_id',$insertedId)->whereNotIn('size',$requested_size)->delete();
    //             }
    //             else{
    //                 $unmatched_stored_product = Product_sizes::where('product_id',$insertedId)->whereNotIn('size',$requested_size)->delete();
    //                 $sizes = new Product_sizes;
    //                 $size_id = Sizes::where('size',$val)->select()->get()->toArray();
    //                 $sizes->size = $size_id[0]['id'];
    //                 $sizes->product_id = $insertedId;
    //                 $sizes->status = $req->status;
    //                 $sizes->save();
    //             }

    //         }

    //         // dd($req->colour);
    //         $requested_colors = array();
    //         foreach ($req->colour as  $val){
    //             $color_id = Colours::where('id',$val)->select()->get()->toArray();
    //             $requested_color_id = $color_id[0]['id'];
    //             array_push($requested_colors,$requested_color_id);
    //         }
    //         // dd($requested_colors);

    //         foreach ($req->colour as  $val)
    //         {
    //             $color_id = Colours::where('id',$val)->select()->get()->toArray();
    //             $requested_color_id = $color_id[0]['id'];
    //             $stored_product_color = Product_colours::where('product_id',$insertedId)->where('colour_id',$requested_color_id)->select()->get()->toArray();
    //             if(count($stored_product_color)>0){
    //                 $unmatched_stored_product_color = Product_colours::where('product_id',$insertedId)->whereNotIn('colour_id',$requested_colors)->delete();
    //             }
    //             else{
    //                 $unmatched_stored_product_color = Product_colours::where('product_id',$insertedId)->whereNotIn('colour_id',$requested_colors)->delete();
    //                 $colors = new Product_colours;
    //                 $color_id = Colours::where('id',$val)->select()->get()->toArray();
    //                 $colors->colour_id = $color_id[0]['id'];
    //                 $colors->product_id = $insertedId;
    //                 $colors->status = $req->status;
    //                 $colors->save();
    //             }

    //         }

    //         return true;
    //     }
    //     else{
    //         return false;
    //     }
    // }

    public function getSizeNameByProductId($id){
        $allSizeOfProduct = Product_sizes::where('product_id',$id)->pluck('size')->toArray();
        $allSizeName = Sizes::whereIn('id',$allSizeOfProduct)->select('size')->get()->toArray();
        return $allSizeName;
    }

    public function getColorNameByProductId($id){
        $allColorOfProduct = Product_colours::where('product_id',$id)->pluck('colour_id')->toArray();
        $allColorName = Colours::whereIn('id',$allColorOfProduct)->select('colour')->get()->toArray();
       
        return $allColorName;
    }

    public function getCategoryNameByProductId($id){
        $Product = Product::where('id',$id)->first();
        $categoryName = Category::where('id',$Product->category_id)->select('name')->get()->toArray();
        return $categoryName;

    }
}
