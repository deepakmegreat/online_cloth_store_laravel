<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product;
use App\Models\Orders;

class OrderDetails extends Model
{
    use HasFactory;
    protected $table = 'orders_details';

    function getproductDetail(){
         return $this->hasMany(Product::class, 'id', 'product_id');
    }

}
