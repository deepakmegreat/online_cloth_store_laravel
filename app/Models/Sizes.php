<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Sizes extends Model
{
    use Sortable;
    use HasFactory;
    protected $table = 'sizes';

    public $sortable = [
        'size'
    ];


    public function getAllSizes(){
        return Sizes::get()->toArray();
    }

}
