<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\OrderDetails;
use Kyslik\ColumnSortable\Sortable;


class Orders extends Model
{
    use Sortable;
    use HasFactory;
    protected $table = 'orders';
   
    public $sortable=['id',
    'created_at',
    'first_name',
    'grand_total',
    'email',
    'product_name',
    'order_status'];
   

    function getOrderDetail(){
         return $this->hasMany(OrderDetails::class, 'order_id', 'id');
    }

}
