<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Colours extends Model
{
    use Sortable;
    use HasFactory;
    protected $table = 'colours';
    public $fillable = [
        'colour'
    ];
    public $sortable = [
        'colour',
        'colour_code'
    ];

    public function getAllColours(){
        return Colours::select()->get()->toArray();
    }
}
