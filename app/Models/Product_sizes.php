<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Product_sizes extends Model
{
    use HasFactory, SoftDeletes;

   public function getSizes($pid){
    return Product_sizes::join('sizes','sizes.id','=','product_sizes.size')->
                select()->where('product_id',$pid)->get()->toArray();
   }

}
