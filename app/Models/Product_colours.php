<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Product_colours extends Model
{
    use HasFactory, SoftDeletes;

    public function getColours($pid){
    return Product_colours::select()->where('product_id',$pid)->get();
    }

}
