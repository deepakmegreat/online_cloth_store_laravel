<?php
namespace App\Http\Controllers\front_theme;

use App\Http\Controllers\front_theme\Controller;
use Illuminate\Http\Request;
use App\Models\Wishlist;
use App\Models\Product;
use App\Models\Cart;
use Illuminate\Support\Facades\Auth;


class wishlistController extends Controller
{
    //This function will display cart product 
    public function wishlist(){
        return view('front_theme.wishlist');
    }

    public function addToWishlist(Request $request){
        if(Auth::user())
        {
            $id = checkWishlistForItem(Auth::user()->id,$request);
            if(!empty($id)){
                session()->flash('success_wishlist','Product already in wishlist');
                return true;
            }
            else{
                $wishlist = new Wishlist();
                $wishlist->user_id = Auth::user()->id;
                $wishlist->product_id = $request->id;
                $wishlist->color = $request->color;
                $wishlist->size = $request->size;
                $wishlist->photo = $request->photo;
                $wishlist->product_name = $request->product_name;
                $wishlist->Price = $request->price;
                $wishlist->quantity = $request->quantity;
                $wishlist->save();    
                session()->flash('success_wishlist','Product add to Wishlist successfully');
                return true;
            }   
        }
    }

    function remove(Request $request){
        if(Auth::user()){
            $wishlist = Wishlist::find($request->id);
            $wishlist->delete();
        }
    
        session()->flash('success','Products Successfully removed');
    }

    function add_cart(Request $request){
        if(Auth::user()){

            // $id = checkCartForItem(Auth::user()->id,$request);
            $allWishlist = Wishlist::all()->toArray();
        
            foreach ($allWishlist as $wishlist) {
                $id = checkCartForItem2(Auth::user()->id,$wishlist['product_id'],$wishlist['color'],$wishlist['size']);
                
                if(!empty($id)){
                    $cart = Cart::find($id);
                    $cart->quantity += $wishlist['quantity'];
                    $cart->save();
                    Wishlist::where('product_id',$wishlist['product_id'])->delete();
                }
                else
                {
                    $cart = new Cart();
                    $cart->Userid = Auth::user()->id;
                    $cart->Productid = $wishlist['product_id'];
                    $cart->color = $wishlist['color'];
                    $cart->size = $wishlist['size'];
                    $cart->photo = $wishlist['photo'];
                    $cart->product_name = $wishlist['product_name'];
                    $cart->Price = $wishlist['price'];
                    $cart->quantity = $wishlist['quantity'];
                    $cart->save();
                    Wishlist::where('product_id',$wishlist['product_id'])->delete();
                }
            }
        }
        return redirect('wishlist')->with('success','Product added to cart successfully!');
    }
}
