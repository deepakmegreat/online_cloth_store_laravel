<?php

namespace App\Http\Controllers\front_theme;

use App\Http\Controllers\front_theme\Controller;
use App\Models\Product;
use App\Models\Category;
use App\Models\Sizes;
use App\Models\Colours;
use App\Models\Cart;
use App\Models\ProductColours;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Session;

class ProductController extends Controller
{
    function shop(Request $request,$id=null)
    {
        $products = $this->prod_query($request);
        $category = Category::orderBy('id')->where('status','=','A')->get();
        $sizes = Sizes::orderBy('id')->get();
        // $productsColor = ProductColours::distinct()->select('colour')->get();
        $productsColor = Colours::all();
        return view('front_theme/shop', compact('products', 'category', 'sizes', 'productsColor','id'));
    }

    function shop_filter(Request $request)
    {
         $products = $this->prod_query($request);
         $view = view('front_theme/filter_product', compact('products'))->render();
         return response()->json(['html' => $view]);
    }

    function prod_query($request)
    {
        //DB::enableQueryLog();
        $products = Product::where('products.status','=','A')
                    ->select('products.*')
                    ->leftJoin('categories', 'categories.id','products.category_id')
                    ->where('categories.status','=','A');

        if ($request->has('minamount') && $request->has('maxamount')) {
            $products->whereBetween('price', [$request->minamount,$request->maxamount]);
        }

        if($request->has('categoryid') && !empty($request->categoryid)){
            $products->where('category_id', $request->categoryid);
        };
        if($request->has('sizeId') && !empty($request->sizeId)){
            $sizeIds = explode(',', $request->sizeId);
             $products->leftJoin('product_sizes', 'product_sizes.product_id','products.id')
                    ->whereNull('product_sizes.deleted_at')
                     ->whereIn('size', $sizeIds)
                     ->groupBy('products.id');
            }
        
        if($request->has('colorId') && !empty($request->colorId)){
             $colorIds = explode(',', $request->colorId);
             $products->leftJoin('product_colours', 'product_colours.product_id','products.id')
                    ->whereNull('product_colours.deleted_at')
                    ->whereIn('colour_id', $colorIds)
                    ->groupBy('products.id');
        }

        $products = $products->paginate(6);
        // dd(DB::getQueryLog($products));
        //dd($products);
        return $products;
    }
    //This function display Product detail page
    function view(Request $request)
    {
        $id = $request->id;
        $product = Product::with(['prodSize','prodColor'])->where('id',$id)->first();
//$categoryid = Product::find($id)->category_id;
        $relatedProduct = Product::where('category_id','=',$product->category_id)
                            ->where('id','<>',$id)->inRandomOrder()->take(4)->get();
        return view('front_theme/product-details',compact('product','relatedProduct'));
    }

    //This function will work for Add to cart functionality
    function addToCart(Request $request){
        $product = Product::findOrFail($request->id);
        if(Auth::user())
        {
            $id = checkCartForItem(Auth::user()->id,$request);
            if(!empty($id)){
                $cart = Cart::find($id);
                $cart->quantity += $request->quantity;
                $cart->save();
            }else
            {
                $cart = new Cart();
                $cart->Userid = Auth::user()->id;
                $cart->Productid = $request->id;
                $cart->color = $request->color;
                $cart->size = $request->size;
                $cart->photo = $request->photo;
                $cart->product_name = $request->product_name;
                $cart->Price = $request->price;
                $cart->quantity = $request->quantity;
                $cart->save();
            }
        }
        else
        {
            $cart = session()->get('cart', []);
            if(isset($cart[$request->id]))
            {
                //If the color array variable
                if(isset($cart[$request->id]['color'][$request->color])) {
                    //If the size variable available then work
                    if(isset($cart[$request->id]['color'][$request->color]['size'])) {
                        //if size Alreay then quantity ++
                        if(isset($cart[$request->id]['color'][$request->color]['size'][$request->size]))
                        {
                            $cart[$request->id]['color'][$request->color]['size'][$request->size]['quantity'] += $request->quantity;
                            $cart[$request->id]['color'][$request->color]['size'][$request->size]['price'] = $request->price;
                        }
                        else
                        {
                            $cart[$request->id]['color'][$request->color]['size'][$request->size] = [
                                "quantity" => $request->quantity,
                                "price"=> $request->price
                            ];
                        }
                    }
                }
                else
                {
                        $cart[$request->id]['color'][$request->color] = [
                            "size" => [
                                $request->size => [
                                    "quantity" => $request->quantity,
                                    "price"=> $request->price
                                ],
                            ],
                        ];
                }
            }
            else
            {
                $cart[$request->id] = [
                    "product_name" => $product->name,
                    "photo" => $product->image,
                    "price" => $request->price,
                    "color" => [
                        $request->color => [
                            "size" => [
                                $request->size => [
                                    "quantity" => $request->quantity,
                                    "price"=> $request->price
                                ],
                            ],
                        ],
                    ],
                ];
            }
            session()->put('cart',$cart);
        }

        session()->flash('success','Product add to cart successfully');
        return true;
    }

    //This function will display cart product
    function cart(){
        return view('front_theme.shop-cart');
    }

    //This function remove product from cart
    function remove(Request $request){
        if($request->id){
            $cart = session()->get('cart');
            if(isset($cart[$request->id])){
                if(isset($cart[$request->id]['color'][$request->color]))
                {
                    if(isset($cart[$request->id]['color'][$request->color]['size'][$request->size]))
                    {
                        unset($cart[$request->id]['color'][$request->color]['size'][$request->size]);
                        if(count($cart[$request->id]['color'][$request->color]['size']) == 0){
                            unset($cart[$request->id]['color'][$request->color]);
                        }
                    }
                }
                if(empty($cart[$request->id]['color'])){
                    unset($cart[$request->id]);
                }
                session()->put('cart',$cart);
            }
            else
            {
                if(Auth::user()){
                    $cart = Cart::find($request->id);
                    $cart->delete();
                }
            }
        }
        session()->flash('success','Products Successfully removed');
    }


    //This function Update cart
    function update_cart(Request $request){
        if($request->id && $request->quantity){
            $cart = session()->get('cart');
            if(isset($cart[$request->id])){
                if(isset($cart[$request->id]['color'][$request->color]))
                {
                    if(isset($cart[$request->id]['color'][$request->color]['size'][$request->size]))
                    {
                        $cart[$request->id]['color'][$request->color]['size'][$request->size]['quantity'] = $request->quantity;
                    }
                }
                session()->put('cart',$cart);
            }
            else
            {
                if(Auth::user()){
                    $cart = Cart::find($request->id);
                    $cart->quantity = $request->quantity;
                    $cart->save();
                }
            }
        }
        session()->flash('success','Cart Successfully Updated!');
    }
}
