<?php

namespace App\Http\Controllers\front_theme;

use App\Http\Controllers\front_theme\Controller;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    // this function will display Home Page
    function index()
    {
        return view('front_theme/contact');
    }
}
