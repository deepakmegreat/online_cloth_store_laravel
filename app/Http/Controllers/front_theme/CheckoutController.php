<?php

namespace App\Http\Controllers\front_theme;

use App\Http\Controllers\front_theme\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Product;

class CheckoutController extends Controller
{
    //this function will display checkout Page
    function index()
    {
        if(Auth::user()){
            $cart_data = find_cart_login_user(Auth::user()->id);
            if(count($cart_data) == 0){
                $message = "Shopping Cart is empty! Please add Products to checkout";
                return redirect('cart')->with('error_message',$message);
            }
            else{
                $msg = '';
                $cart_data = find_cart_login_user(Auth::user()->id);
                foreach($cart_data as $details){
                    $Product = Product::find($details['Productid']);
                    if($Product->quantity == 0){
                        $msg .= $Product->name . " is Out of Stock. <br>";
                        continue;
                    }
                    else if($Product->quantity < $details['quantity']){
                        $msg .= $Product->name . " Has Only " . $Product->quantity . " Quantity Left.<br>";
                    }
                }
                if($msg != ''){
                    session()->flash('error_message', $msg);
                }
            }
        }
        else{
            if(!empty(session('cart'))){
                if(count(session('cart')) == 0){
                    $message = "Shopping Cart is empty! Please add Products to checkout";
                    return redirect('cart')->with('error_message',$message);
                }
                else{
                    $msg = '';
                    foreach(session('cart') as $id=>$details){
                        $Product = Product::find($id);
                        if($Product->quantity == 0){
                            $msg .= $Product->name . " is Out of Stock. <br>";
                            continue; // Skip the rest of the loop if product is out of stock
                        }
                        foreach ($details['color']  as $color => $size){
                            foreach ($size['size'] as $sizelabel => $sizename){
                                if($Product->quantity < $sizename['quantity']){
                                    $msg .= $Product->name . " Has Only " . $Product->quantity . " Quantity Left.<br>";
                                }
                            }
                        }
                    }
                    if($msg != ''){
                        session()->flash('error_message', $msg);
                    }
                }
            }
            else{
                $message = "Shopping Cart is empty! Please add Products to checkout";
                return redirect('cart')->with('error_message',$message);
            }
        }
        return view('front_theme/checkout');
    }
}
