<?php

namespace App\Http\Controllers\front_theme;

use App\Http\Controllers\front_theme\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Orders;
use App\Models\Cart;
use App\Models\Product;
use App\Models\OrderDetails;
use Session;

class OrderController extends Controller
{
    function place_order(Request $request){
        $request->validate([
            'firstname'=>'required ',
            'lastname'=>'required ',
            'city'=>'required ',
            'state'=>'required ',
            'country'=>'required ',
            'phone'=>'required | numeric | digits:10 ',
            'address'=>'required',
            'postcode'=>'required | numeric | digits:6 ',
        ]);

        if($request->isMethod('post'))
        {
            $msg='';

            $data = $request->all();
            if($data['payment_gateway']=='cod'){
                $payment_method = "cod";
                $order_status = "New";
            }
            $total = 0;
            $cart_data = find_cart_login_user(Auth::user()->id);
            foreach($cart_data as $details){
                $Product = Product::find($details['Productid']);

                if($Product->quantity == 0){
                    $msg .= $Product->name . " is Out of Stock. <br>";
                    continue;
                }
                else if($Product->quantity < $details['quantity']){
                    $msg .= $Product->name . " Has Only " . $Product->quantity . " Quantity Left.<br>";
                }
            }
            if($msg != ''){
                session()->flash('error_message', $msg);
                return redirect('/checkout');
            }
            foreach($cart_data as $details){
                $total += $details['Price'] * $details['quantity'];
            }
            $grand_total = $total;

            //insert Order Details
            $order = new Orders();
            $order->user_id = Auth::user()->id;
            $order->first_name = $data['firstname'];
            $order->last_name = $data['lastname'];
            $order->city = $data['city'];
            $order->state = $data['state'];
            $order->country = $data['country'];
            $order->phone = $data['phone'];
            $order->address = $data['address'];
            $order->postcode = $data['postcode'];
            $order->email =  Auth::user()->email;
            $order->order_status = $order_status;
            $order->payment_method = $payment_method;
            $order->payment_gateway = $data['payment_gateway'];
            $order->grand_total = $grand_total;
            $order->save();
            $order_id = $order->id;
            session()->put('orderid',$order_id);
            session()->put('grandtotal',$grand_total);

            foreach($cart_data as $details){
                $cartItem = new OrderDetails();
                $cartItem->order_id = $order_id;
                $cartItem->product_id = $details['Productid'];
                $cartItem->product_name  = $details['product_name'];
                $cartItem->product_color = $details['color'];
                $cartItem->product_size = $details['size'];
                $cartItem->product_price = $details['Price'];
                $cartItem->product_qty = $details['quantity'];
                $cartItem->save();
            }
        }
        // return view('front_theme/order-success');
        return redirect('/order-success');

    }

    function order_success(Request $request){
        $orderid = session()->get('orderid');
        if(Session::has('orderid')){
            Cart::where('Userid',Auth::user()->id)->delete();
            $order = Orders::where('id',$orderid)->first()->toArray();
            $order_details = OrderDetails::with('getproductDetail')->where('order_id',$orderid)->get()->toArray();
            foreach ($order_details as $details){
                $product = Product::find($details['product_id']);
                $product->quantity -= $details['product_qty'];
                $product->save();
            }
            return view('front_theme.order-success',compact('orderid','order','order_details'));
        }else{
            return redirect('cart');
        }

    }

    function my_order($order_id = null){

        $user_id= Auth::user()->id;
        $orders=Orders::with('getOrderDetail')->where('user_id',$user_id)->get()->toArray();

         if($orders==null){
             return view('front_theme.my_order')->with(compact('orders'));
         }else{
             foreach ($orders as $val) {
                 foreach ($val['get_order_detail'] as $key => $orderId) {
                     $order_ids[] = $orderId['order_id'];
                 }
             }
             $total_order_id = array_unique($order_ids);
             //dd($order_id);
             if(!empty($order_id)){
                if (in_array($order_id, $total_order_id)) {
                    $orders = Orders::with('getOrderDetail')->where('id', $order_id)->first()->toArray();
                    return view('front_theme.order_details')->with(compact('orders'));
                } else {
                    return redirect('my-order');
                }
             }else{
                return view('front_theme.my_order')->with(compact('orders'));
             }
         }

     }

    function cancelorder(Request $request){
        $data = $request->all();
        $order_status = Orders::where('id',$data['order_id'])->update(['order_status'=>'Cancelled']);
        $order = Orders::select('order_status')->where('id',$data['order_id'])->first()->toArray();
        $order_status = $order['order_status'];
        $orderdetails = OrderDetails::select('product_qty','product_id')->where('order_id',$data['order_id'])->get()->toArray();
        if($order_status == 'Cancelled'){
                foreach($orderdetails as $val){
                    $prod_qty =  $val['product_qty'];
                    $prod_id =  $val['product_id'];
                    $check_id_exists = Product::where('id', '=', $prod_id)->first();
                    if($check_id_exists!=null){
                        // OrderDetails::where('order_id',$data['order_id'])->update(['product_qty'=>0]);
                        $product = Product::find($prod_id);
                        $product->quantity += $prod_qty;
                        $product->save();
                    }
                }
        }
        $message = "Order has been Cancelled successfully";
        return redirect()->back()->with('success',$message);
    }
 }


