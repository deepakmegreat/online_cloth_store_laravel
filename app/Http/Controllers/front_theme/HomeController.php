<?php

namespace App\Http\Controllers\front_theme;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\front_theme\Controller;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    // this function will display Home Page
    function index(){
        $categorys = Category::limit(3)->where('status','=','A')->get();
        $products = Product::limit(8)
            ->leftJoin('categories', 'categories.id','products.category_id')
            ->select('products.*')
            ->where('products.status','=','A')
            ->where('categories.status','=','A')
            ->get();
        return view('front_theme/index',compact('products','categorys'));

    }

    function register()
    {
        return view('front_theme/register');
    }

    function login()
    {
        return view('front_theme/login');
    }

    public function logout(Request $request){
        Auth::logout();
        return redirect('/');
    }

    public function product_filter(Request $req){

       if($req->id){
            $products = Product::limit(8)->where('status','=','A')->where('category_id','=',$req->id)->get();

        }
        else{
            $products = Product::limit(8)
            ->leftJoin('categories', 'categories.id','products.category_id')
            ->select('products.*')
            ->where('products.status','=','A')
            ->where('categories.status','=','A')
            ->get();

        }
       $view = view('front_theme/category_product_filter', compact('products'))->render();
       return response()->json(['html' => $view]);
    }

    public function search(Request $req){
        $search=$req->search;
        if(!empty($req->search)){
        $products = Product::where('products.status','=','A')
        ->where('name','Like',$search.'%')
        ->get();
         return view('front_theme/search',compact('products','search'));
        }else{
          return redirect()->route('index');
        }
    }

    public function livesearch(Request $req){
        if($req->search!=""){
        $output="";
        $products = Product::where('products.status','=','A')
        ->where('name','Like',$req->search.'%')
        ->get();
        if($products)
        {
        foreach ($products as $key => $product) {
        // $output.='<li>'.$product->name.'</li>';
        $output.='<option value='.$product->name.'>'.$product->name.'</option>';
        
        
        }
        return Response($output);
    }
    }

  
       
    }


}
