<?php

namespace App\Http\Controllers\backend;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Orders;
use App\Models\OrderDetails;
use App\Models\OrderStatus;
use App\Models\Product;
use App\Models\User;

class adminOrderController extends Controller
{
    //
    public function show(Request $request)
    {
        // dd($request);
        $orders = Orders::with('getOrderDetail')->sortable();
        if ($request->has('search_submit') && $request->search_submit != '') {
            if ($request->has('name') && $request->name != '') {
                $orders->where(DB::raw("CONCAT(first_name, ' ', last_name)"), $request->name);
            }
            if ($request->has('email') && $request->email != '') {
               $orders->where('user_id', $request->email);
            }
            if ($request->has('orderstatus') && $request->orderstatus != '') {
                $orders->whereIn('order_status',$request->orderstatus);
            }
            if ($request->has('start') && $request->start != '') {
                $orders->whereDate('created_at','>=',$request->start);
            }
            if ($request->has('end') && $request->end != '') {
                $orders->whereDate('created_at','<=',$request->end);
            }

        }
        $users_name = Orders::select('first_name','last_name')->get();
        $users_email = User::select('id','email')->where('role','!=','1')->get();
        
        $allStatus = Orders::select('order_status')->distinct()->get();
        $orders = $orders->orderBy('id','DESC')->paginate(10);
        return view("backend.pages.order.list", compact('orders','users_name','users_email','allStatus','request'));
    }

    public function order_detail($order_id){
        $orders_detail = Orders::with('getOrderDetail')->where('id', $order_id)->get();
        if(count($orders_detail) != 0){
            $orders_detail = $orders_detail->first()->toArray();
            $user_detail = User::where('id', $orders_detail['user_id'])->first()->toArray();
            $orderStatuses= OrderStatus::where('status', 1)->get()->toArray();
            return view('backend.pages.order.view')->with(compact('orders_detail', 'user_detail', 'orderStatuses'));
        }
        else{
            return redirect()->route('admin.view-order');
        }
    }
    public function updateOrderStatus(Request $request){
            $data = $request->all();
            $order_status = Orders::where('id',$data['order_id'])->update(['order_status'=>$data['sltstatus']]);
            $order = Orders::select('order_status')->where('id',$data['order_id'])->first()->toArray();
            $orderdetails = OrderDetails::select('product_qty','product_id')->where('order_id',$data['order_id'])->get()->toArray();
            $order_status = $order['order_status'];
            if($order_status == 'Cancelled'){
                foreach($orderdetails as $val){
                    $prod_qty =  $val['product_qty'];
                    $prod_id =  $val['product_id'];
                    $check_id_exists = Product::where('id', '=', $prod_id)->first();
                    if($check_id_exists!=null){
                        // OrderDetails::where('order_id',$data['order_id'])->update(['product_qty'=>0]);
                        $product = Product::find($prod_id);
                        $product->quantity += $prod_qty;
                        $product->save();
                    }
                }
            }
            $message = "Order Status has been updated successfully";
            return redirect()->back()->with('success',$message);
        }

    public function store(Request $request)
    {
        $product = $request->input('product');
        $quantity = $request->input('quantity');

        // Process the order details as needed

        return response()->json(['message' => 'Order placed Successfully']);
    }

    public function destroy($id)
    {
        // Find the item to be deleted
        $orders = Orders::findOrFail($id);

        // Delete the orders$orders
        $orders->delete();

        // Redirect back to the previous page with a success message
        return redirect()->back()->with('success', 'Item deleted successfully.');
    }
}
