<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Models\Category;
use App\Models\Product;
use App\Models\Product_colours;
use App\Models\Colours;
use App\Models\Product_sizes;
use App\Models\ProductVariant;
use App\Models\Sizes;
use App\Http\Controllers\backend\Controller;

class adminproductController extends Controller
{
    public function add(){
        $data = [];

        $category = new Category();
        $data['category_list'] = $category->getCategory();

        $colorList = Colours::all();
        $sizeList = Sizes::all();
        
        // return view('backend.pages.product.add', ['data'=>$data]);
        return view('backend.pages.product.add', compact('colorList','sizeList','data'));
    }

    public function edit($id){
        $data =[];
        $category = new Category();
        $data['category_list'] = $category->getCategory();
        
        $colorList = Colours::all();
        $sizeList = Sizes::all();
        $Product = Product::with('ProductVariant')->where('id',$id)->first()->toArray();
        
        return view('backend.pages.product.edit', compact('colorList','sizeList','Product','data'));
    }

    public function index(Request $request){
        $obj = new Product;
        $data = $obj->getDetails($request);

        $allStatus = Product::select('status')->distinct()->get();

        $allCategoryId = Product::select('category_id')->distinct()->get()->toArray();
        $allCategoryName = Category::select('id','name')->whereIn('id',$allCategoryId)->get();

        $allSizeId = Product_sizes::select('size')->distinct()->get()->toArray();
        $allSizeName = Sizes::select('id','size')->whereIn('id',$allSizeId)->get();

        $allColorId = Product_colours::select('colour_id')->distinct()->get()->toArray();
        $allColorName = Colours::select('id','colour')->whereIn('id',$allColorId)->get();

        return view('backend.pages.product.list',['data'=> $data, 'status'=>$allStatus, 'category'=> $allCategoryName, 'size'=>$allSizeName, 'color'=>$allColorName, 'request'=>$request] );
    }

    public function view($id){

        $data = Product::find($id);
        if(!empty($data)){
            $size_name = $data->getSizeNameByProductId($id);
            $colour_name = $data->getColorNameByProductId($id);
            $category_name = $data->getCategoryNameByProductId($id);
            return view('backend.pages.product.view', ['data'=>$data,'size_data'=>$size_name,'colour_data'=>$colour_name, 'category_data'=>$category_name]);
        }else{
            return redirect()->route('admin.product');
        }
        
    }

    public function delete(Request $req){
        $product = Product::find($req->id);
        if($product->delete()){
            if(Product_sizes::where('product_id', $req->id)->delete() && Product_colours::where('product_id', $req->id)->delete()){
                session()->flash('success','Products Successfully removed');
            }
        }
        else{
            session()->flash('error','Product not deleted');
        }
    }
     // $messages = [
    //         'color_id.*.required' => 'The color field is required.',
    //         'size_id.*.required' => 'The Size field is required.',
    //         'price.*.required' => 'The Price field is required.',
    //         'quantity.*.required' => 'The Quantity field is required.',
    // ]
    public function save_product(Request $req){
        
        $rules = array(
                'name'=>'required',
                'category_id'=>'required',
                'price_main'=>'required',
                'quantity_main'=>'required',
                'description'=>'required',
                'color_id.*' => 'required|exists:colours,id',
                'size_id.*' => 'required|exists:sizes,id',
                'price.*'=>'required | regex:/^[0-9]+$/u',
                'quantity.*'=>'required | regex:/^[0-9]+$/u',
          );
        $validator = Validator::make($req->all(), $rules);
        
        
        if ($validator->fails()) {
            return Redirect::to('admin/add-product')
                            ->withErrors($validator)
                            ->withInput();
        } 
        else
        {
            $colorArray = array();
            $msg = [];
            if(isset($req->color_id) && !empty($req->color_id)){
                foreach ($req->color_id as $colorkey => $color) {
                    foreach ($req->size_id as $sizekey => $size) {
                        if($colorkey == $sizekey){
                            if(isset($colorArray[$color])){
                                if(in_array($size,$colorArray[$color])){
                                    $msg[] = "In Color " . getColorName($color) . " Size " . getSizeName($size) . " Already Exist";           

                                }else{
                                    array_push($colorArray[$color],$size);
                                }
                            }
                            else{
                                $colorArray[$color] = array($size);
                            }
                        }
                    }
                }
            }
            if(!empty($msg)){
                return redirect('admin/add-product')->with('error',$msg);
            }else{
                $product = new Product;
                $product->name = $req->name;
                $product->category_id = $req->category_id;
                $product->price = $req->price_main;
                $product->quantity = $req->quantity_main;
                $product->status = $req->status;
                $product->description = $req->description;
                if($req->file('image')==''){
                    $img = 'defaultproduct.jpg';
                    $product->image = $img;
                }else{
                    $img = $req->file('image')->storeAs('',time().'.png');
                    $product->image = $img;
                    $req->file('image')->move(public_path('backend/images/uploads/products/'), $img);
                }

                $product->save();
                $insertedId = $product->id;
                if(isset($insertedId)){
                    if(isset($req->color_id) && !empty($req->color_id)){
                        foreach ($req->color_id as $key => $color) {
                            $colour = $req->color_id[$key];
                            $size = $req->size_id[$key];
                            $price = $req->price[$key];
                            $quantity = $req->quantity[$key];
                            $ProductVariant = new ProductVariant();
                            $ProductVariant->product_id = $insertedId;
                            $ProductVariant->colour = $colour;
                            $ProductVariant->size = $size;
                            $ProductVariant->price = $price;
                            $ProductVariant->quantity = $quantity;
                            $ProductVariant->save();
                            }    
                        }
                    }
                    return redirect()->route('admin.product')->with('success','Product added successfully!');
                }
            }
            
    }

    public function  save_edit_product(Request $req,$id){
        $rules = array(
                'name'=>'required',
                'category_id'=>'required',
                'price_main'=>'required',
                'quantity_main'=>'required',
                'description'=>'required',
                'color_id.*' => 'required|exists:colours,id',
                'size_id.*' => 'required|exists:sizes,id',
                'price.*'=>'required | regex:/^[0-9]+$/u',
                'quantity.*'=>'required | regex:/^[0-9]+$/u',
          );
        $validator = Validator::make($req->all(), $rules);
        
        
        if ($validator->fails()) {
            return Redirect::to('admin/edit-product/'. $id)
                            ->withErrors($validator)
                            ->withInput();
        } 
        else
        {
            dd($req);
        }
    }
        // $product = Product::find($req->pid);
        // $product->name = $req->name;
        // $product->category_id = $req->category_id;
        // $product->price = $req->price;
        // $product->quantity = $req->quantity;
        // $product->status = $req->status;
        // $product->description = $req->description;
        // if($req->file('image')!=''){
        //     if(file_exists('backend/images/uploads/products/'.$product['image']) && $product->image != null && $product->image != "defaultproduct.jpg"){
        //         unlink('backend/images/uploads/products/'.$product->image);
        //     }

        //     $img = $req->file('image')->storeAs('',time().'.png');
        //     $product->image = $img;
        //     $req->file('image')->move(public_path('backend/images/uploads/products'), $img);
        // }
        // else{
        //     if(!file_exists('backend/images/uploads/products/'.$product['image'])){
        //         $img = 'defaultproduct.jpg';
        //         $product->image = $img;
        //     }
        // }

        // $product->save();
        // $insertedId = $req->pid;

        // // delete size
        // $requested_size = array();
        // $size_id = Sizes::whereIn('size',$req->size)->get();
        // foreach ($size_id as $val){
        //     array_push($requested_size, $val->id);
        // }

        // foreach ($size_id as  $val)
        // {
        //     $stored_product = Product_sizes::where('product_id',$insertedId)->where('size',$val->id)->get();
        //     if(count($stored_product)>0){
        //         $unmatched_stored_product = Product_sizes::where('product_id',$insertedId)->whereNotIn('size',$requested_size)->delete();
        //     }
        //     else{
        //         $unmatched_stored_product = Product_sizes::where('product_id',$insertedId)->whereNotIn('size',$requested_size)->delete();
        //         $sizes = new Product_sizes;
        //         $sizes->size = $val->id;
        //         $sizes->product_id = $insertedId;
        //         $sizes->status = $req->status;
        //         $sizes->save();
        //     }

        // }

        // // delete color
        // $requested_colors = array();
        // $color_id = Colours::whereIn('id',$req->colour)->get();
        // foreach ($color_id as  $val){
        //     array_push($requested_colors,$val->id);
        // }

        // foreach ($color_id as  $val)
        // {
        //     $stored_product_color = Product_colours::where('product_id',$insertedId)->where('colour_id',$val->id)->get();
        //     if(count($stored_product_color)>0){
        //         $unmatched_stored_product_color = Product_colours::where('product_id',$insertedId)->whereNotIn('colour_id',$requested_colors)->delete();
        //     }
        //     else{
        //         $unmatched_stored_product_color = Product_colours::where('product_id',$insertedId)->whereNotIn('colour_id',$requested_colors)->delete();
        //         $colors = new Product_colours;
        //         $colors->colour_id = $val->id;
        //         $colors->product_id = $insertedId;
        //         $colors->status = $req->status;
        //         $colors->save();
        //     }
        // }
        // return redirect()->route('admin.product')->with('success','Product updated successfully!');
        
    public function getproductvariant(Request $req){
        $id = $req->id;
        $colorList = Colours::all();
        $sizeList = Sizes::all();
        $view = view('backend.pages.product.productvariant', compact('colorList','sizeList','id'))->render();
        return response()->json(['html' => $view]);
    }
}
