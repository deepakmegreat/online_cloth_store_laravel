<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Models\User;

class adminUserController extends Controller
{
    public function index(Request $request)
    {
        $item = User::sortable()->select('id', 'name', 'email', 'password')->where('role','!=','1');
        if ($request->has('search_submit') && $request->search_submit != '') {
            if ($request->has('name') && $request->name != '') {
                $item->where('name', $request->name);
            }
             if ($request->has('email') && $request->email != '') {
                $item->where('email', $request->email);
            }
        }
        $allUsers = user::where('role','!=','1')->get();
        $item = $item->orderBy('id','DESC')->paginate(10);
        return view("backend.pages.user.list", ["users" => $item, "allUsers" => $allUsers, "request"=>$request]);
    }

    public function destroy(Request $req)
    {
        // Find the user to be deleted
        $user = User::findOrFail($req->id);

        // Delete the user
        $user->delete();

        // Redirect back to the previous page with a success message
        return  session()->flash('success','User Successfully deleted');
    }

    public function edit($id){
        $data=User::find($id);
        // $admin_data = User::find(1);
        // print_r($admin_data);
        // die();
        return view("backend.pages.user.edit",["user"=>$data]);
    }

    public function update(Request $req){
        $req->validate([
            'name'=>'required | regex:/^[a-zA-Z_ ]+$/u | max:20',
            'email'=>'required | regex: /^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$/u'
        ]);
        // print_r($req->all());
        // die();
        $obj = new User;
        $res = $obj->saveEditUser($req);
        if($res=="success"){
            return redirect('admin/user')->with('success','user updated successfully!');
        }
    }

}
