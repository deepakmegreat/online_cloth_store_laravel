<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Orders;
use App\Models\User;
use Illuminate\Http\Request;

class dashboardController extends Controller
{
    public function index(){
        $total_products = Product::count();
        $total_users = User::count();
        $total_orders = Orders::where('order_status','!=','Cancelled')->count();
        return view('backend.pages.dashboard',['total_products'=>$total_products,
        'total_users'=>$total_users,'total_orders'=>$total_orders]);
    }


}
