<?php

namespace App\Http\Controllers\backend;

use App\Models\Sizes;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SizeController extends Controller
{
    public function index(Request $request){
        $data=Sizes::sortable();
        if ($request->has('search_submit') && $request->search_submit != '') {
            if ($request->has('size') && $request->size != '') {
                $data->where('size',  $request->size);
            }
        }
        $allSize = Sizes::get();
        $data = $data->orderBy('id','DESC')->paginate(10);
      
        return view("backend.pages.size.list",["sizes"=>$data, "size"=>$allSize, "request"=>$request]);
    }

    public function add(Request $req){
        $req->validate([
            'size'=>'required | regex:/^[A-Z]+$/u | max:4 | unique:sizes'
        ]);
        $size=new Sizes();
        $size->size=$req->size;
        $res=$size->save();
        if($res=="success"){
            return redirect('admin/size')->with('success','Size added successfully!');
        }
    }

    public function delete(Request $req){
        $res=Sizes::find($req->id)->delete();
        if($res == 1){
            return  session()->flash('success','Size Successfully deleted');
        }
        else{
            return  session()->flash('error','Size not deleted');
        }
    }
}
