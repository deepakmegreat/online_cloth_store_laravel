<?php

namespace App\Http\Controllers\backend;

use App\Models\Category;
use App\Models\Product;
use App\Models\Product_sizes;
use App\Models\Product_colours;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function index(Request $request){
        $data=Category::sortable();
        if ($request->has('search_submit') && $request->search_submit != '') {
            if ($request->has('category') && $request->category != '') {
                $data->where('name',  $request->category);
            }
            if ($request->has('status') && $request->status != '') {
                $data->where('status',  $request->status);
            }
        }
        $data = $data->orderBy('id','DESC')->paginate(10);
        $allStatus = Category::select('status')->distinct()->get();
        $allCategory = Category::select('name')->get();
        return view("backend.pages.category.list",["categories"=>$data, "status"=>$allStatus, 'allCategory'=>$allCategory,
            "request"=>$request]);
    }

    public function add(Request $req){
        $req->validate([
            'name'=>'required | regex:/^[a-zA-Z]+$/u | max:20 ',
            'category_image.*'=>'mimes:png,jpeg,jpg'
        ]);
        if ($req->isMethod('post')) {
            $category = new Category();
            $category->name = $req->name;
            if ($req->file('category_image')=='') {
                $img = 'default.jpg';
                $category->category_image = $img;
            } else {
                $extension = $req->file('category_image')->extension();
                $img = $req->file('category_image')->storeAs('', time().'.'.$extension);
                $category->category_image = $img;
                $req->file('category_image')->move(public_path('backend/images/uploads/category'), $img);
            }
            $category->status = $req->status;

            if ($category->save()) {
                return redirect()->route('admin.category')->with('success', 'category added successfully!');
            } else {
                return redirect()->route('admin.category')->with('error', 'category not added!');
            }
        }

    }

    public function edit($id){
        $data=Category::find($id);
        return view("backend.pages.category.edit",["category"=>$data]);
    }

    public function update(Request $req){
        $req->validate([
            'name'=>'required | regex:/^[a-zA-Z]+$/u | max:20',
            'category_image.*'=>'mimes:png,jpeg,jpg'
        ]);
        if ($req->isMethod('post')) {
            $category = Category::find($req->cid);

            $category->name = $req->name;
            if ($req->file('category_image')!='') {
                if (file_exists('backend/images/uploads/category/'.$category['category_image']) && $category->category_image != null && $category->category_image != "default.jpg") {
                    unlink('backend/images/uploads/category/'.$category->category_image);
                }
                $extension = $req->file('category_image')->extension();
                $img = $req->file('category_image')->storeAs('', time().'.'.$extension);
                $category->category_image = $img;
                $req->file('category_image')->move(public_path('backend/images/uploads/category'), $img);
            } else {
                if (!file_exists('backend/images/uploads/category/'.$category['category_image'])) {
                    $img = 'default.jpg';
                    $category->category_image = $img;
                }
            }
            $category->status = $req->status;

            if ($category->save()) {
                return redirect()->route('admin.category')->with('success', 'category updated successfully!');
            } else {
                return redirect()->route('admin.category')->with('error', 'category not updated!');
            }
        }
    }

    public function delete(Request $req){
        $category = Category::find($req->id);
        if($category->delete()){
            $productWithProductSize = Product::with('productSizes', 'productColors')->where('category_id',$req->id)->get()->toArray();
            foreach($productWithProductSize as $product){
                Product::find($product['id'])->delete();
                Product_sizes::where('product_id',$product['id'])->delete();
                Product_colours::where('product_id',$product['id'])->delete();
            }
            session()->flash('success','Category Successfully deleted');
        }
        else{
            session()->flash('error','Category not deleted');
        }
    }


}
