<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\backend\Controller;
use Illuminate\Http\Request;

class userController extends Controller
{
    public function add_user(){
        return view('backend.pages.user.add');
    }
    public function delete_user(){
        return view('backend.pages.user.delete');
    }
    public function edit_user(){
        return view('backend.pages.user.edit');


    }
}
