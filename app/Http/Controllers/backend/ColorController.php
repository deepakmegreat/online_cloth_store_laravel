<?php

namespace App\Http\Controllers\backend;

use App\Models\Colours;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class ColorController extends Controller
{
    public function index(Request $request){
        // dd($request);
        $data=Colours::sortable();
        if ($request->has('search_submit') && $request->search_submit != '') {
            if ($request->has('color_name') && $request->color_name != '') {
                $data->where('colour', $request->color_name);
            }
            if ($request->has('color_code') && $request->color_code != '') {
                $data->where('colour_code',  $request->color_code);
            }
        }
        $allColor = Colours::get();
        $data = $data->orderBy('id', 'DESC')->paginate(10);
        return view("backend.pages.color.list",["colors"=>$data, "color"=>$allColor, "request"=>$request]);
    }

    public function add(Request $req){
        $req->validate([
            'colour'=>'required | regex:/^[a-zA-Z]+$/u | max:20 | unique:colours',
            'colour_code'=>'required | regex:/^[#0-9a-zA-Z]+$/u | max:8'
        ]);
        $color=new Colours();
        $color->colour=$req->colour;
        $color->colour_code=$req->colour_code;
        $res=$color->save();
        if($res=="success"){
            return redirect('admin/color')->with('success','Color added successfully!');
        }
    }

    public function edit($id){
        $data=Colours::find($id);
        return view("backend.pages.color.edit",["color"=>$data]);
    }

     function update(Request $req){
        $req->validate([
            'colour'=>'required | regex:/^[a-zA-Z]+$/u | max:20',
            'colour_code'=>'required | regex:/^[#0-9a-zA-Z]+$/u | max:8'
        ]);
        $color=Colours::find($req->cid);
        $color->colour=$req->colour;
        $color->colour_code=$req->colour_code;
        $res=$color->save();
        if($res=="success"){
            return redirect('admin/color')->with('success','Color Updated!');
        }

    }

    public function delete(Request $req){
        $res=Colours::find($req->id)->delete();
        if($res == 1){
            return  session()->flash('success','Color Successfully deleted');
        }
        else{
            return  session()->flash('success','Color not deleted');
        }
    }
}
