<?php

namespace App\Http\Controllers\Auth;

use App\Models\Cart;
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Redirect;
use URL;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated(Request $request)
    {   
         // Get the cart data from the session
        $cartData = session()->get('cart', []);
        // If there is cart data, store it in the database for the logged in user
        if (!empty($cartData)) {
            foreach (session('cart') as $id=>$details){
                foreach ($details['color']  as $color => $size)
                {
                    foreach ($size['size'] as $sizelabel => $sizename){
                    $id2 = checkCartForItem2(Auth::user()->id,$id,$color,$sizelabel);
                    if(!empty($id2) && isset($id2)){
                        $cart = Cart::find($id2);
                        $cart->Price = $sizename['price'];
                        $cart->quantity += $sizename['quantity'];
                        $cart->save();    
                    }
                    else
                    {
                            $cart = new Cart();
                            $cart->Userid = Auth::user()->id;
                            $cart->Productid = $id;
                            $cart->color = $color;
                            $cart->size = $sizelabel;
                            $cart->photo = $details['photo'];
                            $cart->product_name = $details['product_name'];
                            $cart->Price = $sizename['price'];
                            $cart->quantity = $sizename['quantity'];
                            $cart->save();
                    }
                }
                }
            }
            // Clear the cart data from the session
            session()->forget('cart');
        }

        if(Auth::user()->role == '1'){
            // User is an admin, redirect to admin dashboard
                return redirect('admin/dashboard');
        } else {
            // User is not an admin, show error message
                if (Str::contains(url()->previous(), 'checkout')) {
                    return redirect()->intended(route('checkout'));
                }
                return redirect()->intended('/');
        }
    }

}
