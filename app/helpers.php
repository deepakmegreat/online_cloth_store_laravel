<?php

use App\Models\Cart;
use App\Models\Colours;
use App\Models\Sizes;
use App\Models\Wishlist;
use App\Models\Product;

function find_cart_login_user($userid) {
    $result = Cart::all()->where('Userid',$userid)->toArray();
    return $result;
}

function total_item_in_cart($userid) {
        $result = Cart::where('Userid',$userid)->distinct('id')->count();
        return $result;
}

function checkCartForItem($userid,$request){
    $result = Cart::all()->where('Userid',$userid)->where('Productid',$request->id)->toArray();
    foreach($result as $data)
    {
        if(($data['color'] == $request->color) && ($data['size'] == $request->size))
        {
            return $data['id'];
        }
    }
}

function checkCartForItem2($userid,$id,$color,$size){
    $result = Cart::where('Userid',$userid)->where('Productid',$id)->where('color',$color)->where('size',$size)->first();
    if(isset($result)){
        return $result->id;
    } else {
        return null;
    }
}

function find_wishlist_login_user($userid) {
    $result = Wishlist::all()->where('user_id',$userid)->toArray();
    return $result;
}

function total_item_in_wishlist($userid) {
        $result = Wishlist::where('user_id',$userid)->distinct('product_id')->count();
        return $result;
}

function checkWishlistForItem($userid,$request){
    $result = Wishlist::all()->where('user_id',$userid)->where('product_id',$request->id)->toArray();
    foreach($result as $data)
    {
        if(($data['color'] == $request->color) && ($data['size'] == $request->size))
        {
            return $data['id'];
        }
    }
}

function checkForStockAvailability($id){
    $quantity = Product::all()->where('id',$id)->where('deleted_at',null)->toArray();
    foreach($quantity as $quan){
        return $quan['quantity'];
    }
}

function getColorName($colorid){
    $colorName = Colours::select('colour')->where('id',$colorid)->first();
    return $colorName->colour;
}

function getSizeName($sizeid){
    $sizeName = Sizes::select('size')->where('id',$sizeid)->first();
    return $sizeName->size;
}
?>